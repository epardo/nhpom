MODULE blk33d
        USE  PARAMETERS 
	USE  double
        REAL(KIND=DP), DIMENSION(IM, JM), SAVE :: dtf
        REAL(KIND=DP), DIMENSION(IM, JM, KB), SAVE :: &
                wb, wf, ww, wwf, wwb, ccf, fff, &
                aaf, bbf
        REAL(KIND=DP), SAVE :: moda
        REAL(KIND=DP), DIMENSION(IM, JM, KB), SAVE :: wq
END MODULE blk33d
