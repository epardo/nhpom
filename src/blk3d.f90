MODULE BLK3D
        USE  PARAMETERS
	USE double
        REAL(KIND=DP), DIMENSION(IM, JM, KB), SAVE :: &
                UF, VF, KM, KH, KQ, L, &
                Q2, Q2B, AAM, Q2L, Q2LB, &
                U, UB, W, V, VB, T, TB, S, SB, &
                RHO, RMEAN
	REAL(KIND=DP), DIMENSION(IM, JM, KB), TARGET :: DTEF
END MODULE BLK3D
