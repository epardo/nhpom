 MODULE PARAMETERS
 	IMPLICIT NONE
 !      REAL :: KM, KH, KQ, L
 !      INCLUDE 'IMJMKB'
        INTEGER, PARAMETER :: &
                  IM=400, &
                  JM=6, &
                  KB=80, &
                  ks=80, &
 !     PARAMETER (IM=100,JM=40,KB=15)
                  IMM1=IM-1, &
                  JMM1=JM-1, &
                  KBM1=KB-1, &
                  IMM2=IM-2, &
                  JMM2=JM-2, &
                  KBM2=KB-2, &
                  LIJ=IM*JM, &
                  LIJ2=LIJ*2, &
                  LIJK=LIJ*KB, &
                  LIJKM2=LIJ*KBM2, &
                  LIJM1=IM*(JM-1), &
                  LIJKM1=LIJ*KBM1
 END MODULE PARAMETERS
