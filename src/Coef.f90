       SUBROUTINE coef1(aa1,bb1,cc1,ff)
       USE BLK1D
       USE BLK2D
       USE BLK3D
       USE BLKCON
       IMPLICIT NONE
!      INCLUDE 'comblk98.h'  
       REAL(KIND=DP), dimension(im,jm,kb) :: aa1, bb1, cc1, ff
       integer :: i, j, k 
	do k=1,kb 
		do i=2,imm1
			do j=2,jmm1
	
				AA1(i,j,k)=(zz(k)*(etF(i+1,j)-etF(i-1,j)+h(i+1,j)-h(i-1,j))+ &
				EtF(I+1,J)-EtF(I-1,J))/(2.)
				bb1(i,j,k)=(zz(k)*(etF(i,j+1)-etF(i,j-1)+h(i,j+1)-h(i,j-1))+ &
				EtF(I,J+1)-EtF(I,J-1))/(2.)
				cc1(i,j,k)=(zz(k)*(etf(i,j)-etb(i,j)+h(i,j)-h(i,j))/(2.*dti)+ &
				(etf(i,j)-etb(i,j))/(2.*dti))
			end do
		end do
	end do

	do k=1,kb 
		do i=1,im
			do j=1,jm
				if (i==2.or.i==1) then
					aa1(i,j,k)=(zz(k)*(etF(i+1,j)-etF(i,j)+h(i+1,j)-h(i,j))+ &
					EtF(I+1,J)-EtF(I,J))
				endif
				if (j==2.or.j==1) then
     					bb1(i,j,k)=(zz(k)*(etF(i,j+1)-etF(i,j)+h(i,j+1)-h(i,j))+ &
					EtF(I,J+1)-EtF(I,J))
				endif
				if (i==imm1.or.i==im) then
					aa1(i,j,k)=(zz(k)*(etF(i,j)-etF(i-1,j)+h(i,j)-h(i-1,j))+ &
					EtF(I,J)-EtF(I-1,J))
				endif
				if (j==jmm1.or.j==jm) then
					bb1(i,j,k)=(zz(k)*(etF(i,j)-etF(i,j-1)+h(i,j)-h(i,j-1))+ &
					EtF(I,J)-EtF(I,J-1))
				endif
			end do
		end do
	end do		

	DO K=1,KB
        	DO I=2,IMm1
        		DO J=2,JMm1
	  			ff(i,j,k)=.5e0*(uF(i,j,k)+uF(i+1,j,k)) &
				*aa1(i,j,k)/dx(i,j)+ &
				2.5e0*(vF(i,j,k)+vF(i,j+1,k))/dy(i,j) &
     				*bb1(i,j,k)+cc1(i,j,k)
			END DO
		END DO
	END DO
	
	return 

	END SUBROUTINE coef1

	SUBROUTINE coef(aa1,bb1,cc1)
	USE BLK1D
        USE BLK2D
	USE BLKCON
	IMPLICIT NONE
!	INCLUDE 'comblk98.h'  
	REAL(KIND=DP), dimension(im,jm,kb) :: aa1, bb1, cc1
	integer :: i, j, k

	do k=1,kb 
		do i=2,imm1
			do j=2,jmm1
	
				AA1(i,j,k)=(zz(k)*(etF(i+1,j)-etF(i-1,j)+h(i+1,j)-h(i-1,j))+ &
				EtF(I+1,J)-EtF(I-1,J))/(2.)
				bb1(i,j,k)=(zz(k)*(etF(i,j+1)-etF(i,j-1)+h(i,j+1)-h(i,j-1))+ &
				EtF(I,J+1)-EtF(I,J-1))/(2.)
				cc1(i,j,k)=(zz(k)*(etf(i,j)-etb(i,j)+h(i,j)-h(i,j))/(2.*dti)+ &
				(etf(i,j)-etb(i,j))/(2.*dti))
			end do
		end do
	end do

	do k=1,kb 
		do i=1,im
			do j=1,jm
				if (i==2.or.i==1) then
					aa1(i,j,k)=(zz(k)*(etF(i+1,j)-etF(i,j)+h(i+1,j)-h(i,j))+ &
					EtF(I+1,J)-EtF(I,J))
				endif
				if (j==2.or.j==1) then
     					bb1(i,j,k)=(zz(k)*(etF(i,j+1)-etF(i,j)+h(i,j+1)-h(i,j))+ &
					EtF(I,J+1)-EtF(I,J))
				endif
				if (i==imm1.or.i==im) then
					aa1(i,j,k)=(zz(k)*(etF(i,j)-etF(i-1,j)+h(i,j)-h(i-1,j))+ &
					EtF(I,J)-EtF(I-1,J))
				endif
				if (j==jmm1.or.j==jm) then
					bb1(i,j,k)=(zz(k)*(etF(i,j)-etF(i,j-1)+h(i,j)-h(i,j-1))+ &
					EtF(I,J)-EtF(I,J-1))
				endif
			end do
		end do
	end do
	
	return 
	END SUBROUTINE coef
			
	subroutine coef2(aa1,bb1,cc1,ff)
	USE BLK1D
        USE BLK2D
        USE BLK3D
	USE BLKCON
        IMPLICIT NONE
!	INCLUDE 'comblk98.h'  
	REAL(KIND=DP), dimension(im,jm,kb) :: aa1, bb1, cc1, ff
	integer :: i, j, k
	do k=1,kb 
		do i=2,imm1
			do j=2,jmm1
	
				AA1(i,j,k)=(z(k)*(etF(i+1,j)-etF(i-1,j)+h(i+1,j)-h(i-1,j))+ &
				EtF(I+1,J)-EtF(I-1,J))/(2.)
				bb1(i,j,k)=(z(k)*(etF(i,j+1)-etF(i,j-1)+h(i,j+1)-h(i,j-1))+ &
				EtF(I,J+1)-EtF(I,J-1))/(2.)
				cc1(i,j,k)=(z(k)*(etf(i,j)-etb(i,j)+h(i,j)-h(i,j))/(2.*dti)+ &
				(etf(i,j)-etb(i,j))/(2.*dti))
			end do
		end do
	end do

	do k=1,kb 
		do i=1,im
			do j=1,jm
				if (i==2.or.i==1) then
					aa1(i,j,k)=(z(k)*(etF(i+1,j)-etF(i,j)+h(i+1,j)-h(i,j))+ &
					EtF(I+1,J)-EtF(I,J))
				endif
				if (j==2.or.j==1) then
     					bb1(i,j,k)=(z(k)*(etF(i,j+1)-etF(i,j)+h(i,j+1)-h(i,j))+ &
					EtF(I,J+1)-EtF(I,J))
				endif
				if (i==imm1.or.i==im) then
					aa1(i,j,k)=(z(k)*(etF(i,j)-etF(i-1,j)+h(i,j)-h(i-1,j))+ &
					EtF(I,J)-EtF(I-1,J))
				endif
				if (j==jmm1.or.j==jm) then
					bb1(i,j,k)=(z(k)*(etF(i,j)-etF(i,j-1)+h(i,j)-h(i,j-1))+ &
					EtF(I,J)-EtF(I,J-1))
				endif
			end do
		end do
	end do
	
	DO K=2,KB
      		DO I=2,IMm1
      			DO J=2,JMm1
				ff(i,j,k)=.25e0*(uF(i,j,k)+uF(i+1,j,k)+uF(i,j,k-1)+uF(i+1,j,k-1)) &
				*aa1(i,j,k)/dx(i,j)+ &
				2.25e0*(vF(i,j,k)+vF(i,j+1,k)+vF(i,j,k-1)+vF(i,j+1,k-1)) &
     				*bb1(i,j,k)/dY(i,j)+cc1(i,j,k)
				ff(i,j,1)=.5e0*(uF(i,j,1)+uF(i+1,j,1)) &
				*aa1(i,j,1)/dx(i,j)+ &
				2.5e0*(vF(i,j,1)+vF(i,j+1,1))/dY(i,j) &
     				*bb1(i,j,1)+cc1(i,j,1)
			END DO
		END DO
	END DO
	
	return 

	end subroutine coef2

	SUBROUTINE coef3(aa1,bb1)
	USE BLK1D
        USE BLK2D
	IMPLICIT NONE
!	INCLUDE 'comblk98.h'  
	REAL(KIND=DP), dimension(im,jm,kb) :: aa1, bb1
	integer :: i, j, k
	do k=1,kb 
		do i=2,imm1
			do j=2,jmm1
	
				AA1(i,j,k)=(zz(k)*(etb(i+1,j)-etb(i-1,j)+h(i+1,j)-h(i-1,j))+ &
				Etb(I+1,J)-Etb(I-1,J))/(2.)
				bb1(i,j,k)=(zz(k)*(etb(i,j+1)-etb(i,j-1)+h(i,j+1)-h(i,j-1))+ &
				Etb(I,J+1)-Etb(I,J-1))/(2.)
			end do
		end do
	end do

	do k=1,kb 
		do i=1,im
			do j=1,jm
				if (i==2.or.i==1) then
					aa1(i,j,k)=(zz(k)*(etb(i+1,j)-etb(i,j)+h(i+1,j)-h(i,j))+ &
					Etb(I+1,J)-Etb(I,J))
				endif
				if (j==2.or.j==1) then
     					bb1(i,j,k)=(zz(k)*(etb(i,j+1)-etb(i,j)+h(i,j+1)-h(i,j))+ &
					Etb(I,J+1)-Etb(I,J))
				endif
				if (i==imm1.or.i==im) then
					aa1(i,j,k)=(zz(k)*(etb(i,j)-etb(i-1,j)+h(i,j)-h(i-1,j))+ &
					Etb(I,J)-Etb(I-1,J))
				endif
				if (j==jmm1.or.j==jm) then
					bb1(i,j,k)=(zz(k)*(etb(i,j)-etb(i,j-1)+h(i,j)-h(i,j-1))+ &
					Etb(I,J)-Etb(I,J-1))
				endif
			end do
		end do
	end do

	return 

	END SUBROUTINE coef3
