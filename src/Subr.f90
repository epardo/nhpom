      SUBROUTINE ADVAVE(ADVUA,ADVVA)
      USE BLK2D
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'
      REAL(KIND=DP), DIMENSION(IM,JM) :: ADVUA, ADVVA
      REAL(KIND=DP), DIMENSION(:, :), POINTER ::CURV2D
      INTEGER :: I, J 
      CURV2D => TPS
!---------------------------------------------------------------------
!      CALCULATE U-ADVECTION & DIFFUSION
!---------------------------------------------------------------------
!
!-------- ADVECTIVE FLUXES -------------------------------------------
!
      DO J=1,JM
		DO I=1,IM
			ADVUA(I,J)=0.
		END DO
	  END DO
      DO J=2,JM
		DO I=2,IMM1
			FLUXUA(I,J)=.125E0*((D(I+1,J)+D(I,J))*UA(I+1,J) &
                      +(D(I,J)+D(I-1,J))*UA(I,J)) &
                      *(UA(I+1,J)+UA(I,J))
        END DO
      END DO
      DO J=2,JM
		DO I=2,IM
			FLUXVA(I,J)=.125E0*((D(I,J)+D(I,J-1))*VA(I,J) &
						+(D(I-1,J)+D(I-1,J-1))*VA(I-1,J)) &
                        *(UA(I,J)+UA(I,J-1))
        END DO
      END DO
!----------- ADD VISCOUS FLUXES ---------------------------------
      DO J=2,JM
		DO I=2,IMM1
			FLUXUA(I,J)=FLUXUA(I,J) &
              -D(I,J)*2.E0*AAM2D(I,J)*(UAB(I+1,J)-UAB(I,J))/DX(I,J)
        END DO
      END DO
      DO J=2,JM
		DO I=2,IM
			CURV2D(I,J)=.25E0*(D(I,J)+D(I-1,J)+D(I,J-1)+D(I-1,J-1)) &
                 *(AAM2D(I,J)+AAM2D(I,J-1)+AAM2D(I-1,J)+AAM2D(I-1,J-1)) &
                     *((UAB(I,J)-UAB(I,J-1)) &
                     /(DY(I,J)+DY(I-1,J)+DY(I,J-1)+DY(I-1,J-1)) &
                      +(VAB(I,J)-VAB(I-1,J)) &
                     /(DX(I,J)+DX(I-1,J)+DX(I,J-1)+DX(I-1,J-1)) )
			FLUXUA(I,J)=FLUXUA(I,J)*DY(I,J)
			FLUXVA(I,J)=(FLUXVA(I,J)-CURV2D(I,J)) &
                 *.25E0*(DX(I,J)+DX(I-1,J)+DX(I,J-1)+DX(I-1,J-1))
        END DO
      END DO
!----------------------------------------------------------------
      DO J=2,JMM1
		DO I=2,IMM1
			ADVUA(I,J)=FLUXUA(I,J)-FLUXUA(I-1,J) &
                +FLUXVA(I,J+1)-FLUXVA(I,J)
		END DO
	  END DO
!----------------------------------------------------------------
!       CALCULATE V-ADVECTION & DIFFUSION
!----------------------------------------------------------------
!
      DO J=1,JM
		DO I=1,IM
			ADVVA(I,J)=0.
		END DO
	  END DO
!
!---------ADVECTIVE FLUXES ----------------------------
      DO J=2,JM
		DO I=2,IM
			FLUXUA(I,J)=.125E0*((D(I,J)+D(I-1,J))*UA(I,J) &
              +(D(I,J-1)+D(I-1,J-1))*UA(I,J-1))* &
              (VA(I-1,J)+VA(I,J))
        END DO
      END DO
      DO J=2,JMM1
		DO I=2,IM
			FLUXVA(I,J)=.125E0*((D(I,J+1)+D(I,J)) &
            *VA(I,J+1)+(D(I,J)+D(I,J-1))*VA(I,J)) &
            *(VA(I,J+1)+VA(I,J))
        END DO
      END DO
!------- ADD VISCOUS FLUXES -----------------------------------
      DO J=2,JMM1
		DO I=2,IM
			FLUXVA(I,J)=FLUXVA(I,J) &
            -D(I,J)*2.E0*AAM2D(I,J)*(VAB(I,J+1)-VAB(I,J))/DY(I,J)
		END DO
      END DO
      DO J=2,JM
		DO I=2,IM
			FLUXVA(I,J)=FLUXVA(I,J)*DX(I,J)
			FLUXUA(I,J)=(FLUXUA(I,J)-CURV2D(I,J)) &
            *.25E0*(DY(I,J)+DY(I-1,J)+DY(I,J-1)+DY(I-1,J-1))
		END DO
      END DO
!---------------------------------------------------------------
      DO J=2,JMM1
		DO I=2,IMM1
			ADVVA(I,J)=FLUXUA(I+1,J)-FLUXUA(I,J) &
            +FLUXVA(I,J)-FLUXVA(I,J-1)
        END DO
      END DO
!
!---------------------------------------------------------------
! 
      RETURN
      END SUBROUTINE ADVAVE
!               
      SUBROUTINE ADVQ(QB,Q,DTI2,QF)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'   
      REAL(KIND=DP), DIMENSION(IM,JM,KB) ::  QB,Q, QF
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: XFLUX, YFLUX
      REAL(KIND=DP) :: DTI2
      INTEGER :: I, J, K
!      EQUIVALENCE (XFLUX,A),(YFLUX,C)
      XFLUX => A
      YFLUX => C
!
!******* HORIZONTAL ADVECTION ************************************
      DO K=2,KBM1
		DO J=2,JM
			DO I=2,IM
				XFLUX(I,J,K)=.125E0*(Q(I,J,K)+Q(I-1,J,K)) &
				*(DT(I,J)+DT(I-1,J))*(U(I,J,K)+U(I,J,K-1))
				YFLUX(I,J,K)=.125E0*(Q(I,J,K)+Q(I,J-1,K))*(DT(I,J)+DT(I,J-1)) &
				*(V(I,J,K)+V(I,J,K-1))
			END DO
		END DO
      END DO
!******* HORIZONTAL DIFFUSION ************************************
      DO K=2,KBM1
		DO J=2,JM
			DO I=2,IM
				XFLUX(I,J,K)=XFLUX(I,J,K) &
				-.25*(AAM(I,J,K)+AAM(I-1,J,K)+AAM(I,J,K-1)+AAM(I-1,J,K-1)) &
				*(H(I,J)+H(I-1,J))*(QB(I,J,K)-QB(I-1,J,K))*DUM(I,J) &
				/(DX(I,J)+DX(I-1,J))
				YFLUX(I,J,K)=YFLUX(I,J,K) &
				-.25*(AAM(I,J,K)+AAM(I,J-1,K)+AAM(I,J,K-1)+AAM(I,J-1,K-1)) &
				*(H(I,J)+H(I,J-1))*(QB(I,J,K)-QB(I,J-1,K))*DVM(I,J) &
				/(DY(I,J)+DY(I,J-1))
				XFLUX(I,J,K)=.5E0*(DY(I,J)+DY(I-1,J))*XFLUX(I,J,K)
				YFLUX(I,J,K)=.5E0*(DX(I,J)+DX(I,J-1))*YFLUX(I,J,K)
			END DO
		END DO
      END DO
!****** VERTICAL ADVECTION; ADD FLUX TERMS ;THEN STEP FORWARD IN TIME **
      DO K=2,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				QF(I,J,K)=(W(I,J,K-1)*Q(I,J,K-1)-W(I,J,K+1)*Q(I,J,K+1)) &
				/(DZ(K)+DZ(K-1))*ART(I,J) &
				+XFLUX(I+1,J,K)-XFLUX(I,J,K) &
				+YFLUX(I,J+1,K)-YFLUX(I,J,K)
				QF(I,J,K)=((H(I,J)+ETB(I,J))*ART(I,J)*QB(I,J,K)-DTI2*QF(I,J,K)) &
                  /((H(I,J)+ETF(I,J))*ART(I,J))
            END DO
        END DO
	  END DO
!
      RETURN
      END SUBROUTINE ADVQ

      SUBROUTINE ADVCT(ADVX,ADVY)        
!======================================================================
!  This routine calculates the horizontal portions of momentum advection
!  well in advance of their use in ADVU and ADVV so that their vertical 
!  integrals (created in MAIN) may be used in the external mode calculation.
!======================================================================
      USE BLK2D
      USE BLK3D
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'   
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: ADVX, ADVY
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: XFLUX, YFLUX, CURV
      REAL(KIND=DP) :: DTAAM
      INTEGER :: I, J, K
!      EQUIVALENCE (A,XFLUX),(C,YFLUX),(EE,CURV)
      XFLUX => A
      YFLUX => C
      CURV => EE
!
      DO K=1,KB
		DO J=1,JM
			DO I=1,IM
				CURV(I,J,K)=0.
				ADVX(I,J,K)=0.
				XFLUX(I,J,K)=0.
				YFLUX(I,J,K)=0.
			END DO
		END DO
	  END DO
!
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				CURV(I,J,K)= &
				+.25E0*((V(I,J+1,K)+V(I,J,K))*(DY(I+1,J)-DY(I-1,J)) &
                -(U(I+1,J,K)+U(I,J,K))*(DX(I,J+1)-DX(I,J-1)) ) 
            END DO
        END DO
      END DO
!-----------------------------------------------------------------
!      Calculate x-component of velocity advection                 
!-----------------------------------------------------------------
!
!******** HORIZONTAL ADVECTION FLUXES *****************************
      DO K=1,KBM1
		DO J=1,JM
			DO I=2,IMM1
				XFLUX(I,J,K)=.125E0*((DT(I+1,J)+DT(I,J))* &
				U(I+1,J,K)+(DT(I,J)+DT(I-1,J)) &
                *U(I,J,K))*(U(I+1,J,K)+U(I,J,K))
            END DO
        END DO
      END DO
      DO K=1,KBM1
		DO J=2,JM
			DO I=2,IM
				YFLUX(I,J,K)=.125E0*((DT(I,J)+DT(I,J-1)) &
				*V(I,J,K)+(DT(I-1,J)+DT(I-1,J-1)) &
				*V(I-1,J,K))*(U(I,J,K)+U(I,J-1,K))
			END DO
		END DO
      END DO
!****** ADD HORIZONTAL DIFFUSION FLUXES ****************************
      DO K=1,KBM1
		DO J=2,JM
			DO I=2,IMM1
				XFLUX(I,J,K)=XFLUX(I,J,K) &
                -DT(I,J)*AAM(I,J,K)*2.E0*(UB(I+1,J,K)-UB(I,J,K))/DX(I,J)
				DTAAM=.25E0*(DT(I,J)+DT(I-1,J)+DT(I,J-1)+DT(I-1,J-1)) &
                *(AAM(I,J,K)+AAM(I-1,J,K)+AAM(I,J-1,K)+AAM(I-1,J-1,K))
				YFLUX(I,J,K)=YFLUX(I,J,K) &
				-DTAAM*((UB(I,J,K)-UB(I,J-1,K)) &
				/(DY(I,J)+DY(I-1,J)+DY(I,J-1)+DY(I-1,J-1)) &
				+(VB(I,J,K)-VB(I-1,J,K)) &
				/(DX(I,J)+DX(I-1,J)+DX(I,J-1)+DX(I-1,J-1)))
!
				XFLUX(I,J,K)=DY(I,J)*XFLUX(I,J,K)
				YFLUX(I,J,K)= &
				.25E0*(DX(I,J)+DX(I-1,J)+DX(I,J-1)+DX(I-1,J-1))*YFLUX(I,J,K)
			END DO
		END DO
      END DO
!
!
!******** DO HORIZ. ADVECTION *******
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				ADVX(I,J,K)= &
				+XFLUX(I,J,K)-XFLUX(I-1,J,K) &
				+YFLUX(I,J+1,K)-YFLUX(I,J,K)
			END DO
		END DO
      END DO
      DO K=1,KBM1
		DO J=3,JMM2
			DO I=3,IMM2
				ADVX(I,J,K)=ADVX(I,J,K) &
				-ARU(I,J)*.25*(CURV(I,J,K)*DT(I,J)*(V(I,J+1,K)+V(I,J,K)) &
				+CURV(I-1,J,K)*DT(I-1,J)*(V(I-1,J+1,K)+V(I-1,J,K)))
			END DO
		END DO
      END DO
!
!-----------------------------------------------------------------
!      Calculate y-component of velocity advection                 
!-----------------------------------------------------------------
      DO K=1,KB
		DO J=1,JM
			DO I=1,IM
				ADVY(I,J,K)=0.E0
				XFLUX(I,J,K)=0.E0
				YFLUX(I,J,K)=0.E0
			END DO
		END DO
	  END DO
!
!********** HORIZONTAL ADVECTION FLUXES **************************
      DO K=1,KBM1
		DO J=2,JM
			DO I=2,IM
				XFLUX(I,J,K)=.125E0*((DT(I,J)+DT(I-1,J))*U(I,J,K) &
                +(DT(I,J-1)+DT(I-1,J-1))*U(I,J-1,K)) &
                *(V(I,J,K)+V(I-1,J,K))
            END DO
        END DO
      END DO
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=1,IM
				YFLUX(I,J,K)=.125E0*((DT(I,J+1)+DT(I,J))*V(I,J+1,K) &
				+(DT(I,J)+DT(I,J-1))*V(I,J,K)) &
                *(V(I,J+1,K)+V(I,J,K))
            END DO
        END DO
      END DO
!******* ADD HORIZONTAL DIFFUSION FLUXES **************************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IM
				DTAAM=.25E0*(DT(I,J)+DT(I-1,J)+DT(I,J-1)+DT(I-1,J-1)) &
                *(AAM(I,J,K)+AAM(I-1,J,K)+AAM(I,J-1,K)+AAM(I-1,J-1,K))
				XFLUX(I,J,K)=XFLUX(I,J,K) &
				-DTAAM*((UB(I,J,K)-UB(I,J-1,K)) &
				/(DY(I,J)+DY(I-1,J)+DY(I,J-1)+DY(I-1,J-1)) &
				+(VB(I,J,K)-VB(I-1,J,K)) &
				/(DX(I,J)+DX(I-1,J)+DX(I,J-1)+DX(I-1,J-1)))
				YFLUX(I,J,K)=YFLUX(I,J,K) &
				-DT(I,J)*AAM(I,J,K)*2.E0*(VB(I,J+1,K)-VB(I,J,K))/DY(I,J)
!
				XFLUX(I,J,K) = &
				.25E0*(DY(I,J)+DY(I-1,J)+DY(I,J-1)+DY(I-1,J-1))*XFLUX(I,J,K)
				YFLUX(I,J,K)=DX(I,J)*YFLUX(I,J,K)
			END DO
		END DO
      END DO
!
!********** DO HORIZ. ADVECTION ************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				ADVY(I,J,K)= &
				+XFLUX(I+1,J,K)-XFLUX(I,J,K) &
                +YFLUX(I,J,K)-YFLUX(I,J-1,K)
            END DO
        END DO
      END DO
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				ADVY(I,J,K)=ADVY(I,J,K) &
				+ARV(I,J)*.25*(CURV(I,J,K)*DT(I,J)*(U(I+1,J,K)+U(I,J,K)) &
                +CURV(I,J-1,K)*DT(I,J-1)*(U(I+1,J-1,K)+U(I,J-1,K)))
            END DO
        END DO
	  END DO
!
      RETURN
      END SUBROUTINE ADVCT
!               
      SUBROUTINE ADVU(DRHOX,ADVX,DTI2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'   
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: DRHOX, ADVX
      REAL(KIND=DP) :: DTI2
      INTEGER :: I,J,K
!
!  Do vertical advection
      DO K=1,KB  
		DO J=1,JM
			DO I=1,IM
				UF(I,J,K)=0.
			END DO
		END DO
	  END DO
      DO K=2,KBM1
		DO J=1,JM
			DO I=2,IM
				UF(I,J,K)=.25E0*(W(I,J,K)+W(I-1,J,K))*(U(I,J,K)+U(I,J,K-1))
			END DO
		END DO
	  END DO
!****COMBINE HOR. and VERT. ADVECTION with
!           -FVD + GDEG/DX + BAROCLINIC TERM **********************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				UF(I,J,K)=ADVX(I,J,K)+(UF(I,J,K)-UF(I,J,K+1))*ARU(I,J)/DZ(K) &
                -ARU(I,J)*.25*(COR(I,J)*DT(I,J)*(V(I,J+1,K)+V(I,J,K)) &
                +COR(I-1,J)*DT(I-1,J)*(V(I-1,J+1,K)+V(I-1,J,K))) &
                +GRAV*.25E0*(DT(I,J)+DT(I-1,J)) &
                *.5*(EGF(I,J)-EGF(I-1,J)+EGB(I,J)-EGB(I-1,J)) &
                *(DY(I,J)+DY(I-1,J)) &
                +DRHOX(I,J,K)
            END DO
        END DO
      END DO
!******* STEP FORWARD IN TIME ***********************************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				UF(I,J,K)= &
			    ((H(I,J)+ETB(I,J)+H(I-1,J)+ETB(I-1,J))*ARU(I,J)*UB(I,J,K) &
                -2.E0*DTI2*UF(I,J,K)) &
                /((H(I,J)+ETF(I,J)+H(I-1,J)+ETF(I-1,J))*ARU(I,J))
             END DO
        END DO
	  END DO
      RETURN
      END SUBROUTINE ADVU
!               
      SUBROUTINE ADVV(DRHOY,ADVY,DTI2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'   
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: DRHOY, ADVY                    
      REAL(KIND=DP) :: DTI2
      INTEGER :: I,J,K
!
!  Do vertical advection
      DO K=1,KB  
		DO J=1,JM
			DO I=1,IM
				VF(I,J,K)=0.
			END DO
		END DO
	  END DO
      DO K=2,KBM1
		DO J=2,JM
			DO I=1,IM
				VF(I,J,K)=.25*(W(I,J,K)+W(I,J-1,K))*(V(I,J,K)+V(I,J,K-1))
			END DO
		END DO
	  END DO
!****COMBINE HOR. and VERT. ADVECTION with
!           +FUD + GDEG/DY + BAROCLINIC TERM **********************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				VF(I,J,K)=ADVY(I,J,K)+(VF(I,J,K)-VF(I,J,K+1))*ARV(I,J)/DZ(K) &
                +ARV(I,J)*.25*(COR(I,J)*DT(I,J)*(U(I+1,J,K)+U(I,J,K)) &
                +COR(I,J-1)*DT(I,J-1)*(U(I+1,J-1,K)+U(I,J-1,K))) &
                +GRAV*.25E0*(DT(I,J)+DT(I,J-1)) &
                *.5*(EGF(I,J)-EGF(I,J-1)+EGB(I,J)-EGB(I,J-1)) &
                *(DX(I,J)+DX(I,J-1)) &
                +DRHOY(I,J,K)
             END DO
        END DO
      END DO
!******* STEP FORWARD IN TIME ***************************************
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				VF(I,J,K)= &
				((H(I,J)+ETB(I,J)+H(I,J-1)+ETB(I,J-1))*ARV(I,J)*VB(I,J,K) &
                -2.E0*DTI2*VF(I,J,K)) &
				/((H(I,J)+ETF(I,J)+H(I,J-1)+ETF(I,J-1))*ARV(I,J))
			END DO
		END DO
      END DO
      RETURN
      END SUBROUTINE ADVV

!
      
!               

!               
      SUBROUTINE EPRXYZ(LABEL,TIME,A,IM,ISKP,JM,JSKP,KB)
      USE double
      IMPLICIT NONE
      REAL(KIND=DP) :: TIME, SCALE
      INTEGER :: IM, ISKP, JM, JSKP, KB, I, IB, IE, J, JWR, K, &
          KEND, KM 
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: A
      INTEGER, DIMENSION(4) :: KP
      INTEGER, DIMENSION(300) :: NUM
      CHARACTER LABEL*(*)
      DATA KP/1,2,11,12/
      KP(3)=KB-1
      KP(4)=KB
      KEND=4
      IF(KB.LE.4) KEND=KB
!
!        THIS SUBROUTINE WRITES HORIZONTAL LAYERS OF A 3-D FIELD
!        WITH FLOATING POINT NUMBERS.
!
!      TIME=TIME IN DAYS
!      A= ARRAY(IM,JM,KB) TO BE PRINTED
!      ISPL=PRINT SKIP FOR I
!      JSPL=PRINT SKIP FOR J
!
      SCALE=1.
      WRITE(6,9980) LABEL
 9980 FORMAT(' ',A30)
      WRITE(6,9981) TIME,SCALE
 9981 FORMAT(' TIME = ',F8.2,'    MULTIPLY ALL VALUES BY',F8.4)
      DO KM=1,KEND 
		  K=KP(KM)
		  WRITE(6,30) K
	 30   FORMAT(3X,/7H LAYER ,I2)
		  IB=1
		  IE=IB+11*ISKP
	 
	  50  CONTINUE
		  IF(IE.GT.IM) IE=IM
		  DO I=IB,IE,ISKP
			NUM(I)=I
		  END DO
		  WRITE(6,9990) (NUM(I),I=IB,IE,ISKP)
	 9990 FORMAT(/,12I10,/)
		  DO J=1,JM,JSKP
			JWR=JM+1-J
			WRITE(6,9966) JWR,(A(I,JWR,K),I=IB,IE,ISKP)
		  END DO
		  WRITE(6,9984)
		  IF(IE.EQ.IM) EXIT
		  IB=IB+12*ISKP
		  IE=IB+11*ISKP
		  GO TO 50
	 9966 FORMAT(1X,I2,12(E10.2))
	 9984 FORMAT(//)
      END DO
      RETURN
      END SUBROUTINE EPRXYZ
!                  
 
!                  
 
      SUBROUTINE SLPMIN(H,IM,JM,FSM,SL)
      USE double
      IMPLICIT NONE
      INTEGER :: IM, JM, I, J, LOOP
      REAL(KIND=DP) :: DH, SLMIN, SN
      REAL(KIND=DP), DIMENSION(IM,JM) :: H, FSM, SL
!     This subroutine limits the maximum difference in H divided
!     by twice the average of H of adjacent cells.
!     The maximum possible value is unity.
!     
      SLMIN=0.2
!
      DO LOOP=1,10
!    sweep right
	      DO J=2,JM-1
		    DO I=2,IM-1
			  IF(FSM(I,J).EQ.0..OR.FSM(I+1,J).EQ.0.) CYCLE
			  SL(I,J)=ABS(H(I+1,J)-H(I,J))/(H(I,J)+H(I+1,J))
			  IF(SL(I,J).LT.SLMIN) CYCLE
			  DH=0.5*(SL(I,J)-SLMIN)*(H(I,J)+H(I+1,J))
			  SN=-1.
			  IF(H(I+1,J).GT.H(I,J)) SN=1.
			  H(I+1,J)=H(I+1,J)-SN*DH
			  H(I,J)=H(I,J)+SN*DH
		    END DO
	!    sweep left
		    DO I=IM-1,2,-1
			  IF(FSM(I,J).EQ.0..OR.FSM(I+1,J).EQ.0.) CYCLE
			  SL(I,J)=ABS(H(I+1,J)-H(I,J))/(H(I,J)+H(I+1,J))
			  IF(SL(I,J).LT.SLMIN) CYCLE
			  DH=0.5*(SL(I,J)-SLMIN)*(H(I,J)+H(I+1,J))
			  SN=-1.
			  IF(H(I+1,J).GT.H(I,J)) SN=1.
			  H(I+1,J)=H(I+1,J)-SN*DH
			  H(I,J)=H(I,J)+SN*DH
		    END DO
	      END DO
	!   sweep up     
	      DO I=2,IM-1
		    DO J=2,JM-1
			IF(FSM(I,J).EQ.0..OR.FSM(I,J+1).EQ.0.) CYCLE
			SL(I,J)=ABS(H(I,J+1)-H(I,J))/(H(I,J)+H(I,J+1))
			IF(SL(I,J).LT.SLMIN) CYCLE
			DH=0.5*(SL(I,J)-SLMIN)*(H(I,J)+H(I,J+1))
			SN=-1.
			IF(H(I,J+1).GT.H(I,J)) SN=1.
			H(I,J+1)=H(I,J+1)-SN*DH
			H(I,J)=H(I,J)+SN*DH
		    END DO
		!   sweep down
		    DO J=JM-1,2,-1
			IF(FSM(I,J).EQ.0..OR.FSM(I,J+1).EQ.0.) CYCLE
			SL(I,J)=ABS(H(I,J+1)-H(I,J))/(H(I,J)+H(I,J+1))
			IF(SL(I,J).LT.SLMIN) CYCLE
			DH=0.5*(SL(I,J)-SLMIN)*(H(I,J)+H(I,J+1))
			SN=-1.
			IF(H(I,J+1).GT.H(I,J)) SN=1.
			H(I,J+1)=H(I,J+1)-SN*DH
			H(I,J)=H(I,J)+SN*DH
		    END DO
	      END DO
!
      END DO 
      RETURN
      END SUBROUTINE SLPMIN
!
      SUBROUTINE PRXZ(LABEL,A,IM,ISKP,JM,J1,J2,KB,SCALA,DT,ZZ)
      USE double
      IMPLICIT NONE
      INTEGER :: IM, ISKP, JM, J1, J2, KB, I, J, K, IB, IE, JPP
      REAL(KIND=DP) :: SCALA, AMX, SCALE, SCALEI
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: A
      INTEGER, DIMENSION(100) :: NUM, LINE, IDT
      REAL(KIND=DP), DIMENSION(KB) :: ZZ
      REAL(KIND=DP), DIMENSION(IM,JM) :: DT
      CHARACTER LABEL*(*)
      REAL(KIND=DP), PARAMETER :: ZERO = 1.E-10
      
!
!        THIS SUBROUTINE WRITES VERTICAL SECTIONS OF A 3-D FIELD
!
!      TIME=TIME IN DAYS
!      A= ARRAY(IM,JM,KB) TO BE PRINTED
!      ISPL=PRINT SKIP FOR I
!      JSPL=PRINT SKIP FOR J
!      SCALE=DIVISOR FOR VALUES OF A
!
      SCALE=SCALA
      IF (SCALE.GT.ZERO) GO TO 150
      AMX=ZERO
      DO K=1,KB
		DO J=1,JM
			DO I=1,IM,ISKP
				AMX=MAX(ABS(A(I,J,K)),AMX)
			END DO
		END DO
	  END DO
      IF (AMX.EQ.0.) THEN   
       SCALEI=0.   
       GOTO 165  
      ENDIF    
      SCALE=10.E0**(INT(LOG10(AMX)+1.E2)-103)
  150 CONTINUE
      SCALEI=1./SCALE
  165 CONTINUE
      WRITE(6,9980) LABEL
 9980 FORMAT(' ',A30)
!      WRITE(6,9981) TIME,SCALE
! 9981 FORMAT(' TIME = ',F9.3,' DAYS    MULTIPLY ALL VALUES BY ',1PE8.2)
!23456   |         |         |         |         |         |         |  xxxxxxx|
      DO JPP=1,2
		  J=J1
		  IF(JPP.EQ.2) J=J2
		  IF(J.EQ.0) RETURN
		  WRITE(6,30) J
	 30   FORMAT(3X,/' SECTION J =',I3)
		  IB=1
		  IE=IB+23*ISKP
	  50  CONTINUE
		  IF(IE.GT.IM) IE=IM
		  DO I=IB,IE,ISKP
			IDT(I)=INT(DT(I,J))
			NUM(I)=I
		  END DO
		  WRITE(6,9990) (NUM(I),I=IB,IE,ISKP)
	 9990 FORMAT(/,'    I =  ',24I5,/)
		  WRITE(6,9991) (IDT(I),I=IB,IE,ISKP)
	 9991 FORMAT(8X,'D =',24I5.0,/,'       ZZ ')
		  DO K=1,KB
			  DO I=IB,IE,ISKP
				LINE(I)=INT(SCALEI*A(I,J,K))
			  END DO
			  WRITE(6,9966) K,ZZ(K),(LINE(I),I=IB,IE,ISKP)
		 9966 FORMAT(1X,I2,2X,F6.3,24I5)
		  END DO
		  WRITE(6,9984)
		  IF(IE.EQ.IM) CYCLE
		  IB=IB+24*ISKP
		  IE=IB+23*ISKP
		  GO TO 50
	 9984 FORMAT(//)
      END DO
      RETURN
      END SUBROUTINE PRXZ
!               
      SUBROUTINE PRYZ(LABEL,A,IM,ISKP,JM,JSKP,KB,SCALA,DT,ZZ)
      USE double
      IMPLICIT NONE
      INTEGER :: IM, ISKP, JM, JSKP, KB, I, J, K, IPP, JB, JE
      REAL(KIND=DP) :: SCALA, AMX, SCALE, SCALEI
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: A
      INTEGER, DIMENSION(200) :: NUM
      REAL(KIND=DP), DIMENSION(200) :: PLINE 
      REAL(KIND=DP), DIMENSION(IM,JM) :: DT
      REAL(KIND=DP), DIMENSION(KB) :: ZZ
      INTEGER, DIMENSION(2) :: IP
      CHARACTER LABEL*(*)
      REAL(KIND=DP), PARAMETER :: ZERO = 1.E-10
      IP(1)=IM/2
      IP(2)=IM-3
!
!        THIS SUBROUTINE WRITES VERTICAL SECTIONS OF A 3-D FIELD
!
!      TIME=TIME IN DAYS
!      A= ARRAY(IM,JM,KB) TO BE PRINTED
!      ISPL=PRINT SKIP FOR I
!      JSPL=PRINT SKIP FOR J
!      SCALE=DIVISOR FOR VALUES OF A
!
      SCALE=SCALA
      IF (SCALE.GT.ZERO) GO TO 150
      AMX=ZERO
      DO K=1,KB
		DO J=1,JM,JSKP
			DO I=1,IM,ISKP
				AMX=MAX(ABS(A(I,J,K)),AMX)
			END DO
		END DO
      END DO
      IF(AMX.EQ.0.) THEN   
       SCALEI=0.   
       GOTO 165  
      ENDIF    
      SCALE=10.E0**(INT( LOG10(AMX)+1.E2)-103)
  150 CONTINUE
      SCALEI=1./SCALE            
  165 CONTINUE
      WRITE(6,9980) LABEL
 9980 FORMAT(' ',A30)
!      WRITE(6,9981) TIME,SCALE
! 9981 FORMAT(' TIME = ',F8.2,'    MULTIPLY ALL VALUES BY',E8.2)
      DO IPP=1,2
		  I=IP(IPP)
		  WRITE(6,30) I
	 30   FORMAT(3X,/' SECTION I =',I3)
		  JB=1
		  JE=JB+23*JSKP
	  50  CONTINUE
		  IF(JE.GT.JM) JE=JM
		  DO J=JB,JE,JSKP
			NUM(J)=J
	          END DO
		  WRITE(6,9990) (NUM(J),J=JB,JE,JSKP)
	 9990 FORMAT(/,'    J =  ',24I5,/)
		  WRITE(6,9991) (DT(I,J),J=JB,JE,JSKP)
	 9991 FORMAT(7X,'D =',24F5.0,/,'       ZZ ')
		  DO K=1,KB
			  DO J=JB,JE,JSKP
				PLINE(J)=SCALEI*A(I,J,K)
			  END DO
			  WRITE(6,9966) K,ZZ(K),(PLINE(J),J=JB,JE,JSKP)
		 9966 FORMAT(1X,I2,2X,F6.3,24(F5.1))
	          END DO
		  WRITE(6,9984)
		  IF(JE.EQ.JM) EXIT
		  JB=JB+24*JSKP
		  JE=JB+23*JSKP
		  GO TO 50
	 9984 FORMAT(//)
      END DO
      RETURN
      END SUBROUTINE PRYZ

    
!             
      SUBROUTINE VERTVL(DTI2)
!     INCLUDE 'comblk98.h'
      USE BLK1D    
      USE BLK2D
      USE BLK3D
      USE PROM
      IMPLICIT NONE
      REAL(KIND=DP) :: DTI2
      INTEGER :: I, J, K
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: XFLUX, YFLUX
!      EQUIVALENCE (XFLUX,A),(YFLUX,C)
      XFLUX => A
      YFLUX => C
!
!
! CALCULATE NEW VERTICAL VELOCITY
!
! REESTABLISH BOUNDARY CONDITIONS
      DO K=1,KBM1
		DO J=2,JM
			DO I=2,IM
				XFLUX(I,J,K) =.25E0*(DY(I,J)+DY(I-1,J))*(DT(I,J)+DT(I-1,J))*U(I,J,K)
			END DO
		END DO
      END DO
     
      DO K=1,KBM1
		DO J=2,JM
			DO I=1,IM
				YFLUX(I,J,K) =.25E0*(DX(I,J)+DX(I,J-1))*(DT(I,J)+DT(I,J-1))*V(I,J,K)
			END DO
		END DO
	  END DO
!
      DO J=1,JM
		DO I=1,IM
      		W(I,J,1)=0.E0
      	END DO
	  END DO
      DO K=1,KBM1
		DO J=2,JMM1
			DO I=2,IMM1
				W(I,J,K+1)=W(I,J,K) &
				+DZ(K)*((XFLUX(I+1,J,K)-XFLUX(I,J,K) &
                +YFLUX(I,J+1,K)-YFLUX(I,J,K))/(DX(I,J)*DY(I,J)) &
                +(ETF(I,J)-ETB(I,J))/DTI2 )
            END DO
        END DO
      END DO
      RETURN
      END SUBROUTINE VERTVL
