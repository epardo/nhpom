	
      SUBROUTINE BAROPG(DRHOX,DRHOY)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'   
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: DRHOX, DRHOY
      INTEGER :: I, J, K
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
 			RHO(I,J,K)=RHO(I,J,K)
 		END DO
 	END DO
      END DO
! -rmean(i,j,k)
!
!               X COMPONENT OF BAROCLINIC PRESSURE GRADIENT
!
      DO J=2,JMM1
      	DO I=2,IMM1
  		DRHOX(I,J,1)=.5E0*GRAV*(-ZZ(1))*(DT(I,J)+DT(I-1,J)) &
     		*(RHO(I,J,1)-RHO(I-1,J,1))
     	END DO
      END DO
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
 			DRHOX(I,J,K)=DRHOX(I,J,K-1) &
     			+GRAV*.25E0*(ZZ(K-1)-ZZ(K))*(DT(I,J)+DT(I-1,J)) &
     			*(RHO(I,J,K)-RHO(I-1,J,K)+RHO(I,J,K-1)-RHO(I-1,J,K-1)) &
     			+GRAV*.25E0*(ZZ(K-1)+ZZ(K))*(DT(I,J)-DT(I-1,J)) &
     			*(RHO(I,J,K)+RHO(I-1,J,K)-RHO(I,J,K-1)-RHO(I-1,J,K-1))
     		END DO
     	END DO
      END DO
!
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
 			DRHOX(I,J,K)=.25E0*(DT(I,J)+DT(I-1,J))*DRHOX(I,J,K)*DUM(I,J) &
     			*(DY(I,J)+DY(I-1,J))
     		END DO
     	END DO
      END DO
!
!               Y COMPONENT OF BAROCLINIC PRESSURE GRADIENT
!
      DO J=2,JMM1
      	DO I=2,IMM1
 		DRHOY(I,J,1)=.5E0*GRAV*(-ZZ(1))*(DT(I,J)+DT(I,J-1)) &
     		*(RHO(I,J,1)-RHO(I,J-1,1))
     	END DO
      END DO
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
 			DRHOY(I,J,K)=DRHOY(I,J,K-1) &
     			+GRAV*.25E0*(ZZ(K-1)-ZZ(K))*(DT(I,J)+DT(I,J-1)) &
     			*(RHO(I,J,K)-RHO(I,J-1,K)+RHO(I,J,K-1)-RHO(I,J-1,K-1)) &
     			+GRAV*.25E0*(ZZ(K-1)+ZZ(K))*(DT(I,J)-DT(I,J-1)) &
     			*(RHO(I,J,K)+RHO(I,J-1,K)-RHO(I,J,K-1)-RHO(I,J-1,K-1))
     		END DO
     	END DO
      END DO
!
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
 			DRHOY(I,J,K)=.25E0*(DT(I,J)+DT(I,J-1))*DRHOY(I,J,K)*DVM(I,J) &
     			*(DX(I,J)+DX(I,J-1))
     		END DO
     	END DO
      END DO
!
      DO K=1,KB
      	DO J=2,JMM1
      		DO I=2,IMM1
      			DRHOX(I,J,K)=DRHOX(I,J,K)
 			DRHOY(I,J,K)=DRHOY(I,J,K)
 		END DO
 	END DO
      END DO
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
 			RHO(I,J,K)=RHO(I,J,K)
 		END DO
 	END DO
      END DO
! +rmean(i,j,k)
!
      RETURN
      END SUBROUTINE BAROPG
!
      SUBROUTINE DENS(SI,TI,RHOO)
      USE BLK1D
      USE BLK2D
      USE BLKCON
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: SI, TI, RHOO
      REAL(KIND=DP) tr,sr,p,rhor,cr,tr2,tr3,tr4
!       If using 32 bit precision, it is recommended that
!       TR, SR, P, RHOR , CR be made double precision.
!       and the E's in the constants should be changed 
!       to D's.
!
!         THIS SUBROUTINE COMPUTES (DENSITY- 1000.)/RHOREF
!         T = POTENTIAL TEMPERATURE
!    ( See: Mellor, 1991, J. Atmos. Oceanic Tech., 609-611)
!
      INTEGER :: I, J, K
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			TR=TI(I,J,K)+TBIAS
      			SR=SI(I,J,K)+SBIAS
      			TR2=TR*TR
      			TR3=TR2*TR
      			TR4=TR3*TR
!            Approximate pressure in units of bars
      			P=-GRAV*RHOREF*ZZ(K)*DT(I,J)*1.E-5 
!
      			RHOR = -0.157406  + 6.793952E-2*TR &
     			- 9.095290E-3*TR2 + 1.001685E-4*TR3 &
     			- 1.120083E-6*TR4 + 6.536332E-9*TR4*TR
!
      			RHOR = RHOR + (0.824493 - 4.0899E-3*TR &
     			+ 7.6438E-5*TR2 - 8.2467E-7*TR3 &
     			+ 5.3875E-9*TR4) * SR &
     			+ (-5.72466E-3 + 1.0227E-4*TR &
     			- 1.6546E-6*TR2) * ABS(SR)**1.5 &
     			+ 4.8314E-4 * SR*SR
!
      			CR=1449.1+.0821*P+4.55*TR-.045*TR2 &
     			+1.34*(SR-35.)
      			RHOR=RHOR + 1.E5*P/(CR*CR)*(1.-2.*P/(CR*CR))
!
      			RHOO(I,J,K)=RHOR/RHOREF*FSM(I,J)
      		END DO
      	END DO
      END DO
!
      RETURN
      END SUBROUTINE DENS
!   

      SUBROUTINE PROFT(rF,DT2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'
      INTEGER :: I, J, K, KI
      REAL(KIND=DP) :: DT2, UMOLPR
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: RF, umolp, RAD 
      REAL(KIND=DP), DIMENSION(:, :), POINTER :: DH
!
! Irradiance parameters after Paulson and Simpson, JPO, 1977, 952-956.

      UMOLPR=UMOL

      DH => TPS

      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
  			RAD(I,J,K)=0.
  		END DO
  	END DO
      END DO
!***********************************************************************
!                                                                      *
!        THE FOLLOWING SECTION SOLVES THE EQUATION                     *
!         DTI2*(KH*F')'-F=-FB                                          *
!                                                                      *
!***********************************************************************
      DO J=1,JM
      	DO I=1,IM
      		DH(I,J)=H(I,J)+ETF(I,J)
      	END DO
      END DO
      
      DO K=2,KB
      	DO J=1,JM
      		DO I=1,IM	
      			A(I,J,K-1)=-DT2*(KH(I,J,K)+UMOLP(i,j,k))/(DZz(K-1)*DZZ(K-1) &
			*DH(I,J)*DH(I,J))
      			C(I,J,K)=-DT2*(KH(I,J,K)+UMOLP(i,j,k))/(DZz(K)*DZZ(K-1)*DH(I,J) &
     			*DH(I,J))
!			c(i,j,kb)=c(i,j,kbm1)
		END DO
	END DO
      END DO

      DO J=1,JM
      	DO I=1,IM
      		EE(I,J,1)=1.
      		GG(I,J,1)=0. 
      	END DO
      END DO
    
      DO K=2,KBm1
      	DO J=1,JM
      		DO I=1,IM
      			GG(I,J,K)=1./(A(I,J,K)+C(I,J,K)*(1.-EE(I,J,K-1))-1.)
      			EE(I,J,K)=A(I,J,K)*GG(I,J,K)
      			GG(I,J,K)=(C(I,J,K)*GG(I,J,K-1)-rF(I,J,K) &
     			+DT2*(RAD(I,J,K)-RAD(I,J,K+1))/(DH(I,J)*DZ(K)))*GG(I,J,K)
     		END DO
     	END DO
      END DO
!-----  BOTTOM ADIABATIC B.!. ------------------------------------------
      DO J=1,JM
      	DO I=1,IM
		rF(I,J,KB)=gg(i,j,kb-1)/(1-ee(i,j,kb-1))
	END DO
      END DO
!----------------------------------------------------------------------
      DO K=1,KBM1
      	KI=KB-K
      	DO J=1,JM
      		DO I=1,IM
      			rF(I,J,KI)=(EE(I,J,KI)*rF(I,J,KI+1)+GG(I,J,KI))
      		END DO
      	END DO
      END DO

      RF(:,:,KB)=RF(:,:,KBM1)
!
      RETURN
      END SUBROUTINE PROFT

      SUBROUTINE ADVT(FB,F, DTI2,FF)
!
!     THIS SUBROUTINE INTEGRATES CONSERVATIVE SCALAR EQUATIONS
!
!     INCLUDE 'comblk98.h'   
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: FB, F, FF
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: XFLUX, YFLUX
      REAL(KIND=DP) :: DTI2
      INTEGER :: I, J, K
      !EQUIVALENCE (XFLUX,A),(YFLUX,C)
      XFLUX => A
      YFLUX => C

!
      DO J=1,JM
      	DO I=1,IM
      		F(I,J,KB)=F(I,J,KBM1)
 		FB(I,J,KB)=FB(I,J,KBM1)
 	END DO
      END DO
!
!******* DO ADVECTION FLUXES **************************************
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IM
      			XFLUX(I,J,K)=.25E0*((DT(I,J)+DT(I-1,J)) &
     			*(F(I,J,K)+F(I-1,J,K))*U(I,J,K))
 			YFLUX(I,J,K)=.25E0*((DT(I,J)+DT(I,J-1)) &
     			*(F(I,J,K)+F(I,J-1,K))*V(I,J,K))
     		END DO
     	END DO
      END DO
!******  ADD DIFFUSIVE FLUXES *************************************
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
  			FB(I,J,K)=FB(I,J,K)
  		END DO
  	END DO
      END DO
!
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IM
      			XFLUX(I,J,K)=XFLUX(I,J,K) &
     			-.5E0*(AAM(I,J,K)+AAM(I-1,J,K))*(H(I,J)+H(I-1,J))*TPRNI &
     			*(FB(I,J,K)-FB(I-1,J,K))*DUM(I,J)/(DX(I,J)+DX(I-1,J))
      			YFLUX(I,J,K)=YFLUX(I,J,K) &
     			-.5E0*(AAM(I,J,K)+AAM(I,J-1,K))*(H(I,J)+H(I,J-1))*TPRNI &
      			*(FB(I,J,K)-FB(I,J-1,K))*DVM(I,J)/(DY(I,J)+DY(I,J-1))
      			XFLUX(I,J,K)=.5E0*(DY(I,J)+DY(I-1,J))*XFLUX(I,J,K)
      			YFLUX(I,J,K)=.5E0*(DX(I,J)+DX(I,J-1))*YFLUX(I,J,K)
      		END DO
      	END DO
      END DO
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
 			FB(I,J,K)=FB(I,J,K)
 		END DO
 	END DO
      END DO
!
!****** DO VERTICAL ADVECTION *************************************
      DO J=2,JMM1
      	DO I=2,IMM1
 		FF(I,J,1)=-.5*(F(I,J,1)+F(I,J,2))*W(I,J,2)*ART(I,J)/DZ(1)
 	END DO
      END DO
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
 			FF(I,J,K)=.5*((F(I,J,K-1)+F(I,J,K))*W(I,J,K) &
     			-(F(I,J,K)+F(I,J,K+1))*W(I,J,K+1))*ART(I,J)/DZ(K)
     		END DO
     	END DO
      END DO
!****** ADD NET HORIZONTAL FLUXES; THEN STEP FORWARD IN TIME **********
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
      			FF(I,J,K)=FF(I,J,K) &
     			+XFLUX(I+1,J,K)-XFLUX(I,J,K) &
     			+YFLUX(I,J+1,K)-YFLUX(I,J,K)
      			FF(I,J,K)=(FB(I,J,K)*(H(I,J)+ETB(I,J))*ART(I,J)-DTI2*FF(I,J,K)) &
     			/((H(I,J)+ETF(I,J))*ART(I,J))
     		END DO
     	END DO
      END DO  
      RETURN
      END SUBROUTINE ADVT
