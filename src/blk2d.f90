MODULE BLK2D
	USE  PARAMETERS
	USE double 
        REAL(KIND=DP), DIMENSION(IM, JM), SAVE ::  H, DX, DY, D, DT, &
                ART, ARU, ARV, CBC, ALON, ALAT, x, y, &
                DUM, DVM, FSM, COR, &
                WUSURF, WVSURF, WUBOT, WVBOT, &
                WTSURF, WSSURF, AAM2D, &
                UAF, UA, UAB, VAF, VA, &
                VAB, ELF, EL, ELB, &
                ETF, ET, ETB, FLUXUA, FLUXVA, &
                EGF, EGB
	REAL(KIND=DP), DIMENSION(IM, JM), TARGET :: TPS
END MODULE BLK2D
