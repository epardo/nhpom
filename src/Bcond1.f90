      SUBROUTINE BCOND1(IDX)
      USE BLK2D
      USE BLK3D
      USE double
      IMPLICIT NONE

!     INCLUDE 'comblk98.h'   

      REAL(KIND=DP) :: vel
      INTEGER :: IDX, I, J, K
      REAL(KIND=DP) :: GA, HMAX

      REAL(KIND=DP), PARAMETER :: PI=3.14167E0, GEE=9.807E0
!
      GO TO (10,20,30,40,50,60), IDX
!
!-----------------------------------------------------------------------
!                   EXTERNAL B.!.'S
!  In this example the governing boundary conditions are a radition   
!  condition on UAF(IM,J) in the east and an inflow UAF(2,J) in the
!  west. The tangential velocities are set to zero on both boundaries.
!  These are only set of possibilities and may not represent a  choice
!  which yields the most physically realistic result.
!-----------------------------------------------------------------------
!
!

 10   CONTINUE
!---------ELEVATION --------------------
!  In this application, elevation is not a primary B.!.     
	vel=0.
      DO J=1,JM
	ELF(1,J)=ELF(2,J)
	ELF(IM,J)=ELF(IMM1,J)
      END DO
      DO I=1,IM
	ELF(I,1)=ELF(I,2)
	ELF(I,JM)=ELF(I,JMM1)
      END DO
      DO J=1,JM
	DO I=1,IM
	    ELF(I,J)=ELF(I,J)
	END DO
      END DO
      RETURN
!
!
 20   CONTINUE
!---------- VELOCITY --------------        
      DO J=2,JMM1
!--- Governing B.!.s -------------
	UAF(2,J)=vel
!     1/(H(2,J)+ELF(2,J)+H(1,J)+ELF(1,J))
		
	UAF(IM,J)=0.
!	uaf(imm1,j)
!	dte*SQRT(GRAV/H(IM,J))/dx(im,j)*ua(imm1,j)+
!	1(1.-dte*SQRT(GRAV/H(IM,J))/dx(im,j))*ua(im,j)

	VAF(1,J)=0.0
	VAF(IM,J)=0.0
!---------------------------
	UAF(1,J)=UAF(2,J)
      END DO
!
      DO J=1,JM
      DO I=1,IM
	    UAF(I,J)=UAF(I,J)*DUM(I,J)
	    VAF(I,J)=VAF(I,J)*DVM(I,J)
	END DO
      END DO
      RETURN
!
!
 30   CONTINUE
!-----------------------------------------------------------------------
!                   INTERNAL VEL B.!.'S
!-----------------------------------------------------------------------
!         Eastern and western radiation boundary conditions. Some smoothing
!         is used in the J-direction; it may not be necessary.
!
      HMAX=10.
      DO K=1,KB
	DO J=2,JMM1
	    GA=SQRT(H(IM,J)/HMAX)    
	    UF(IM,J,K)=0.
!	uf(imm1,j,k)
!     1  =GA*U(IMM1,J,K)
!     2   +(1.E0-GA)*U(IM,J,K)
	    GA=SQRT(H(1,J)/HMAX)    
	    UF(2,J,K)=vel
!     1  =GA*(.25*U(3,J-1,K)+.5*U(3,J,K)+.25*U(3,J+1,K))
!     2   +(1.-GA)*(.25*U(2,J-1,K)+.5*U(2,J,K)+.25*U(2,J+1,K))
	    uf(1,j,k)=uf(2,j,k)
	END DO
      END DO
!
      DO k=1,kb
	DO J=1,JM
	    VF(1,J,K)=0.
	    VF(IM,J,K)=0.                                        
	END DO
      END DO
!
      DO K=1,KB
	DO J=1,JM
	    DO I=1,IM
		UF(I,J,K)=UF(I,J,K)*DUM(I,J)
		VF(I,J,K)=VF(I,J,K)*DVM(I,J)
	    END DO
	END DO
      END DO
!      
      RETURN
!
!
 40   CONTINUE
!-----------------------------------------------------------------------
!                   TEMP(UF) & SAL(VF) B.!.'S
!-----------------------------------------------------------------------
      DO K=1,KB
	DO J=1,JM
	    DO i=1,iM
		rho(1,j,k)=rho(2,j,k)
		rho(i,1,k)=rho(i,2,k)
		rho(im,j,k)=rho(imm1,j,k)
		rho(i,jm,k)=rho(i,jmm1,k)
		vf(1,j,k)=vf(2,j,k)
		vf(i,1,k)=vf(i,2,k)
		vf(im,j,k)=vf(imm1,j,k)
		vf(i,jm,k)=vf(i,jmm1,k)
		uf(1,j,k)=uf(2,j,k)
		uf(i,1,k)=uf(i,2,k)
		uf(im,j,k)=uf(imm1,j,k)
		uf(i,jm,k)=uf(i,jmm1,k)
	    END DO
	END DO
      END DO

      RETURN
!
!
 50   CONTINUE
!---------------VERTICAL VEL. B. !.'S --------------------------------
      DO K=1,KB
	DO J=1,JM
	    DO I=1,IM
		W(I,J,K)=W(I,J,K)*FSM(I,J)
	    END DO
	END DO
      END DO
!    
      RETURN
!
!
 60   CONTINUE
      DO K=1,KB
!     
	DO J=1,JM
	    UF(IM,J,K)=1.E-10
	    VF(IM,J,K)=1.E-10
	    UF(1,J,K)=1.E-10
	    VF(1,J,K)=1.E-10
	END DO
	DO J=1,JM
	    DO I=1,IM
		UF(I,J,K)=UF(I,J,K)*FSM(I,J)
		VF(I,J,K)=VF(I,J,K)*FSM(I,J)
	    END DO
	END DO
      END DO
      RETURN

      RETURN
      END
