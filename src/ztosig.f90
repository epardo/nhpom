! ------------------------------------------------------------------
      SUBROUTINE ZTOSIG(ZS,TB,ZZ,H,T,IM,JM,KS,KB)
!      INCLUDE 'comblk98.h'
!   VERTICAL INTERPOLATION FROM A Z-GRID TO A SIGMA GRID
!
      USE double
      IMPLICIT NONE
      INTEGER :: I, J, K, KS, KB, IM, JM, IPR, JPR
      REAL(KIND=DP), DIMENSION(KS) :: ZS, TIN
      REAL(KIND=DP), DIMENSION(KB) :: ZZ, ZZH, TOUT
      REAL(KIND=DP), DIMENSION(IM,JM) ::	H
      REAL(KIND=DP), DIMENSION(IM,JM,KS) :: TB 
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: T
      REAL(KIND=DP) :: TMAX
      DO I=2,IM-1
      	DO J=2,JM-1
!        IF(H(I,J).LE.1.0)GO TO 40
!       IF(H(I,J).LT.1.00001)GO TO 40
!-- special interp on z-lev for cases of no data because H smoothing
      		DO K=1,KS
      			TIN(K)=TB(I,J,K)
      			IF(ZS(K)<=H(I,J))THEN   
      				TMAX=AMAX1(TB(I-1,J,K),TB(I+1,J,K),TB(I,J-1,K),TB(I,J+1,K))
      				TIN(K)=TMAX
         		ENDIF
!      IF(K.NE.1)TIN(K)=TIN(K-1)
   		END DO
!
      		DO K=1,KB
   			ZZH(K)=-ZZ(K)*H(I,J)
   		END DO
!
!        VERTICAL SPLINE INTERP. 
      		CALL SPLINC(ZS,TIN,KS  ,2.d30,2.d30,ZZH,TOUT,KB)
!
      		IPR=IM/2
      		JPR=JM/2
!      IF(I.EQ.IPR.AND.J.EQ.JPR) THEN    
!      WRITE(6,'(//,'' Data interpolated from z to sigma grid at I,J =''
!     1    ,I5,'','',I5)') IPR,JPR
!      WRITE(6,'(''    H ='',F10.1)') H(IPR,JPR)
!      WRITE(6,'(1X,I5,4F10.4)') (K,ZS(K),TIN(K),ZZH(K),TOUT(K),K=1,KB)
!      WRITE(6,'(1X,I5,2F10.4)') (K,ZS(K),TIN(K),K=KB+1,KS)
!      ENDIF    
!
      		T(I,J,:)=TOUT(:)
   
   	END DO
      END DO
!
      RETURN 
      END
! 
      SUBROUTINE SPLINC(X,Y,N,YP1,YPN,XNEW,YNEW,M)
!
!       from "Numerical Recipes" by W.H. Press et al, Cambridge
!       Univerisity Press, 1986.
!
      USE double
      IMPLICIT NONE
      INTEGER, PARAMETER :: NMAX=100
      REAL(KIND=DP), DIMENSION(N) :: X, Y
      REAL(KIND=DP), DIMENSION(NMAX) :: Y2, U
      REAL(KIND=DP), DIMENSION(M) :: XNEW, YNEW
      INTEGER :: I, K, M, N
      REAL(KIND=DP) :: P, QN, SIG, UN, YP1, YPN
      IF (YP1.GT..99E30) THEN
        Y2(1)=0.
        U(1)=0.
      ELSE
        Y2(1)=-0.5
        U(1)=(3./(X(2)-X(1)))*((Y(2)-Y(1))/(X(2)-X(1))-YP1)
      ENDIF
      DO I=2,N-1
        SIG=(X(I)-X(I-1))/(X(I+1)-X(I-1))
        P=SIG*Y2(I-1)+2.
        Y2(I)=(SIG-1.)/P
        U(I)=(6.*((Y(I+1)-Y(I))/(X(I+1)-X(I))-(Y(I)-Y(I-1)) &
            /(X(I)-X(I-1)))/(X(I+1)-X(I-1))-SIG*U(I-1))/P
      END DO
      IF (YPN.GT..99E30) THEN
        QN=0.
        UN=0.
      ELSE
        QN=0.5
        UN=(3./(X(N)-X(N-1)))*(YPN-(Y(N)-Y(N-1))/(X(N)-X(N-1)))
      ENDIF
      Y2(N)=(UN-QN*U(N-1))/(QN*Y2(N-1)+1.)
      DO K=N-1,1,-1
        Y2(K)=Y2(K)*Y2(K+1)+U(K)
      END DO
!        
      DO I =1,M
      	CALL SPLINT(X,Y,Y2,N,XNEW(I),YNEW(I))
      END DO
      RETURN
      END SUBROUTINE SPLINC

      SUBROUTINE SPLINT(XA,YA,Y2A,N,X,Y)
      USE double
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(N) :: XA, YA, Y2A
      INTEGER :: K, KHI, KLO, N
      REAL(KIND=DP) :: A, B, H, X, Y
      KLO=1
      KHI=N
      DO WHILE (KHI-KLO.GT.1)
        K=(KHI+KLO)/2
        IF(XA(K).GT.X)THEN
          KHI=K
        ELSE
          KLO=K
        END IF
      END DO
      H=XA(KHI)-XA(KLO)
      IF (H.EQ.0.) CALL CRASH(5,0)
      A=(XA(KHI)-X)/H
      B=(X-XA(KLO))/H
      Y=A*YA(KLO)+B*YA(KHI)+ &
            ((A**3-A)*Y2A(KLO)+(B**3-B)*Y2A(KHI))*(H**2)/6.
      RETURN
      END SUBROUTINE SPLINT
! ------------------------------------------------------------------
      SUBROUTINE CRASH(ICRASH,IERR)
      IMPLICIT NONE
      INTEGER :: ICRASH, IERR
      IF(ICRASH.EQ.1)WRITE(6,10)IERR
 10   FORMAT(' SEPELI FAILED WHILE COMPUTING X SOLUTION: IERR =',I3)
      IF(ICRASH.EQ.2)WRITE(6,20)IERR
 20   FORMAT(' SEPELI FAILED WHILE COMPUTING Y SOLUTION: IERR =',I3)
      IF(ICRASH.EQ.3)WRITE(6,30)
 30   FORMAT(' WRITE ERROR WHILE OUTPUTING SOLUTION')
      IF(ICRASH.EQ.4)WRITE(6,40)
 40   FORMAT(' TRACKING ERROR IN RECT')
      IF(ICRASH.EQ.5)WRITE(6,50)
 50   FORMAT(' BAD XA INPUT IN SPLINT')
      CALL EXIT
      RETURN
      END
