	SUBROUTINE pressure1(q)
	USE BLK1D
	USE BLK2D
	USE BLK33D
	USE BLK3D
	USE BLKCON
	USE PROM
        IMPLICIT NONE
	
!	INCLUDE 'comblk98.h'
	


	REAL(KIND=DP), DIMENSION(im,jm,kb) :: q, &
	    aa1, bb1, ga1, gb1, gc1, gen(im,jm,kb), &
	    aa2, bb2, ga2, gb2, gc2, &
	    aa3, bb3, aa4, bb4
	REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: r, r1, r2
	REAL(KIND=DP), DIMENSION(im,jm) :: ddx, ddy, dq

	integer,parameter::lm=imm2*jmm2*kbm2
	INTEGER N, NELT, ISYM, ITOL, ITMAx,lz,lzz
	INTEGER ITER, IERR, IUNIT, LENW,LENIW,lapr
	REAL(KIND=DP) pok
	REAL(KIND=DP),allocatable,dimension(:)::apr,rwork
	integer,allocatable,dimension(:)::ja,ia,iwork
	REAL(KIND=DP) rr(lm),qq(lm),TOL, ERR

	integer ind(lm)
	integer i, j, k, m, ma, ma1, ma2, maa1, maa2, &
	   mb, mb1, mb2, mbb1, mbb2, mc, mga, mgb, mgc, &
	   nel, neym, nu
	
!	equivalence(r,a),(r1,c),(r2,ee)
        r => a
	r1 => c
	r2 => ee
	neym=1
	isym=0
	itol=1
	itmax=1000
        tol=0.01


	DO i=1,imm1
		DO j=1,jmm1

			ddx(i,j)=.5e0*(dx(i,j)+dx(i+1,j))
			ddy(i,j)=.5e0*(dy(i,j)+dy(i,j+1))
		END DO
	END DO

	n=lm
	maa1=imm2*jmm2+jmm2
	ma1=lm-maa1
	mbb1=imm2*jmm2+1
	mb1=lm-mbb1
	mgc=imm2*jmm2
	mc=lm-mgc
	mbb2=imm2*jmm2-1
	mb2=lm-mbb2
	maa2=imm2*jmm2-jmm2
	ma2=lm-maa2
	mga=jmm2
	ma=lm-mga
	mgb=1
	mb=lm-mgb


	nel=ma1+mb1+ma2+mb2+mc+ma+mb+lm
	nu=ma1+mb1+ma2+mb2+mc+ma+mb+lm
	nelt=nel+nu-lm

	LENIW=NEL+NU+4*N+12
	LENW=NEL+NU+8*N
	allocate(apr(nelt))
	allocate(ia(nelt))
	allocate(ja(nelt))
	allocate(iwork(leniw))
	allocate(rwork(lenw))
	DO m=1,lm
		ind(m)=m
	END DO
	DO i=1,im
		DO j=1,jm
			dq(i,j)=dtf(i,j)
		END DO
	END DO
	lzz=1
	lz=2
	call coef1(aaf,bbf,ccf,fff)
	dq(1,:)=dtf(2,:)
	dq(im,:)=dtf(imm1,:)
	dq(:,1)=dtf(:,2)
	dq(:,jm)=dtf(:,jmm1)

	m=0


		! form banded matrix of Puasson equastion
	pok=3.
	DO k=2,kbm1
		DO i=2,imm1
			DO j=2,jmm1
				IF (k+1<=kb.and.i+1<=im) THEN
					aa1(i+1,j,k+1)=.25e0*aaf(i+1,j,k+1) &
					*.5*(dy(i,j)+dy(i+1,j))/ddx(i,j)/art(i,j) &
					+ .25e0*aaf(i,j,k+1)*dq(i+1,j)/dq(i,j) &
					*dy(i,j)/ddx(i,j)/art(i,j)	
				END IF
				IF (k+1<=kb.and.i-1>=1) THEN
					aa2(i-1,j,k+1)=-.25e0*aaf(i-1,j,k+1) &
					*.5*(dy(i,j)+dy(i-1,j))/ddx(i-1,j)/art(i,j) &
					- .25e0*aaf(i,j,k+1)*dq(i-1,j)/dq(i,j) &
					*dy(i,j)/ddx(i,j)/art(i,j)	
				END IF
				IF (k-1>=1.and.i+1<=im) THEN
					aa3(i+1,j,k-1)=-.25e0*aaf(i+1,j,k-1) &
					*.5*(dy(i,j)+dy(i+1,j))/ddx(i,j)/art(i,j) &
					- .25e0*aaf(i,j,k-1)*dq(i+1,j)/dq(i,j) &
					*dy(i,j)/ddx(i,j)/art(i,j)	
				END IF
				IF (k-1>=1.and.i-1>=1) THEN
					aa4(i-1,j,k-1)=.25e0*aaf(i-1,j,k-1) &
					*.5*(dy(i,j)+dy(i-1,j))/ddx(i-1,j)/art(i,j) &
					+ .25e0*aaf(i,j,k-1)*dq(i-1,j)/dq(i,j) &
					*dy(i,j)/ddx(i,j)/art(i,j)	
				END IF
				IF (k+1<=kb.and.j+1<=jm) THEN
					bb1(i,j+1,k+1)=.25e0*bbf(i,j+1,k+1) &
					*.5*(dx(i,j)+dx(i,j+1))/ddy(i,j)/art(i,j) &
					+ .25e0*bbf(i,j,k+1)*dq(i,j+1)/dq(i,j) &
					*dx(i,j)/ddy(i,j)/art(i,j)	
				END IF
				IF (k+1<=kb.and.j-1>=1) THEN
					bb2(i,j-1,k+1)=-.25e0*bbf(i,j-1,k+1) &
					*.5*(dx(i,j)+dx(i,j-1))/ddy(i,j-1)/art(i,j) &
					- .25e0*bbf(i,j,k+1)*dq(i,j-1)/dq(i,j) &
					*dx(i,j)/ddy(i,j)/art(i,j)	
				END IF
				IF (k-1>=1.and.j+1<=jm) THEN
					bb3(i,j+1,k-1)=-.25e0*bbf(i,j+1,k-1) &
					*.5*(dx(i,j)+dx(i,j+1))/ddy(i,j)/art(i,j) &
					- .25e0*bbf(i,j,k-1)*dq(i,j+1)/dq(i,j) &
					*dx(i,j)/ddy(i,j)/art(i,j)	
				END IF
				IF (k-1>=1.and.j-1>=1) THEN
					bb4(i,j-1,k-1)=.25e0*bbf(i,j-1,k-1) &
					*.5*(dx(i,j)+dx(i,j-1))/ddx(i,j-1)/art(i,j) &
					+ .25e0*bbf(i,j,k-1)*dq(i,j-1)/dq(i,j) &
					*dx(i,j)/ddy(i,j)/art(i,j)	
				END IF
				IF (i+1<=im) THEN
					ga1(i+1,j,k)=dz(k)*dq(i+1,j) &
					*.5*(dy(i,j)+dy(i+1,j))/ddx(i,j)/art(i,j)
				END IF
				IF (i-1>=1) THEN
					ga2(i-1,j,k)=dz(k)*dq(i-1,j) &
					*.5*(dy(i,j)+dy(i-1,j))/ddx(i-1,j)/art(i,j)
				END IF
				IF (j+1<=jm) THEN
					gb1(i,j+1,k)=dz(k)*dq(i,j+1) &
					*.5*(dx(i,j)+dx(i,j+1))/ddy(i,j)/art(i,j)
				END IF
				IF (j-1>=1) THEN
					gb2(i,j-1,k)=dz(k)*dq(i,j-1) &
					*.5*(dx(i,j)+dx(i,j-1))/ddy(i,j-1)/art(i,j)
				END IF
				IF (k+1<=kb) THEN
					gc1(i,j,k+1)=1.e0/(dzz(k)*dq(i,j))* &
					(art(i,j)+.5*(aaf(i,j,k+1)+aaf(i,j,k))*aaf(i,j,k+1)*dy(i,j)/dx(i,j) &
     					+ .5*(bbf(i,j,k+1)+bbf(i,j,k))*bbf(i,j,k+1)*dx(i,j)/dy(i,j) &
					)/art(i,j)
				END IF
				IF (k-1>=1) THEN
					gc2(i,j,k-1)=1.e0/(dzz(k-1)*dq(i,j))* &
					(art(i,j)+.5*(aaf(i,j,k-1)+aaf(i,j,k))*aaf(i,j,k-1)*dy(i,j)/dx(i,j) &
					+ .5*(bbf(i,j,k-1)+bbf(i,j,k))*bbf(i,j,k-1)*dx(i,j)/dy(i,j) &
					)/art(i,j)
				END IF
!	if(iint==5)stop
				IF (i-1>=1.and.j-1>=1.and.k-1>=1.) THEN
					gen(i,j,k)=(-dq(i,j)*dz(k)*(.5*(dy(i,j)+dy(i+1,j))/ddx(i,j)+ &
					.5*(dy(i,j)+dy(i-1,j))/ddx(i-1,j)+ &
					.5*(dx(i,j)+dx(i,j+1))/ddy(i,j)+ &
					.5*(dx(i,j)+dx(i,j-1))/ddy(i,j-1)))/art(i,j)- &
     					(1.e0/dzz(k-1)+1.e0/dzz(k))/dq(i,j)*(art(i,j) &
					+ .5*(aaf(i,j,k+1)+aaf(i,j,k))*aaf(i,j,k)*dy(i,j)/dx(i,j)+ &
					.5*(bbf(i,j,k+1)+bbf(i,j,k))*bbf(i,j,k)*dx(i,j)/dy(i,j) &
					)/art(i,j)
				ELSE
					gen(i,j,k)=(-dq(i,j)*dz(k)*(dy(i,j)/ddx(i,j)+ &
					dy(i,j)/ddx(i-1,j)+ &
					dx(i,j)/ddy(i,j)+ &
					dx(i,j)/ddy(i,j-1)))/art(i,j)- &
     					(2.e0/dzz(k))/dq(i,j)* &
					(art(i,j)+.5*(aaf(i,j,k+1)+aaf(i,j,k))*aaf(i,j,k)*dy(i,j)/dx(i,j)+ &
					.5*(bbf(i,j,k+1)+bbf(i,j,k))*bbf(i,j,k)*dx(i,j)/dy(i,j) &
					)/art(i,j)	
		
				END IF	

				IF (((k-1)*(k-kb)*(i-1)*(i-im)*(j-1)*(j-jm)).ne.0) THEN
					m=m+1

					IF (k==kbm1) THEN
						ga1(i+1,j,k)=ga1(i+1,j,k)+aa1(i+1,j,k+1)
						ga2(i-1,j,k)=ga2(i-1,j,k)+aa2(i-1,j,k+1)
						gb1(i,j+1,k)=gb1(i,j+1,k)+bb1(i,j+1,k+1)
						gb2(i,j-1,k)=gb2(i,j-1,k)+bb2(i,j-1,k+1)
						gen(i,j,k)=gen(i,j,k)+gc1(i,j,k+1)
					END IF
					IF (k==2) THEN !�������� �����������
						ga1(i+1,j,k)=ga1(i+1,j,k)
						aa2(i+1,j,k-1)=0.
						ga2(i-1,j,k)=ga2(i-1,j,k)
						bb2(i,j+1,k-1)=0.
						gb1(i,j+1,k)=gb1(i,j+1,k)
						bb4(i,j-1,k-1)=0.
						gb2(i,j-1,k)=gb2(i,j-1,k)
						bb4(i,j-1,k-1)=0.
						gen(i,j,k)=gen(i,j,k)
						gc2(i,j,k-1)=0.
					END IF

					IF (i==2) THEN
						gc2(i,j,k-1)=gc2(i,j,k-1)+aa4(i-1,j,k-1)
						gc1(i,j,k+1)=gc1(i,j,k+1)+aa2(i-1,j,k+1)
						gen(i,j,k)=gen(i,j,k)+ga2(i-1,j,k)
						gb1(i,j+1,k)=gb1(i,j+1,k)
						gb2(i,j-1,k)=gb2(i,j-1,k)
						bb1(i,j+1,k+1)=bb1(i,j+1,k+1)
						bb2(i,j-1,k+1)=bb2(i,j-1,k+1)
						bb3(i,j+1,k-1)=bb3(i,j+1,k-1)
						bb4(i,j-1,k-1)=bb4(i,j-1,k-1)
					END IF
					
					IF (i==imm1) THEN
						gc2(i,j,k-1)=gc2(i,j,k-1)+aa3(i+1,j,k-1)
						gc1(i,j,k+1)=gc1(i,j,k+1)+aa1(i+1,j,k+1)
						gen(i,j,k)=gen(i,j,k)+ga1(i+1,j,k)
						gb1(i,j+1,k)=gb1(i,j+1,k)
						gb2(i,j-1,k)=gb2(i,j-1,k)
						bb1(i,j+1,k+1)=bb1(i,j+1,k+1)
						bb2(i,j-1,k+1)=bb2(i,j-1,k+1)
						bb3(i,j+1,k-1)=bb3(i,j+1,k-1)
						bb4(i,j-1,k-1)=bb4(i,j-1,k-1)
					END IF
					
					IF (j==2) THEN
						gc2(i,j,k-1)=gc2(i,j,k-1)+bb4(i,j-1,k-1)
						gc1(i,j,k+1)=gc1(i,j,k+1)+bb2(i,j-1,k+1)
						gen(i,j,k)=gen(i,j,k)+gb2(i,j-1,k) 
						ga1(i+1,j,k)=ga1(i+1,j,k)
						ga2(i-1,j,k)=ga2(i-1,j,k)
						aa1(i+1,j,k+1)=aa1(i+1,j,k+1)
						aa2(i-1,j,k+1)=aa2(i-1,j,k+1)
						aa3(i+1,j,k-1)=aa3(i+1,j,k-1)
						aa4(i-1,j,k-1)=aa4(i-1,j,k-1)
					END IF
					
					IF (j==jmm1) THEN
						gc2(i,j,k-1)=gc2(i,j,k-1)+bb3(i,j+1,k-1)
						gc1(i,j,k+1)=gc1(i,j,k+1)+bb1(i,j+1,k+1)
						gen(i,j,k)=gen(i,j,k)+gb1(i,j+1,k) 
						ga1(i+1,j,k)=ga1(i+1,j,k)
						ga2(i-1,j,k)=ga2(i-1,j,k)
						aa1(i+1,j,k+1)=aa1(i+1,j,k+1)
						aa2(i-1,j,k+1)=aa2(i-1,j,k+1)
						aa3(i+1,j,k-1)=aa3(i+1,j,k-1)
						aa4(i-1,j,k-1)=aa4(i-1,j,k-1)
					END IF
				END IF

				IF (maa1+m<=lm) THEN
					IF (k+1>kbm1.or.i+1>imm1) THEN
						apr(m)=0.0	
					ELSE
					apr(m)=aa1(i+1,j,k+1)
					END IF
					ja(m)=ind(m+maa1)
					ia(m)=ind(m)
				END IF
				lapr=ma1
				IF (mbb1+m<=lm) THEN
					IF (k+1>kbm1.or.j+1>jmm1) THEN
						apr(lapr+m)=0.0
					ELSE
						apr(lapr+m)=bb1(i,j+1,k+1)
					END IF
					ja(m+lapr)=ind(m+mbb1)
					ia(m+lapr)=ind(m)
				END IF
				lapr=ma1+mb1
				IF (mgc+m<=lm) THEN  
					IF (k+1>kbm1) THEN
						apr(lapr+m)=0.0
					ELSE
						apr(lapr+m)=gc1(i,j,k+1)
					END IF
					ja(m+lapr)=ind(m+mgc)
					ia(m+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+mc
				IF (mbb2+m<=lm) THEN
					IF (k+1>kbm1.or.j-1<2) THEN
						apr(lapr+m)=0.0           	
					ELSE
						apr(lapr+m)=bb2(i,j-1,k+1)
					END IF	
					ja(m+lapr)=ind(m+mbb2)
					ia(m+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+mc+mb2
				IF (maa2+m<=lm) THEN  
					IF (k+1>kbm1.or.i-1<2) THEN
						apr(lapr+m)=0.0
					ELSE
						apr(lapr+m)=aa2(i-1,j,k+1)
					END IF
					ja(m+lapr)=ind(m+maa2)
					ia(m+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+mc+mb2+ma2
				IF (mga+m<=lm) THEN
					IF (i+1>imm1) THEN 
						apr(lapr+m)=0.0
					ELSE   
						apr(lapr+m)=ga1(i+1,j,k)
					END IF
					ja(m+lapr)=ind(m+mga)
					ia(m+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+mc+mb2+ma2+ma
				IF (1+m<=lm) THEN 
					IF (j+1>jmm1) THEN 
						apr(lapr+m)=0.0
					ELSE  
						apr(lapr+m)=gb1(i,j+1,k)
					END IF
					ja(m+lapr)=ind(m+mgb)
					ia(m+lapr)=ind(m)
				END IF 
				lapr=ma1+mb1+mc+mb2+ma2+ma+mb
			 
				apr(lapr+m)=gen(i,j,k)
				ja(m+lapr)=ind(m)
				ia(m+lapr)=ind(m)
				lapr=ma1+mb1+mc+mb2+ma2+ma+mb+lm
				IF (m-mgb>=1) THEN
					IF (j-1<2) THEN 
						apr(m-mgb+lapr)=0.0
					ELSE  
						apr(m-mgb+lapr)=gb2(i,j-1,k)
					END IF
					ja(m-mgb+lapr)=ind(m-mgb)
				   	ia(m-mgb+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+mc+mb2+ma2+ma+2*mb+lm
				IF (m-mga>=1) THEN
					IF (i-1<2) THEN 
						apr(m-mga+lapr)=0.0
					ELSE
						apr(m-mga+lapr)=ga2(i-1,j,k)
					END IF
					ia(m-mga+lapr)=ind(m)
				   	ja(m-mga+lapr)=ind(-mga+m)
				END IF
				lapr=ma1+mb1+mc+mb2+ma2+2*ma+2*mb+lm
				IF (m-maa2>=1) THEN  
					IF (k-1<2.or.i+1>imm1) THEN
						apr(m-maa2+lapr)=0.0
					ELSE
						apr(m-maa2+lapr)=aa3(i+1,j,k-1)
					END IF
					ia(m-maa2+lapr)=ind(m)
				   	ja(m-maa2+lapr)=ind(-maa2+m)
				END IF
				lapr=ma1+mb1+mc+mb2+2*ma2+2*ma+2*mb+lm
				IF (m-mbb2>=1) THEN 
					IF (j+1>jmm1.or.k-1<2) THEN
						apr(m-mbb2+lapr)=0.0
					ELSE
						apr(m-mbb2+lapr)=bb3(i,j+1,k-1)
					END IF
					ia(m-mbb2+lapr)=ind(m)
				   	ja(m-mbb2+lapr)=ind(-mbb2+m)
				END IF
				lapr=ma1+mb1+mc+2*mb2+2*ma2+2*ma+2*mb+lm
				IF (m-mgc>=1) THEN
					IF (k-1>kbm1) THEN
						apr(m-mgc+lapr)=0.0
					ELSE 
						apr(m-mgc+lapr)=gc2(i,j,k-1)
					END IF
					ja(m-mgc+lapr)=ind(m-mgc)
				   	ia(m-mgc+lapr)=ind(m)
				END IF
				lapr=ma1+mb1+2*mc+2*mb2+2*ma2+2*ma+2*mb+lm
				IF (m-mbb1>=1) THEN 
					IF (j-1<2.or.k-1<2) THEN
						apr(m-mbb1+lapr)=0.0
					ELSE
						apr(m-mbb1+lapr)=bb4(i,j-1,k-1)
					END IF
					ia(m-mbb1+lapr)=ind(m)
				   	ja(m-mbb1+lapr)=ind(-mbb1+m)
				END IF
				lapr=ma1+2*mb1+2*mc+2*mb2+2*ma2+2*ma+2*mb+lm
				IF (m-maa1>=1) THEN 
					IF (i-1<2.or.k-1<2) THEN
						apr(m-maa1+lapr)=0.0
					ELSE
						apr(m-maa1+lapr)=aa4(i-1,j,k-1)
					END IF
					ia(m-maa1+lapr)=ind(m)
				   ja(m-maa1+lapr)=ind(-maa1+m)
				END IF
			END DO
		END DO
	END DO

	!form right column in Puasson eq.  
	m=0
	DO k=1,kb
		DO i=2,im
			DO j=1,jm
				r1(i,j,k)=(uf(i,j,k) &
				)*.5e0*(dq(i-1,j)+dq(i,j))*dz(k)*.5*(dy(i,j)+dy(i-1,j))
			END DO
		END DO
	END DO

	DO k=1,kb
		DO i=1,im
			DO j=2,jm
				r2(i,j,k)=(vf(i,j,k) &
				)*.5e0*(dq(i,j-1)+dq(i,j))*dz(k)*.5*(dx(i,j)+dx(i,j-1))
			END DO
		END DO
	END DO

	call coef2(aaf,bbf,ccf,fff)
	
	DO k=2,kb
		DO i=2,imm1
			DO j=2,jmm1
				wq(i,j,k)=-fFF(I,J,K)+wwf(i,j,k)+CCF(I,J,K)
			END DO
		END DO
	END DO

	DO k=2,kbm1
		DO i=2,imm1
			DO j=2,jmm1
				m=m+1

				r(i,j,k)=1./(2.*dti)*((r1(i+1,j,k)-r1(i,j,k))+ &
				(r2(i,j+1,k)-r2(i,j,k))+ &
				(wq(i,j,k)-wq(i,j,k+1))*art(i,j))/art(i,j)

				rr(m)=r(i,j,k)
			END DO
		END DO
	END DO
  
      	iunit=0

	call DSluBC(N, rr, qq, NELT, IA, JA, Apr, ISYM, ITOL, TOL, &
        ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW)
	
	m=0
	DO k=2,kbm1
		DO i=2,imm1
			DO j=2,jmm1
				m=m+1
				q(i,j,k)=qq(m)
			END DO
		END DO
	END DO

	DO k=1,kb
		DO i=1,im
			DO  j=1,jm
!				q(2,j,k)=q(3,j,k)
				q(1,j,k)=q(2,j,k)
!				q(i,2,k)=q(i,3,k)
				q(i,1,k)=q(i,2,k)
!				q(imm1,j,k)=q(imm2,j,k)

				q(im,j,k)=q(imm1,j,k)
!				q(i,jmm1,k)=q(i,jmm2,k)
				q(i,jm,k)=q(i,jmm1,k)
!		
			END DO
		END DO
	END DO
	DO i=2,imm1
		DO j=2,jmm1
	
!			q(i,j,kbm1)=q(i,j,kbm2)
			q(i,j,kb)=q(i,j,kbm1)

			q(i,j,1)=0.0
		END DO 
	END DO

	RETURN
	END SUBROUTINE pressure1
	
	SUBROUTINE faz(q)
	USE BLK1D
	USE BLK2D
	USE BLK33D
	USE BLK3D
	USE BLKCON
	USE PROM
	IMPLICIT NONE
!	INCLUDE 'comblk98.h'  
	REAL(KIND=DP), DIMENSION(im,jm,kb) :: q, ungf, qflvy, qfluz, qflvz, &
	     qflwz, vngf, wng
	REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: qux, qvy, quz, &
	     qvz, qwz, qflux, qfluy
        INTEGER :: i, j, k
!	equivalence(qux,xpsi),(qvy,ypsi),(qwz,zpsi), &
!	(quz,a),(qvz,c),(qflux,ee),(qfluy,gg)
        qux => xpsi
	qvy => ypsi
	qwz => zpsi
	quz => a
	qvz => c
	qflux => ee
	qfluy => gg
	call coef2(aaf,bbf,ccf,fff)
	DO k=1,kb
		DO i=1,im
			DO j=1,jm
				qvy(i,j,k)=q(i,j,k)*dtf(i,j)
				qux(i,j,k)=q(i,j,k)*dtf(i,j)
			END DO
		END DO
	END DO
	DO k=2,kb
		DO i=2,im
			DO j=2,jm
				quz(i,j,k)=.25e0*(q(i-1,j,k)+q(i,j,k)+q(i-1,j,k-1)+q(i,j,k-1))* &
				.5e0*(aaf(i-1,j,k)+aaf(i,j,k))

				qvz(i,j,k)=.25e0*(q(i,j-1,k)+q(i,j,k)+q(i,j,k-1)+q(i,j-1,k-1))* &
				.5e0*(bbf(i,j-1,k)+bbf(i,j,k))
			END DO
		END DO
	END DO
	DO k=2,kb
		DO i=1,im
			DO j=1,jm
				qwz(i,j,k)=(q(i,j,k-1)-q(i,j,k))
!				quz=0.
!				qvz=0.	
			END DO
		END DO
	END DO

	DO k=2,kbm1
		DO i=2,imm1
			DO j=2,jmm1
				qflux(i,j,k)=(qux(i,j,k)-qux(i-1,j,k))
	
				qfluz(i,j,k)=(quz(i,j,k)-quz(i,j,k+1))/dz(k)
	
				qflvy(i,j,k)=(qvy(i,j,k)-qvy(i,j-1,k))
	
				qflvz(i,j,k)=(qvz(i,j,k)-qvz(i,j,k+1))/dz(k)

 				qflwz(i,j,k)=qwz(i,j,k)/dzz(k)
				IF (i==2) THEN
					qflux(i,j,k)=0.
					qfluz(i,j,k)=0.
				END IF
				IF (j==2) THEN
					qflvz(i,j,k)=0.
					qflvy(i,j,k)=0.
				END IF

!				if(k==2)then
!					qflwz(i,j,k)=0.
!				endif
			END DO
		END DO
	END DO
	wf=wf
	uf=uf
	vf=vf
	DO k=2,kbm1
		DO i=2,imm1
			DO j=2,jmm1
				ungf(i,j,k)=-2.*dti*(qflux(i,j,k)- &
				qfluz(i,j,k))/(.5e0*(dtf(i,j) &
				+dtf(i-1,j)))/(.5*(dx(i,j)+dx(i-1,j)))
				vngf(i,j,k)=-2.*dti*(qflvy(i,j,k)- &
				qflvz(i,j,k))/(.5e0*(dtf(i,j) &
				+dtf(i,j-1)))/(.5*(dy(i,j)+dy(i,j-1)))
				wng(i,j,k)=-2.*dti*qflwz(i,j,k)/dtf(i,j)
!				wwng(i,j,k)=-2.*dti*qflwz(i,j,k)/dtf(i,j)
				wq(i,j,k)=(wq(i,j,k)+wng(i,j,k))*fsm(i,j)
				wf(i,j,k)=(wq(i,j,k)-ccf(i,j,k))*fsm(i,j)
				uf(i,j,k)=(uf(i,j,k)+ungf(i,j,k))*dum(i,j)
				vf(i,j,k)=(vf(i,j,k)+vngf(i,j,k))*dvm(i,j)	
				wwf(i,j,k)=(wQ(i,j,k)-aaf(i,j,k)/dx(i,j)*ungf(i,j,k) &
				-bbf(i,j,k)/dy(i,j)*vngf(i,j,k))*fsm(i,j)
			END DO
		END DO
	END DO
	RETURN
	END SUBROUTINE faz
