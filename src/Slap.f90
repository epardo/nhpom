MODULE Slap
    
    INTERFACE
	FUNCTION ISDBCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, &
          TOL, ITER, ERR, IERR, IUNIT, R, Z, DZ, &
          RWORK, IWORK, AK, BK, BNRM, SOLNRM)
	    USE SOLBLK
	    IMPLICIT NONE
	    INTEGER :: ISDBCG
	    INTEGER :: I
	    INTEGER, INTENT(IN) ::  N, NELT, ISYM, ITOL, IUNIT
	    INTEGER, INTENT(OUT) :: ITER, IERR
	    INTEGER, DIMENSION(1) :: IWORK
	    INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
	    REAL(KIND=DP), INTENT(IN) :: TOL
	    REAL(KIND=DP), INTENT(OUT) :: ERR
	    REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B, R
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
	    REAL(KIND=DP),  DIMENSION(N) :: Z, DZ
	    REAL(KIND=DP), DIMENSION(*) :: RWORK
	    REAL(KIND=DP), INTENT(IN) :: AK, BK
	    REAL(KIND=DP), INTENT(INOUT) :: BNRM, SOLNRM
	    EXTERNAL MSOLVE
	END FUNCTION ISDBCG
	
	FUNCTION RAND(R)
	    USE double
	    IMPLICIT NONE
	    REAL(KIND=DP) :: R, RAND
	    INTEGER :: IY0, IY1
	    INTEGER, SAVE :: IA1 = 1536, IA0 = 1029, IA1MA0 = 507, &
		IC = 1731, IX1 = 0, IX0 = 0
	END FUNCTION RAND
	
	SUBROUTINE DBCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MTTVEC, &
          MSOLVE, MTSOLV, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
          R, Z, P, RR, ZZ, PP, DZ, RWORK, IWORK)
	    USE double
	    IMPLICIT NONE
	    INTEGER :: ISDBCG
	    INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT
	    INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
	    INTEGER, DIMENSION(*) :: IWORK
	    INTEGER, INTENT(OUT) :: ITER, IERR
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
	    REAL(KIND=DP), DIMENSION(N) :: R, Z, P, RR, ZZ, PP, DZ 
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
	    REAL(KIND=DP), DIMENSION(*) :: RWORK
	    REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A
	    REAL(KIND=DP) :: TOL 
	    REAL(KIND=DP), INTENT(OUT) ::  ERR
	    EXTERNAL MATVEC, MTTVEC, MSOLVE, MTSOLV
	    INTEGER I, K
	END SUBROUTINE DBCG
	
	SUBROUTINE DSDBCG(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL, &
          ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
	    USE double
	    IMPLICIT NONE
	    INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT, LENW, LENIW
	    INTEGER, INTENT(OUT) :: ITER, IERR
	    INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
	    INTEGER, DIMENSION(LENIW) :: IWORK 
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: A
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
	    REAL(KIND=DP), DIMENSION(LENW) :: RWORK
	    REAL(KIND=DP), INTENT(IN) :: TOL
	    REAL(KIND=DP), INTENT(OUT) :: ERR
	    EXTERNAL DSMV, DSMTV, DSDI
	    INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
	    INTEGER :: LOCDIN, LOCDZ, LOCIW, LOCP, LOCPP, LOCR, LOCRR, &
	       LOCW, LOCZ, LOCZZ
	END SUBROUTINE DSDBCG
	
	SUBROUTINE DSLUBC(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL, &
          ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
	    USE double
	    IMPLICIT NONE
	    INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT, LENW, LENIW
	    INTEGER, INTENT(OUT) :: ITER, IERR
	    INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
	    INTEGER, DIMENSION(*) :: IWORK
	    REAL(KIND=DP), INTENT(IN) :: TOL 
	    REAL(KIND=DP), INTENT(OUT) :: ERR
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
	    REAL(KIND=DP), DIMENSION(*) :: RWORK
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(NELT) :: A
	    EXTERNAL DSMV, DSMTV, DSLUI, DSLUTI
	    INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
	    INTEGER :: ICOL, J, JBGN, JEND, LOCDIN, LOCDZ, LOCIL, LOCIU, &
		LOCIW, LOCJL, LOCJU, LOCL, LOCNC, LOCNR, LOCP, LOCPP, LOCR, &
		LOCRR, LOCU, LOCW, LOCZ, LOCZZ, NL, NU
	END SUBROUTINE DSLUBC
	
	SUBROUTINE DSILUS(N, NELT, IA, JA, A, ISYM, NL, IL, JL, &
          L, DINV, NU, IU, JU, U, NROW, NCOL)
	    USE double
	    IMPLICIT NONE
	    INTEGER, INTENT(IN) :: N, NELT, ISYM 
	    INTEGER :: NL, NU
	    INTEGER, DIMENSION(NU) :: IU, JU
	    INTEGER, DIMENSION(N) :: NROW, NCOL
	    INTEGER, DIMENSION(NL) :: IL, JL
	    INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
	    REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A 
	    REAL(KIND=DP), DIMENSION(NL) :: L
	    REAL(KIND=DP), DIMENSION(N) :: DINV
	    REAL(KIND=DP), DIMENSION(NU) :: U
	    INTEGER :: I, IBGN, ICOL, IEND, INDX, INDX1, INDX2, INDXC1, &
		INDXC2, INDXR1, INDXR2, IROW, ITEMP, J, JBGN, JEND, JTEMP, &
		K, KC, KR
	    REAL(KIND=DP) :: TEMP
	END SUBROUTINE DSILUS
	
	SUBROUTINE DSLUI(N, B, X, RWORK, IWORK)
	    USE double
	    IMPLICIT NONE
	    INTEGER :: N
	    INTEGER, DIMENSION(*) :: IWORK
	    REAL(KIND=DP), DIMENSION(N) :: B, X 
	    REAL(KIND=DP), DIMENSION(*) :: RWORK
	    INTEGER :: LOCIL, LOCJL, LOCIU, LOCJU, LOCL, LOCDIN, LOCU
	END SUBROUTINE DSLUI
	
	SUBROUTINE DSLUI2(N, B, X, IL, JL, L, DINV, IU, JU, U )
	    USE double
	    IMPLICIT NONE
	    INTEGER :: N, I, ICOL, IROW, J, JBGN, JEND
	    INTEGER, DIMENSION(1) :: IL, JL, IU, JU
	    REAL(KIND=DP), DIMENSION(N) :: B, X, DINV 
	    REAL(KIND=DP), DIMENSION(1) :: L, U
	END SUBROUTINE DSLUI2
	
	SUBROUTINE DSLUTI(N, B, X, RWORK, IWORK)
	    USE double
	    IMPLICIT NONE
	    INTEGER :: N, LOCDIN, LOCIL, LOCIU, LOCJL, LOCJU, LOCL, LOCU
	    INTEGER, DIMENSION(*) :: IWORK
	    REAL(KIND=DP), DIMENSION(N) :: B, X
	    REAL(KIND=DP), DIMENSION(*) :: RWORK
	END SUBROUTINE DSLUTI
	
	SUBROUTINE DSLUI4(N, B, X, IL, JL, L, DINV, IU, JU, U )
	    USE double
	    IMPLICIT NONE
	    INTEGER :: N, I, ICOL, IROW, J, JBGN, JEND
	    INTEGER, DIMENSION(*) :: IL, JL, IU, JU
	    REAL(KIND=DP), DIMENSION(N) :: B, X, DINV
	    REAL(KIND=DP), DIMENSION(*) :: L, U
	END SUBROUTINE DSLUI4
	
	subroutine xerprt(messg)
	    IMPLICIT NONE
	    integer, external :: i1mach
	    integer, dimension(5) :: lun
	    character*(*) messg
	    integer :: ichar, iunit, kunit, last, lenmes, nunit
	end subroutine xerprt
	
	subroutine xerror(messg,nmessg,nerr,level)
	    IMPLICIT NONE
	    character*(*) messg
	    integer, intent(in) :: nmessg, nerr, level
	end subroutine xerror
	
	subroutine xerrwv(messg,nmessg,nerr,level,ni,i1,i2,nr,r1,r2)
	    USE double
	    IMPLICIT NONE
	    integer :: j4save, i1mach
	    character*(*) messg
	    character*20 lfirst
	    character*37 form
	    integer, dimension(5) :: lun
	    integer :: nmessg, nerr, level, ni, i1, i2, nr, isizef, isizei, &
		ifatal, iunit, junk, kdummy, kount, kunit, lerr, lkntrl, &
		maxmes, mkntrl, nunit, i, llevel, lmessg
	    REAL(KIND=DP) :: r1,r2
	end subroutine xerrwv
	
	subroutine xersav(messg,nmessg,nerr,level,icount)
	    IMPLICIT NONE
	    integer :: nmessg, nerr, level, icount, i, ii, iunit, kountx, &
		kunit, nunit
	    integer, dimension(5) :: lun
	    character*(*) messg
	    character*20 mestab(10),mes
	    integer, dimension(10), save :: nertab, levtab, kount
	    integer, external :: i1mach
	    save mestab,kountx
	end subroutine xersav
	
	subroutine xgetf(kontrl)
	    implicit none
	    integer :: kontrl
	    integer, external :: j4save
	end subroutine xgetf
	
	subroutine xgetua(iunita,n)
	    IMPLICIT NONE
	    integer, external :: j4save
	    integer, dimension(5) :: iunita
	    integer :: i, index, n
	end subroutine xgetua
	
	function j4save(iwhich,ivalue,iset)
	    IMPLICIT NONE
	    integer :: j4save, iwhich, ivalue
	    logical :: iset
	    integer, dimension(9), save :: iparam
	end function j4save
	
	subroutine xerclr
	    implicit none
	    integer :: j4save
	    integer :: junk
	end subroutine xerclr
	
	subroutine xerdmp
	    implicit none
	    integer :: kount
	end subroutine xerdmp
	
	subroutine xermax(max)
	    implicit none
	    integer :: max, junk
	    integer :: j4save
	end subroutine xermax
	
	subroutine xgetun(iunit)
	    implicit none
	    integer :: iunit
	    integer :: j4save
	end subroutine xgetun
	
	subroutine xsetf(kontrl)
	    implicit none
	    integer :: junk, kontrl
	    integer :: j4save
	end subroutine xsetf
	
	subroutine xsetua(iunita,n)
	    implicit none
	    integer :: i, index, junk, n
	    integer, external :: j4save
	    integer, dimension(5) :: iunita
	end subroutine xsetua
	
	subroutine xsetun(iunit)
	    implicit none
	    integer :: iunit, junk
	    integer :: j4save
	end subroutine xsetun
	
	FUNCTION D1MACH(I)
	    USE double
	    IMPLICIT NONE
	    INTEGER :: I
	    INTEGER :: I1MACH
	    REAL(KIND=DP) :: D1MACH
	    INTEGER SMALL(4)
	    INTEGER LARGE(4)
	    INTEGER RIGHT(4)
	    INTEGER DIVER(4)
	    INTEGER LOG10(4)
	    REAL(KIND=DP) DMACH(5)
	    EQUIVALENCE (DMACH(1),SMALL(1))
	    EQUIVALENCE (DMACH(2),LARGE(1))
	    EQUIVALENCE (DMACH(3),RIGHT(1))
	    EQUIVALENCE (DMACH(4),DIVER(1))
	    EQUIVALENCE (DMACH(5),LOG10(1))
	END FUNCTION D1MACH
	
	FUNCTION I1MACH(I)
	    IMPLICIT NONE
	    INTEGER :: OUTPUT, I, I1MACH
	    INTEGER, DIMENSION(16) :: IMACH
	    EQUIVALENCE (IMACH(4),OUTPUT)
	END FUNCTION I1MACH
	
	FUNCTION DDOT(N,DX,INCX,DY,INCY)
	    USE double
	    IMPLICIT NONE
	    REAL(KIND=DP) :: DDOT
	    REAL(KIND=DP), DIMENSION(1) :: DX, DY
	    INTEGER :: INCX, INCY, I, IX, IY, M, MP1, N, NS
	END FUNCTION DDOT
	
	FUNCTION DNRM2 ( N, DX, INCX)
	    USE double
	    IMPLICIT NONE
	    REAL(KIND=DP) :: DNRM2
	    INTEGER :: I, J, N, NN, INCX, NEXT
	    REAL(KIND=DP) :: HITEST, SUM, XMAX
	    REAL(KIND=DP), DIMENSION(1) :: DX
	    REAL(KIND=DP), PARAMETER :: ZERO = 0.0D0 , ONE = 1.0D0
	    REAL(KIND=DP), PARAMETER :: CUTLO = 8.232D-11, CUTHI = 1.304D19
	END FUNCTION DNRM2
	
	SUBROUTINE DSMV( N, X, Y, NELT, IA, JA, A, ISYM )
	    USE double
	    IMPLICIT NONE
	    INTEGER, INTENT(IN) :: N, NELT, ISYM
	    INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
	    REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A 
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: X
	    REAL(KIND=DP), INTENT(OUT), DIMENSION(N) :: Y
	    INTEGER :: I, IBGN, ICOL, IEND, IROW, J, JBGN, JEND
	END SUBROUTINE DSMV
	
	SUBROUTINE DSDI(N, B, X, RWORK, IWORK)
	    USE double
	    IMPLICIT NONE
	    INTEGER, INTENT(IN) :: N
	    INTEGER, INTENT(IN), DIMENSION(10) :: IWORK
	    REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
	    REAL(KIND=DP), INTENT(OUT), DIMENSION(N) :: X 
	    REAL(KIND=DP), DIMENSION(1) :: RWORK
	    INTEGER :: I, LOCD
	END SUBROUTINE DSDI
	
	SUBROUTINE DSDS(N, NELT, JA, A, DINV)
	    USE double
	    IMPLICIT NONE
	    INTEGER :: ICOL, N, NELT
	    INTEGER, DIMENSION(NELT) :: JA
	    REAL(KIND=DP), DIMENSION(NELT) :: A 
	    REAL(KIND=DP), DIMENSION(N) :: DINV
	END SUBROUTINE DSDS
	
	SUBROUTINE DAXPY(N,DA,DX,INCX,DY,INCY)    
	    USE double
	    IMPLICIT NONE    
	    REAL(KIND=DP), DIMENSION(1) :: DX, DY
	    REAL(KIND=DP) :: DA
	    INTEGER :: I, N, INCX, INCY, IX, IY, M, MP1, NS
	END SUBROUTINE DAXPY
	
	SUBROUTINE DS2Y(N, NELT, IA, JA, A)
	    USE double
	    IMPLICIT NONE
	    INTEGER I, IBGN, ICOL, IEND, ITEMP, J 
	    INTEGER, INTENT(IN) :: N, NELT
	    INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
	    REAL(KIND=DP), INTENT(INOUT), DIMENSION(NELT) :: A
	END SUBROUTINE DS2Y
	
	SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG )
	    USE double
	    IMPLICIT NONE
	    INTEGER, DIMENSION(21) :: IL, IU
	    INTEGER :: I, IJ, IT, IIT, J, JT, JJT, K, KK, L, M, NN 
	    INTEGER, DIMENSION(N) :: IA, JA
	    REAL(KIND=DP), DIMENSION(N) :: A 
	    REAL(KIND=DP) :: TA, TTA
	    INTEGER, INTENT(IN) :: N, KFLAG
	END SUBROUTINE QS2I1D
	
	SUBROUTINE DCHKW( NAME, LOCIW, LENIW, LOCW, LENW, &
          IERR, ITER, ERR )
	    USE double
	    IMPLICIT NONE
	    CHARACTER*(*) NAME
	    CHARACTER(LEN=72) MESG
	    INTEGER :: LOCIW, LENIW, LOCW, LENW, IERR, ITER
	    REAL(KIND=DP) :: ERR, D1MACH
	END SUBROUTINE DCHKW
	SUBROUTINE DCOPY(N,DX,INCX,DY,INCY)
	    USE double
	    IMPLICIT NONE
	    REAL(KIND=DP), DIMENSION(1) :: DX, DY
	    INTEGER :: N, I, INCX, INCY, IX, IY, M, MP1, NS
	END SUBROUTINE DCOPY
    END INTERFACE
END MODULE Slap


!DECK DBCG
     SUBROUTINE DBCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MTTVEC, &
          MSOLVE, MTSOLV, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
          R, Z, P, RR, ZZ, PP, DZ, RWORK, IWORK)
!***BEGIN PROLOGUE  DBCG
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DBCG-D),
!             Non-Symmetric Linear system, Sparse, 
!             Iterative Precondition,  BiConjugate Gradient
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Preconditioned BiConjugate Gradient Sparse Ax=b solver.
!            Routine to solve a Non-Symmetric linear system Ax = b 
!            using the Preconditioned BiConjugate Gradient method.
!***DESCRIPTION
! *Usage:
!      INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!      INTEGER ITER, IERR, IUNIT, IWORK(USER DEFINABLE)
!      DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, R(N), Z(N), P(N)
!      DOUBLE PRECISION RR(N), ZZ(N), PP(N), DZ(N)
!      DOUBLE PRECISION RWORK(USER DEFINABLE)
!      EXTERNAL MATVEC, MTTVEC, MSOLVE, MTSOLV
!
!      CALL DBCG(N, B, X, NELT, IA, JA, A, ISYM, MATVEC, MTTVEC,
!     $     MSOLVE, MTSOLV, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, 
!     $     R, Z, P, RR, ZZ, PP, DZ, RWORK, IWORK)
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays contain the matrix data structure for A.
!         It could take any form.  See "Description", below for more 
!         late breaking details...
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MATVEC :EXT      External.
!         Name of a routine which  performs the matrix vector multiply
!         operation  Y = A*X  given A and X.  The  name of  the MATVEC
!         routine must  be declared external  in the  calling program.
!         The calling sequence of MATVEC is:
!             CALL MATVEC( N, X, Y, NELT, IA, JA, A, ISYM )
!         Where N is the number of unknowns, Y is the product A*X upon
!         return,  X is an input  vector.  NELT, IA,  JA,  A and  ISYM
!         define the SLAP matrix data structure: see Description,below.
! MTTVEC :EXT      External.
!         Name of a routine which performs the matrix transpose vector
!         multiply y = A'*X given A and X (where ' denotes transpose).  
!         The name of the MTTVEC routine must be declared external  in
!         the calling program.  The calling sequence to MTTVEC is  the
!         same as that for MTTVEC, viz.:
!             CALL MTTVEC( N, X, Y, NELT, IA, JA, A, ISYM )
!         Where N  is the number  of unknowns, Y is the   product A'*X
!         upon return, X is an input vector.  NELT, IA, JA, A and ISYM
!         define the SLAP matrix data structure: see Description,below.
! MSOLVE :EXT      External.
!         Name of a routine which solves a linear system MZ = R  for Z
!         given R with the preconditioning matrix M (M is supplied via
!         RWORK  and IWORK arrays).   The name  of  the MSOLVE routine
!         must be declared  external  in the  calling   program.   The
!         calling sequence of MSLOVE is:
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, R is  the right-hand side
!         vector, and Z is the solution upon return.  NELT,  IA, JA, A
!         and  ISYM define the SLAP  matrix  data structure: see  
!         Description, below.  RWORK is a  double precision array that 
!         can be used
!         to  pass   necessary  preconditioning     information and/or
!         workspace to MSOLVE.  IWORK is an integer work array for the
!         same purpose as RWORK.
! MTSOLV :EXT      External.                              T
!         Name of a routine which solves a linear system M ZZ = RR for
!         ZZ given RR with the preconditioning matrix M (M is supplied 
!         via RWORK and IWORK arrays).  The name of the MTSOLV routine 
!         must be declared external in the calling program.  The call-
!         ing sequence to MTSOLV is:
!            CALL MTSOLV(N, RR, ZZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, RR is the right-hand side
!         vector, and ZZ is the solution upon return.  NELT, IA, JA, A
!         and  ISYM define the SLAP  matrix  data structure: see  
!         Description, below.  RWORK is a  double precision array that 
!         can be used
!         to  pass   necessary  preconditioning     information and/or
!         workspace to MTSOLV.  IWORK is an integer work array for the
!         same purpose as RWORK.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual 
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the 
!         residual divided by the 2-norm of M-inv times the right hand 
!         side is less than TOL, where M-inv is the inverse of the 
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different 
!         routines.  For this case, the user must supply the "exact" 
!         solution or a very accurate approximation (one with an error 
!         much less than TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!         if ITOL=11, iteration stops when the 2-norm of the difference 
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution 
!         is less than TOL.  Note that this requires the user to set up
!         the "COMMON /SOLBLK/ SOLN(LENGTH)" in the calling routine. 
!         The routine with this declaration should be loaded before the
!         stop test so that the correct length is used by the loader.  
!         This procedure is not standard Fortran and may not work 
!         correctly on your system (although it has worked on every
!         system the authors have tried).  If ITOL is not 11 then this
!         common block is indeed standard Fortran.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or 
!         ITMAX+1 if convergence criterion could not be achieved in 
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as 
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Return error flag.
!           IERR = 0 => All went well.
!           IERR = 1 => Insufficient storage allocated 
!                       for WORK or IWORK.
!           IERR = 2 => Method failed to converge in 
!                       ITMAX steps.
!           IERR = 3 => Error in user input.  Check input
!                       value of N, ITOL.
!           IERR = 4 => User error tolerance set too tight.
!                       Reset to 500.0*D1MACH(3).  Iteration proceeded.
!           IERR = 5 => Preconditioning matrix, M,  is not 
!                       Positive Definite.  $(r,z) < 0.0$.
!           IERR = 6 => Matrix A is not Positive Definite.  
!                       $(p,Ap) < 0.0$.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration, 
!         if this is desired for monitoring convergence.  If unit 
!         number is 0, no writing will occur.
! R      :WORK     Double Precision R(N).
! Z      :WORK     Double Precision Z(N).
! P      :WORK     Double Precision P(N).
! RR     :WORK     Double Precision RR(N).
! ZZ     :WORK     Double Precision ZZ(N).
! PP     :WORK     Double Precision PP(N).
! DZ     :WORK     Double Precision DZ(N).
! RWORK  :WORK     Double Precision RWORK(USER DEFINED).
!         Double Precision array that can be used for workspace in 
!         MSOLVE and MTSOLV.  
! IWORK  :WORK     Integer IWORK(USER DEFINED).
!         Integer array that can be used for workspace in MSOLVE
!         and MTSOLV.
!
! *Description
!       This routine does  not care  what matrix data   structure is
!       used for  A and M.  It simply   calls  the MATVEC and MSOLVE
!       routines, with  the arguments as  described above.  The user
!       could write any type of structure and the appropriate MATVEC
!       and MSOLVE routines.  It is assumed  that A is stored in the
!       IA, JA, A  arrays in some fashion and  that M (or INV(M)) is
!       stored  in  IWORK  and  RWORK   in  some fashion.   The SLAP
!       routines SDBCG and DSLUBC are examples of this procedure.
!       
!       Two  examples  of  matrix  data structures  are the: 1) SLAP
!       Triad  format and 2) SLAP Column format.
!       
!       =================== S L A P Triad format ===================
!       In  this   format only the  non-zeros are  stored.  They may
!       appear  in *ANY* order.   The user  supplies three arrays of
!       length NELT, where  NELT  is the number  of non-zeros in the
!       matrix:  (IA(NELT), JA(NELT),  A(NELT)).  For each  non-zero
!       the  user puts   the row  and  column index   of that matrix
!       element in the IA and JA arrays.  The  value of the non-zero
!       matrix  element is  placed in  the corresponding location of
!       the A  array.  This is  an extremely easy data  structure to
!       generate.  On  the other hand it  is  not too  efficient  on
!       vector  computers   for the  iterative  solution  of  linear
!       systems.  Hence, SLAP  changes this input  data structure to
!       the SLAP   Column  format for the  iteration (but   does not
!       change it back).
!       
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
! *Precision:           Double Precision
! *See Also:
!       SDBCG, DSLUBC
!***REFERENCES  (NONE)
!***ROUTINES CALLED  MATVEC, MTTVEC, MSOLVE, MTSOLV, ISDBCG,
!                    DCOPY, DDOT, DAXPY, D1MACH
!***END PROLOGUE  DBCG
      USE double
      USE Slap, ONLY: D1MACH, DDOT
      IMPLICIT NONE
      INTEGER :: ISDBCG
      INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT
      INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
      INTEGER, DIMENSION(*) :: IWORK
      INTEGER, INTENT(OUT) :: ITER, IERR
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
      REAL(KIND=DP), DIMENSION(N) :: R, Z, P, RR, ZZ, PP, DZ 
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
      REAL(KIND=DP), DIMENSION(*) :: RWORK
      REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A
      REAL(KIND=DP) :: TOL, AK, AKDEN, BK, BKDEN, BKNUM, BRTM, &
          BNRM, FUZZ, SOLNRM, TOLMIN
      REAL(KIND=DP), INTENT(OUT) ::  ERR
      EXTERNAL MATVEC, MTTVEC, MSOLVE, MTSOLV
      INTEGER I, K
!
!         Check some of the input data.
!***FIRST EXECUTABLE STATEMENT  DBCG
      ITER = 0
      IERR = 0
      IF( N.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      FUZZ = D1MACH(3)
      TOLMIN = 500.0*FUZZ
      FUZZ = FUZZ*FUZZ
      IF( TOL.LT.TOLMIN ) THEN
         TOL = TOLMIN
         IERR = 4
      ENDIF
!         
!         Calculate initial residual and pseudo-residual, and check
!         stopping criterion.
      CALL MATVEC(N, X, R, NELT, IA, JA, A, ISYM)
      DO I = 1, N
         R(I)  = B(I) - R(I)
         RR(I) = R(I)
      END DO
      CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
      CALL MTSOLV(N, RR, ZZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
      IF (ISDBCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL, &
          ITER, ERR, IERR, IUNIT, R, Z, &
          DZ, RWORK, IWORK, AK, BK, BNRM, SOLNRM).NE.0) THEN
	     RETURN
      END IF
      IF( IERR.NE.0 ) RETURN
!         
!         ***** iteration loop *****
!         
      DO K=1,ITMAX
         ITER = K
!         
!         Calculate coefficient BK and direction vectors P and PP.
         BKNUM = DDOT(N, Z, 1, RR, 1)
         IF( ABS(BKNUM).LE.FUZZ ) THEN
            IERR = 6
            RETURN
         ENDIF
         IF(ITER .EQ. 1) THEN
            CALL DCOPY(N, Z, 1, P, 1)
            CALL DCOPY(N, ZZ, 1, PP, 1)
         ELSE
            BK = BKNUM/BKDEN
            DO I = 1, N
               P(I) = Z(I) + BK*P(I)
               PP(I) = ZZ(I) + BK*PP(I)
            END DO
         ENDIF
         BKDEN = BKNUM
!         
!         Calculate coefficient AK, new iterate X, new resids R and RR,
!         and new pseudo-resids Z and ZZ.
         CALL MATVEC(N, P, Z, NELT, IA, JA, A, ISYM)
         AKDEN = DDOT(N, PP, 1, Z, 1)
         AK = BKNUM/AKDEN
         IF( ABS(AKDEN).LE.FUZZ ) THEN
            IERR = 6
            RETURN
         ENDIF
         CALL DAXPY(N, AK, P, 1, X, 1)
         CALL DAXPY(N, -AK, Z, 1, R, 1)
         CALL MTTVEC(N, PP, ZZ, NELT, IA, JA, A, ISYM)
         CALL DAXPY(N, -AK, ZZ, 1, RR, 1)
         CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         CALL MTSOLV(N, RR, ZZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         
!         check stopping criterion.
         IF(ISDBCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL, &
             ITER, ERR, IERR, IUNIT, R, Z, &
             DZ, RWORK, IWORK, AK, BK, BNRM, SOLNRM).NE.0) THEN
               RETURN
	 END IF
     
!         
     END DO
!         
!         *****   end of loop  *****
!         
!         stopping criterion not satisfied.
      ITER = ITMAX + 1
      IERR = 2
!         
      RETURN
!------------- LAST LINE OF DBCG FOLLOWS ----------------------------
      END SUBROUTINE DBCG
!DECK DSDBCG
      SUBROUTINE DSDBCG(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL, &
          ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
!***BEGIN PROLOGUE  DSDBCG
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(SSDBCG-D),
!             Non-Symmetric Linear system, Sparse, 
!             Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Diagonally Scaled BiConjugate Gradient Sparse Ax=b solver.
!            Routine to solve a linear system  Ax = b  using the 
!            BiConjugate Gradient method with diagonal scaling.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!     INTEGER ITER, IERR, IUNIT, LENW, IWORK(10), LENIW
!     DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, RWORK(8*N)
!
!     CALL DSDBCG(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL,
!    $     ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See "Description", 
!         below.  If the SLAP Triad format is chosen it is changed 
!         internally to the SLAP Column format.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual 
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the 
!         residual divided by the 2-norm of M-inv times the right hand 
!         side is less than TOL, where M-inv is the inverse of the 
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different 
!         routines.  For this case, the user must supply the "exact" 
!         solution or a very accurate approximation (one with an error 
!         much less than TOL) through a common block,
!                     COMMON /SOLBLK/ SOLN(1)
!         if ITOL=11, iteration stops when the 2-norm of the difference 
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution 
!         is less than TOL.  Note that this requires the user to set up
!         the "COMMON /SOLBLK/ SOLN(LENGTH)" in the calling routine. 
!         The routine with this declaration should be loaded before the
!         stop test so that the correct length is used by the loader.  
!         This procedure is not standard Fortran and may not work 
!         correctly on your system (although it has worked on every
!         system the authors have tried).  If ITOL is not 11 then this
!         common block is indeed standard Fortran.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or 
!         ITMAX+1 if convergence criterion could not be achieved in 
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as 
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Return error flag.
!           IERR = 0 => All went well.
!           IERR = 1 => Insufficient storage allocated 
!                       for WORK or IWORK.
!           IERR = 2 => Method failed to converge in 
!                       ITMAX steps.
!           IERR = 3 => Error in user input.  Check input
!                       value of N, ITOL.
!           IERR = 4 => User error tolerance set too tight.
!                       Reset to 500.0*D1MACH(3).  Iteration proceeded.
!           IERR = 5 => Preconditioning matrix, M,  is not 
!                       Positive Definite.  $(r,z) < 0.0$.
!           IERR = 6 => Matrix A is not Positive Definite.  
!                       $(p,Ap) < 0.0$.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration, 
!         if this is desired for monitoring convergence.  If unit 
!         number is 0, no writing will occur.
! RWORK  :WORK     Double Precision RWORK(LENW).
!         Double Precision array used for workspace.
! LENW   :IN       Integer.
!         Length of the double precision workspace, RWORK.  
!         LENW >= 8*N.
! IWORK  :WORK     Integer IWORK(LENIW).
!         Used to hold pointers into the RWORK array.
! LENIW  :IN       Integer.
!         Length of the integer workspace, IWORK.  LENIW >= 10.
!         Upon return the following locations of IWORK hold information
!         which may be of use to the user:
!         IWORK(9)  Amount of Integer workspace actually used.
!         IWORK(10) Amount of Double Precision workspace actually used.
!         
! *Description:
!       This  routine performs  preconditioned  BiConjugate gradient
!       method on the Non-Symmetric positive definite  linear system
!       Ax=b. The preconditioner is M = DIAG(A), the diagonal of the
!       matrix   A.   This is the  simplest   of preconditioners and
!       vectorizes very well.
!
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures and SLAP  will figure out  which on
!       is being used and act accordingly.
!       
!       =================== S L A P Triad format ===================
!
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!       
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
! *Precision:           Double Precision
! *Side Effects:
!       The SLAP Triad format (IA, JA, A) is  modified internally to
!       be   the SLAP  Column format.   See above.
!       
! *See Also:
!       DBCG, DLUBCG
!***REFERENCES  (NONE)
!***ROUTINES CALLED  DS2Y, DCHKW, DSDS, DBCG
!***END PROLOGUE  DSDBCG
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT, LENW, LENIW
      INTEGER, INTENT(OUT) :: ITER, IERR
      INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
      INTEGER, DIMENSION(LENIW) :: IWORK 
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: A
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
      REAL(KIND=DP), DIMENSION(LENW) :: RWORK
      REAL(KIND=DP), INTENT(IN) :: TOL
      REAL(KIND=DP), INTENT(OUT) :: ERR
      EXTERNAL DSMV, DSMTV, DSDI
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
      INTEGER :: LOCDIN, LOCDZ, LOCIW, LOCP, LOCPP, LOCR, LOCRR, &
           LOCW, LOCZ, LOCZZ
!
!         Change the SLAP input matrix IA, JA, A to SLAP-Column format.
!***FIRST EXECUTABLE STATEMENT  DSDBCG
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A )
!         
!         Set up the workspace.  Compute the inverse of the 
!         diagonal of the matrix.
      LOCIW = LOCIB
!
      LOCDIN = LOCRB
      LOCR = LOCDIN + N
      LOCZ = LOCR + N
      LOCP = LOCZ + N
      LOCRR = LOCP + N
      LOCZZ = LOCRR + N
      LOCPP = LOCZZ + N
      LOCDZ = LOCPP + N
      LOCW = LOCDZ + N
!
!         Check the workspace allocations.
      CALL DCHKW( 'DSDBCG', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
      IF( IERR.NE.0 ) RETURN
!
      IWORK(4) = LOCDIN
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
      CALL DSDS(N, NELT, JA, A, RWORK(LOCDIN))
!         
!         Perform the Diagonally Scaled BiConjugate gradient algorithm.
      CALL DBCG(N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSMTV, &
          DSDI, DSDI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
          RWORK(LOCR), RWORK(LOCZ), RWORK(LOCP), &
          RWORK(LOCRR), RWORK(LOCZZ), RWORK(LOCPP), &
          RWORK(LOCDZ), RWORK(1), IWORK(1))
      RETURN
!------------- LAST LINE OF DSDBCG FOLLOWS ----------------------------
      END SUBROUTINE DSDBCG
!DECK DSLUBC
      SUBROUTINE DSLUBC(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL, &
          ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW )
!***BEGIN PROLOGUE  DSLUBC
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(SSLUBC-D),
!             Non-Symmetric Linear system, Sparse, 
!             Iterative incomplete LU Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Incomplete LU BiConjugate Gradient Sparse Ax=b solver.
!            Routine to solve a linear system  Ax = b  using the 
!            BiConjugate Gradient  method  with  Incomplete   LU 
!            decomposition preconditioning.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITMAX
!     INTEGER ITER, IERR, IUNIT, LENW, IWORK(NEL+NU+4*N+2), LENIW
!     DOUBLE PRECISION B(N), X(N), A(NELT), TOL, ERR, RWORK(NEL+NU+8*N)
!
!     CALL DSLUBC(N, B, X, NELT, IA, JA, A, ISYM, ITOL, TOL,
!    $     ITMAX, ITER, ERR, IERR, IUNIT, RWORK, LENW, IWORK, LENIW)
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See "Description", 
!         below.  If the SLAP Triad format is chosen it is changed 
!         internally to the SLAP Column format.
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual 
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the 
!         residual divided by the 2-norm of M-inv times the right hand 
!         side is less than TOL, where M-inv is the inverse of the 
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different 
!         routines.  For this case, the user must supply the "exact" 
!         solution or a very accurate approximation (one with an error 
!         much less than TOL) through a common block,
!         COMMON /SOLBLK/ SOLN( )
!         if ITOL=11, iteration stops when the 2-norm of the difference 
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution 
!         is less than TOL.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITMAX  :IN       Integer.
!         Maximum number of iterations.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or 
!         ITMAX+1 if convergence criterion could not be achieved in 
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as 
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Return error flag.
!           IERR = 0 => All went well.
!           IERR = 1 => Insufficient storage allocated 
!                       for WORK or IWORK.
!           IERR = 2 => Method failed to converge in 
!                       ITMAX steps.
!           IERR = 3 => Error in user input.  Check input
!                       value of N, ITOL.
!           IERR = 4 => User error tolerance set too tight.
!                       Reset to 500.0*D1MACH(3).  Iteration proceeded.
!           IERR = 5 => Preconditioning matrix, M,  is not 
!                       Positive Definite.  $(r,z) < 0.0$.
!           IERR = 6 => Matrix A is not Positive Definite.  
!                       $(p,Ap) < 0.0$.
!           IERR = 7 => Incomplete factorization broke down
!                       and was fudged.  Resulting preconditioning may
!                       be less than the best.
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration, 
!         if this is desired for monitoring convergence.  If unit 
!         number is 0, no writing will occur.
! RWORK  :WORK     Double Precision RWORK(LENW).
!         Double Precision array used for workspace.  NEL is the 
!         number of non-
!         zeros in the lower triangle of the matrix (including the
!         diagonal).  NU is the number of nonzeros in the upper
!         triangle of the matrix (including the diagonal).
! LENW   :IN       Integer.
!         Length of the double precision workspace, RWORK.  
!         LENW >= NEL+NU+8*N.
! IWORK  :WORK     Integer IWORK(LENIW).
!         Integer array used for workspace.  NEL is the number of non-
!         zeros in the lower triangle of the matrix (including the
!         diagonal).  NU is the number of nonzeros in the upper
!         triangle of the matrix (including the diagonal).
!         Upon return the following locations of IWORK hold information
!         which may be of use to the user:
!         IWORK(9)  Amount of Integer workspace actually used.
!         IWORK(10) Amount of Double Precision workspace actually used.
! LENIW  :IN       Integer.
!         Length of the integer workspace, IWORK.  
!         LENIW >= NEL+NU+4*N+12.
!
! *Description:
!       This routine is simply a  driver for the DBCGN  routine.  It
!       calls the DSILUS routine to set  up the  preconditioning and
!       then  calls DBCGN with  the appropriate   MATVEC, MTTVEC and
!       MSOLVE, MTSOLV routines.
!
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures and SLAP  will figure out  which on
!       is being used and act accordingly.
!       
!       =================== S L A P Triad format ===================
!
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!       
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
! *Precision:           Double Precision
! *Side Effects:
!       The SLAP Triad format (IA, JA,  A) is modified internally to
!       be the   SLAP Column  format.  See above.
!       
! *See Also:
!       DBCG, SDBCG
!***REFERENCES  (NONE)
!***ROUTINES CALLED  DS2Y, DCHKW, DSILUS, DBCG, DSMV, DSMTV
!***END PROLOGUE  DSLUBC
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N, NELT, ISYM, ITOL, ITMAX, IUNIT, LENW, LENIW
      INTEGER, INTENT(OUT) :: ITER, IERR
      INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
      INTEGER, DIMENSION(*) :: IWORK
      REAL(KIND=DP), INTENT(IN) :: TOL 
      REAL(KIND=DP), INTENT(OUT) :: ERR
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
      REAL(KIND=DP), DIMENSION(*) :: RWORK
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(NELT) :: A
      EXTERNAL DSMV, DSMTV, DSLUI, DSLUTI
      INTEGER, PARAMETER :: LOCRB=1, LOCIB=11
      INTEGER :: ICOL, J, JBGN, JEND, LOCDIN, LOCDZ, LOCIL, LOCIU, &
         LOCIW, LOCJL, LOCJU, LOCL, LOCNC, LOCNR, LOCP, LOCPP, LOCR, &
	 LOCRR, LOCU, LOCW, LOCZ, LOCZZ, NL, NU
!
!         Change the SLAP input matrix IA, JA, A to SLAP-Column format.
!***FIRST EXECUTABLE STATEMENT  DSLUBC
      IERR = 0
      IF( N.LT.1 .OR. NELT.LT.1 ) THEN
         IERR = 3
         RETURN
      ENDIF
      CALL DS2Y( N, NELT, IA, JA, A )
!
!         Count number of Non-Zero elements preconditioner ILU matrix.
!         Then set up the work arrays.
      NL = 0
      NU = 0
      DO ICOL = 1, N
!         Don't count diagonal.
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
! The CVD$ NOVECTOR directive suppresses vectorization which would clash with the DBLE function
! CVD$ NOVECTOR is a directive of Power Fortran
!CVD$ NOVECTOR
            DO J = JBGN, JEND
               IF( IA(J).GT.ICOL ) THEN
                  NL = NL + 1
                  IF( ISYM.NE.0 ) NU = NU + 1
               ELSE
                  NU = NU + 1
               ENDIF
            END DO
         ENDIF
      END DO
!         
      LOCIL = LOCIB
      LOCJL = LOCIL + N+1
      LOCIU = LOCJL + NL
      LOCJU = LOCIU + NU
      LOCNR = LOCJU + N+1
      LOCNC = LOCNR + N
      LOCIW = LOCNC + N
!
      LOCL = LOCRB
      LOCDIN = LOCL + NL
      LOCU = LOCDIN + N
      LOCR = LOCU + NU
      LOCZ = LOCR + N
      LOCP = LOCZ + N
      LOCRR = LOCP + N
      LOCZZ = LOCRR + N
      LOCPP = LOCZZ + N
      LOCDZ = LOCPP + N
      LOCW = LOCDZ + N
!
!         Check the workspace allocations.
      CALL DCHKW( 'DSLUBC', LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
      IF( IERR.NE.0 ) RETURN
!
      IWORK(1) = LOCIL
      IWORK(2) = LOCJL
      IWORK(3) = LOCIU
      IWORK(4) = LOCJU
      IWORK(5) = LOCL
      IWORK(6) = LOCDIN
      IWORK(7) = LOCU
      IWORK(9) = LOCIW
      IWORK(10) = LOCW
!
!         Compute the Incomplete LU decomposition.
      CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IWORK(LOCIL), &
           IWORK(LOCJL), RWORK(LOCL), RWORK(LOCDIN), NU, IWORK(LOCIU), &
           IWORK(LOCJU), RWORK(LOCU), IWORK(LOCNR), IWORK(LOCNC) )
!         
!         Perform the incomplete LU preconditioned 
!         BiConjugate Gradient algorithm.
      CALL DBCG(N, B, X, NELT, IA, JA, A, ISYM, DSMV, DSMTV, &
           DSLUI, DSLUTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
           RWORK(LOCR), RWORK(LOCZ), RWORK(LOCP), &
           RWORK(LOCRR), RWORK(LOCZZ), RWORK(LOCPP), &
           RWORK(LOCDZ), RWORK, IWORK )
      RETURN
!------------- LAST LINE OF DSLUBC FOLLOWS ----------------------------
      END SUBROUTINE DSLUBC
!DECK ISDBCG
      FUNCTION ISDBCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, &
          TOL, ITER, ERR, IERR, IUNIT, R, Z, DZ, &
          RWORK, IWORK, AK, BK, BNRM, SOLNRM)
!***BEGIN PROLOGUE  ISDBCG
!***REFER TO  DBCG, DSDBCG, DSLUBC
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(ISDBCG-D),
!             Non-Symmetric Linear system, Sparse,
!             Iterative Precondition, Stop Test
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Preconditioned BiConjugate Gradient Stop Test.
!            This routine calculates the stop test for the BiConjugate
!            Gradient iteration scheme.  It returns a nonzero if the
!            error estimate (the type of which is determined by ITOL)
!            is less than the user specified tolerance TOL.
!***DESCRIPTION
! *Usage:
!     INTEGER  N, NELT, IA(NELT), JA(NELT), ISYM, ITOL, ITER
!     INTEGER  IERR, IUNIT, IWORK(USER DEFINED)
!     DOUBLE PRECISION B(N), X(N), A(N), TOL, ERR, R(N), Z(N), DZ(N)
!     DOUBLE PRECISION RWORK(USER DEFINED), AK, BK, BNRM, SOLNRM
!     EXTERNAL MSOLVE
!
!     IF( ISDBCG(N, B, X, NELT, IA, JA, A, ISYM, MSOLVE, ITOL, TOL,
!    $     ITER, ERR, IERR, IUNIT, R, Z, DZ, 
!    $     RWORK, IWORK, AK, BK, BNRM, SOLNRM) .NE. 0 )
!    $     THEN ITERATION DONE
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right-hand side vector.
! X      :INOUT    Double Precision X(N).
!         On input X is your initial guess for solution vector.
!         On output X is the final approximate solution.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays contain the matrix data structure for A.
!         It could take any form.  See "Description", in the SLAP 
!         routine DBCG for more late breaking details...
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
! MSOLVE :EXT      External.
!         Name of a routine which solves a linear system MZ = R  for Z
!         given R with the preconditioning matrix M (M is supplied via
!         RWORK  and IWORK arrays).   The name  of  the MSOLVE routine
!         must be declared  external  in the  calling   program.   The
!         calling sequence of MSLOVE is:
!             CALL MSOLVE(N, R, Z, NELT, IA, JA, A, ISYM, RWORK, IWORK)
!         Where N is the number of unknowns, R is  the right-hand side
!         vector, and Z is the solution upon return.  NELT,  IA, JA, A
!         and  ISYM define the SLAP  matrix  data structure: see  
!         Description, below.  RWORK is a  double precision array that 
!         can be used
!         to  pass   necessary  preconditioning     information and/or
!         workspace to MSOLVE.  IWORK is an integer work array for the
!         same purpose as RWORK.
! ITOL   :IN       Integer.
!         Flag to indicate type of convergence criterion.
!         If ITOL=1, iteration stops when the 2-norm of the residual 
!         divided by the 2-norm of the right-hand side is less than TOL.
!         If ITOL=2, iteration stops when the 2-norm of M-inv times the 
!         residual divided by the 2-norm of M-inv times the right hand 
!         side is less than tol, where M-inv is the inverse of the 
!         diagonal of A.
!         ITOL=11 is often useful for checking and comparing different 
!         routines.  For this case, the user must supply the "exact" 
!         solution or a very accurate approximation (one with an error 
!         much less than tol) through a common block,
!         COMMON /SOLBLK/ SOLN( )
!         if ITOL=11, iteration stops when the 2-norm of the difference 
!         between the iterative approximation and the user-supplied
!         solution divided by the 2-norm of the user-supplied solution 
!         is less than tol.
! TOL    :IN       Double Precision.
!         Convergence criterion, as described above.
! ITER   :OUT      Integer.
!         Number of iterations required to reach convergence, or 
!         ITMAX+1 if convergence criterion could not be achieved in 
!         ITMAX iterations.
! ERR    :OUT      Double Precision.
!         Error estimate of error in final approximate solution, as 
!         defined by ITOL.
! IERR   :OUT      Integer.
!         Error flag.  IERR is set to 3 if ITOL is not on of the 
!         acceptable values, see above. 
! IUNIT  :IN       Integer.
!         Unit number on which to write the error at each iteration, 
!         if this is desired for monitoring convergence.  If unit 
!         number is 0, no writing will occur.
! R      :IN       Double Precision R(N).
!         The residual r = b - Ax.
! Z      :WORK     Double Precision Z(N).
! DZ     :WORK     Double Precision DZ(N).
!         If ITOL.eq.0 then DZ is used to hold M-inv * B on the first
!         call.  If ITOL.eq.11 then DZ is used to hold X-SOLN.
! RWORK  :WORK     Double Precision RWORK(USER DEFINED).
!         Double Precision array that can be used for workspace in 
!         MSOLVE and MTSOLV.  
! IWORK  :WORK     Integer IWORK(USER DEFINED).
!         Integer array that can be used for workspace in MSOLVE
!         and MTSOLV.
! AK     :IN       Double Precision.
!         Current iterate BiConjugate Gradient iteration parameter.
! BK     :IN       Double Precision.
!         Current iterate BiConjugate Gradient iteration parameter.
! BNRM   :INOUT    Double Precision.
!         Norm of the right hand side.  Type of norm depends on ITOL.
!         Calculated only on the first call.
! SOLNRM :INOUT    Double Precision.
!         2-Norm of the true solution, SOLN.  Only computed and used
!         if ITOL = 11.
!
! *Function Return Values:
!       0 : Error estimate (determined by ITOL) is *NOT* less than the 
!           specified tolerance, TOL.  The iteration must continue.
!       1 : Error estimate (determined by ITOL) is less than the 
!           specified tolerance, TOL.  The iteration can be considered
!           complete.
!
! *Precision:           Double Precision
!***REFERENCES  (NONE)
!***ROUTINES CALLED  MSOLVE, DNRM2
!***COMMON BLOCKS    SOLBLK
!***END PROLOGUE  ISDBCG
      USE SOLBLK
      USE Slap, ONLY: DNRM2
      IMPLICIT NONE
      INTEGER :: ISDBCG
      INTEGER :: I
      INTEGER, INTENT(IN) ::  N, NELT, ISYM, ITOL, IUNIT
      INTEGER, INTENT(OUT) :: ITER, IERR
      INTEGER, DIMENSION(1) :: IWORK
      INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
      REAL(KIND=DP), INTENT(IN) :: TOL
      REAL(KIND=DP), INTENT(OUT) :: ERR
      REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B, R
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(N) :: X
      REAL(KIND=DP),  DIMENSION(N) :: Z, DZ
      REAL(KIND=DP), DIMENSION(*) :: RWORK
      REAL(KIND=DP), INTENT(IN) :: AK, BK
      REAL(KIND=DP), INTENT(INOUT) :: BNRM, SOLNRM
      EXTERNAL MSOLVE
!         
!***FIRST EXECUTABLE STATEMENT  ISDBCG
      ISDBCG = 0
!         
      IF( ITOL.EQ.1 ) THEN
!         err = ||Residual||/||RightHandSide|| (2-Norms).
         IF(ITER .EQ. 0) BNRM = DNRM2(N, B, 1)
         ERR = DNRM2(N, R, 1)/BNRM
      ELSE IF( ITOL.EQ.2 ) THEN
!                  -1              -1
!         err = ||M  Residual||/||M  RightHandSide|| (2-Norms).
         IF(ITER .EQ. 0) THEN
            CALL MSOLVE(N, B, DZ, NELT, IA, JA, A, ISYM, RWORK, IWORK)
            BNRM = DNRM2(N, DZ, 1)
         ENDIF
         ERR = DNRM2(N, Z, 1)/BNRM
      ELSE IF( ITOL.EQ.11 ) THEN
!         err = ||x-TrueSolution||/||TrueSolution|| (2-Norms).
         IF(ITER .EQ. 0) SOLNRM = DNRM2(N, SOLN, 1)
         DO I = 1, N
            DZ(I) = X(I) - SOLN(I)
         END DO
         ERR = DNRM2(N, DZ, 1)/SOLNRM
      ELSE
!
!         If we get here ITOL is not one of the acceptable values.
         ERR = 1.0E10
         IERR = 3
      ENDIF
!         
      IF(IUNIT .NE. 0) THEN
         IF( ITER.EQ.0 ) THEN
            WRITE(IUNIT,1000) N, ITOL
         ENDIF
         WRITE(IUNIT,1010) ITER, ERR, AK, BK
      ENDIF
      IF(ERR .LE. TOL) ISDBCG = 1
!         
      RETURN
 1000 FORMAT(' Preconditioned BiConjugate Gradient for N, ITOL = ', &
           I5,I5,/' ITER','   Error Estimate','            Alpha', &
           '             Beta')
 1010 FORMAT(1X,I4,1X,E16.7,1X,E16.7,1X,E16.7)
!------------- LAST LINE OF ISDBCG FOLLOWS ----------------------------
      END FUNCTION ISDBCG

    
!DECK DSDBCG
   
!DECK DSLUBC

!DECK ISDBCG
       SUBROUTINE DSILUS(N, NELT, IA, JA, A, ISYM, NL, IL, JL, &
          L, DINV, NU, IU, JU, U, NROW, NCOL)
!***BEGIN PROLOGUE  DSILUS
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSILUS-D),
!             Non-Symmetric Linear system, Sparse, 
!             Iterative Precondition, Incomplete LU Factorization
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Incomplete LU Decomposition Preconditioner SLAP Set Up.
!            Routine to generate the incomplete LDU decomposition of a 
!            matrix.  The  unit lower triangular factor L is stored by 
!            rows and the  unit upper triangular factor U is stored by 
!            columns.  The inverse of the diagonal matrix D is stored.
!            No fill in is allowed.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT), ISYM
!     INTEGER NL, IL(N+1), JL(NL), NU, IU(N+1), JU(NU)
!     INTEGER NROW(N), NCOL(N)
!     DOUBLE PRECISION A(NELT), L(NL), U(NU), DINV(N)
!
!     CALL DSILUS( N, NELT, IA, JA, A, ISYM, NL, IL, JL, L, 
!    $    DINV, NU, IU, JU, U, NROW, NCOL )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of elements in arrays IA, JA, and A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(NELT).
! A      :IN       Double Precision A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below. 
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the lower 
!         triangle of the matrix is stored.
! NL     :OUT      Integer.
!         Number of non-zeros in the EL array.
! IL     :OUT      Integer IL(N+1).
! JL     :OUT      Integer JL(NL).
! L      :OUT      Double Precision L(NL).
!         IL, JL, L  contain the unit ower  triangular factor of  the
!         incomplete decomposition  of some  matrix stored  in   SLAP
!         Row format.     The   Diagonal  of ones  *IS*  stored.  See
!         "DESCRIPTION", below for more details about the SLAP format.
! NU     :OUT      Integer.
!         Number of non-zeros in the U array.     
! IU     :OUT      Integer IU(N+1).
! JU     :OUT      Integer JU(NU).
! U      :OUT      Double Precision     U(NU).
!         IU, JU, U contain   the unit upper triangular factor of the
!         incomplete  decomposition    of some matrix  stored in SLAP
!         Column  format.   The Diagonal of ones   *IS*  stored.  See 
!         "Description", below  for  more  details  about  the   SLAP 
!         format.
! NROW   :WORK     Integer NROW(N).
!         NROW(I) is the number of non-zero elements in the I-th row
!         of L.
! NCOL   :WORK     Integer NCOL(N).
!         NCOL(I) is the number of non-zero elements in the I-th 
!         column of U.
!
! *Description
!       IL, JL, L should contain the unit  lower triangular factor of
!       the incomplete decomposition of the A matrix  stored in SLAP
!       Row format.  IU, JU, U should contain  the unit upper factor
!       of the  incomplete decomposition of  the A matrix  stored in
!       SLAP Column format This ILU factorization can be computed by
!       the DSILUS routine.  The diagonals (which is all one's) are
!       stored.
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       ==================== S L A P Row format ====================
!       This routine requires  that the matrix A  be  stored  in the
!       SLAP  Row format.   In this format  the non-zeros are stored
!       counting across  rows (except for the diagonal  entry, which
!       must appear first in each "row") and  are stored in the 
!       double precision
!       array A.  In other words, for each row in the matrix put the
!       diagonal entry in  A.   Then   put  in the   other  non-zero
!       elements   going  across the  row (except   the diagonal) in
!       order.   The  JA array  holds   the column   index for  each
!       non-zero.   The IA  array holds the  offsets into  the JA, A
!       arrays  for   the   beginning  of   each  row.   That    is,
!       JA(IA(IROW)),  A(IA(IROW)) points  to  the beginning  of the
!       IROW-th row in JA and A.   JA(IA(IROW+1)-1), A(IA(IROW+1)-1)
!       points to the  end of the  IROW-th row.  Note that we always
!       have IA(N+1) =  NELT+1, where  N  is  the number of rows  in
!       the matrix  and NELT  is the  number   of  non-zeros in  the
!       matrix.
!       
!       Here is an example of the SLAP Row storage format for a  5x5
!       Matrix (in the A and JA arrays '|' denotes the end of a row):
!
!           5x5 Matrix         SLAP Row format for 5x5 matrix on left.
!                              1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 12 15 | 22 21 | 33 35 | 44 | 55 51 53
!       |21 22  0  0  0|  JA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  IA:  1  4  6    8  9   12
!       | 0  0  0 44  0|  
!       |51  0 53  0 55|  
!
! *Precision:           Double Precision
! *See Also:
!       SILUR
!***REFERENCES  1. Gene Golub & Charles Van Loan, "Matrix Computations",
!                 John Hopkins University Press; 3 (1983) IBSN 
!                 0-8018-3010-9.
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSILUS
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N, NELT, ISYM 
      INTEGER :: NL, NU
      INTEGER, DIMENSION(NU) :: IU, JU
      INTEGER, DIMENSION(N) :: NROW, NCOL
      INTEGER, DIMENSION(NL) :: IL, JL
      INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
      REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A 
      REAL(KIND=DP), DIMENSION(NL) :: L
      REAL(KIND=DP), DIMENSION(N) :: DINV
      REAL(KIND=DP), DIMENSION(NU) :: U
      INTEGER :: I, IBGN, ICOL, IEND, INDX, INDX1, INDX2, INDXC1, &
         INDXC2, INDXR1, INDXR2, IROW, ITEMP, J, JBGN, JEND, JTEMP, &
	 K, KC, KR
      REAL(KIND=DP) :: TEMP
!         
!         Count number of elements in each row of the lower triangle.
!***FIRST EXECUTABLE STATEMENT  DSILUS
      DO I=1,N
         NROW(I) = 0
         NCOL(I) = 0
      END DO
!Commenting these POWER FORTRAN directives
!CVD$R NOCONCUR
!CVD$R NOVECTOR
      DO ICOL = 1, N
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO J = JBGN, JEND
               IF( IA(J).LT.ICOL ) THEN
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
                  NROW(IA(J)) = NROW(IA(J)) + 1
                  IF( ISYM.NE.0 ) NCOL(IA(J)) = NCOL(IA(J)) + 1
               ENDIF
            END DO
         ENDIF
      END DO
      JU(1) = 1
      IL(1) = 1
      DO ICOL = 1, N
         IL(ICOL+1) = IL(ICOL) + NROW(ICOL)
         JU(ICOL+1) = JU(ICOL) + NCOL(ICOL)
         NROW(ICOL) = IL(ICOL)
         NCOL(ICOL) = JU(ICOL)
      END DO
!         
!         Copy the matrix A into the L and U structures.
      DO ICOL = 1, N
         DINV(ICOL) = A(JA(ICOL))
         JBGN = JA(ICOL)+1
         JEND = JA(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
            DO J = JBGN, JEND
               IROW = IA(J)
               IF( IROW.LT.ICOL ) THEN
!         Part of the upper triangle.
                  IU(NCOL(ICOL)) = IROW
                  U(NCOL(ICOL)) = A(J)
                  NCOL(ICOL) = NCOL(ICOL) + 1
               ELSE
!         Part of the lower triangle (stored by row).
                  JL(NROW(IROW)) = ICOL
                  L(NROW(IROW)) = A(J)
                  NROW(IROW) = NROW(IROW) + 1
                  IF( ISYM.NE.0 ) THEN
!         Symmetric...Copy lower triangle into upper triangle as well.
                     IU(NCOL(IROW)) = ICOL
                     U(NCOL(IROW)) = A(J)
                     NCOL(IROW) = NCOL(IROW) + 1
                  ENDIF
               ENDIF
            END DO
         ENDIF
      END DO
!
!         Sort the rows of L and the columns of U.
      DO K = 2, N
         JBGN = JU(K)
         JEND = JU(K+1)-1
         IF( JBGN.LT.JEND ) THEN
            DO J = JBGN, JEND-1
               DO I = J+1, JEND
                  IF( IU(J).GT.IU(I) ) THEN
                     ITEMP = IU(J)
                     IU(J) = IU(I)
                     IU(I) = ITEMP
                     TEMP = U(J)
                     U(J) = U(I)
                     U(I) = TEMP
                  ENDIF
               END DO
            END DO
         ENDIF
         IBGN = IL(K)
         IEND = IL(K+1)-1
         IF( IBGN.LT.IEND ) THEN
            DO I = IBGN, IEND-1
               DO J = I+1, IEND
                  IF( JL(I).GT.JL(J) ) THEN
                     JTEMP = JU(I)
                     JU(I) = JU(J)
                     JU(J) = JTEMP
                     TEMP = L(I)
                     L(I) = L(J)
                     L(J) = TEMP
                  ENDIF
               END DO
            END DO
         ENDIF
      END DO

!
!         Perform the incomplete LDU decomposition.
      DO I=2,N
!         
!           I-th row of L
         INDX1 = IL(I)
         INDX2 = IL(I+1) - 1
         IF(INDX1 .LT. INDX2) THEN
	    DO INDX=INDX1,INDX2
		IF(INDX .NE. INDX1) THEN
		    INDXR1 = INDX1
		    INDXR2 = INDX - 1
		    INDXC1 = JU(JL(INDX))
		    INDXC2 = JU(JL(INDX)+1) - 1
		    IF(INDXC1 .LT. INDXC2) THEN
 160        		KR = JL(INDXR1)
 170        		KC = IU(INDXC1)
			IF(KR .GT. KC) THEN
			    INDXC1 = INDXC1 + 1
			    IF(INDXC1 .LE. INDXC2) GO TO 170
			ELSEIF(KR .LT. KC) THEN
			    INDXR1 = INDXR1 + 1
			    IF(INDXR1 .LE. INDXR2) GO TO 160
			ELSEIF(KR .EQ. KC) THEN
			    L(INDX) = L(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
			    INDXR1 = INDXR1 + 1
			    INDXC1 = INDXC1 + 1
			    IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 160
			ENDIF
		    END IF
		END IF
		L(INDX) = L(INDX)/DINV(JL(INDX))
	    END DO
	 END IF
!         
!         ith column of u
         INDX1 = JU(I)
         INDX2 = JU(I+1) - 1
         IF(INDX1 .GT. INDX2) THEN
	    DO INDX=INDX1,INDX2
		IF(INDX .NE. INDX1) THEN 
		    INDXC1 = INDX1
		    INDXC2 = INDX - 1
		    INDXR1 = IL(IU(INDX))
		    INDXR2 = IL(IU(INDX)+1) - 1
		    IF(INDXR1 .LT. INDXR2) THEN
 210        		KR = JL(INDXR1)
 220        		KC = IU(INDXC1)
			IF(KR .GT. KC) THEN
			    INDXC1 = INDXC1 + 1
			    IF(INDXC1 .LE. INDXC2) GO TO 220
			ELSEIF(KR .LT. KC) THEN
			    INDXR1 = INDXR1 + 1
			    IF(INDXR1 .LE. INDXR2) GO TO 210
			ELSEIF(KR .EQ. KC) THEN
			    U(INDX) = U(INDX) - L(INDXR1)*DINV(KC)*U(INDXC1)
			    INDXR1 = INDXR1 + 1
			    INDXC1 = INDXC1 + 1
			    IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 210
			ENDIF
		    ENDIF
		ENDIF
         	U(INDX) = U(INDX)/DINV(IU(INDX))
	    END DO
	 END IF
!         
!         ith diagonal element
         INDXR1 = IL(I)
         INDXR2 = IL(I+1) - 1
         IF(INDXR1 .GT. INDXR2) EXIT
         INDXC1 = JU(I)
         INDXC2 = JU(I+1) - 1
         IF(INDXC1 .GT. INDXC2) EXIT
 270     KR = JL(INDXR1)
 280     KC = IU(INDXC1)
         IF(KR .GT. KC) THEN
            INDXC1 = INDXC1 + 1
            IF(INDXC1 .LE. INDXC2) GO TO 280
         ELSEIF(KR .LT. KC) THEN
            INDXR1 = INDXR1 + 1
            IF(INDXR1 .LE. INDXR2) GO TO 270
         ELSEIF(KR .EQ. KC) THEN
            DINV(I) = DINV(I) - L(INDXR1)*DINV(KC)*U(INDXC1)
            INDXR1 = INDXR1 + 1
            INDXC1 = INDXC1 + 1
            IF(INDXR1 .LE. INDXR2 .AND. INDXC1 .LE. INDXC2) GO TO 270
         ENDIF
!         
      END DO
!         
!         replace diagonal lts by their inverses.
!Commenting Power Fortran directive
!CVD$ VECTOR
      DO I=1,N
         DINV(I) = 1./DINV(I)
      END DO
!         
      RETURN
!------------- LAST LINE OF DSILUS FOLLOWS ----------------------------
      END SUBROUTINE DSILUS
      SUBROUTINE DSLUI(N, B, X, RWORK, IWORK)
!***BEGIN PROLOGUE  DSLUI
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSLUI-S),
!             Non-Symmetric Linear system solve, Sparse, 
!             Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP MSOLVE for LDU Factorization.
!            This routine  acts as an  interface between  the   SLAP
!            generic MSLOVE calling convention and the routine  that 
!            actually computes:     -1
!                              (LDU)  B = X.
!***DESCRIPTION
!       See the "DESCRIPTION" of DSLUI2 for the gory details.
!***ROUTINES CALLED  DSLUI2
!***END PROLOGUE  DSLUI
      USE double
      IMPLICIT NONE
      INTEGER :: N
      INTEGER, DIMENSION(*) :: IWORK
      REAL(KIND=DP), DIMENSION(N) :: B, X 
      REAL(KIND=DP), DIMENSION(*) :: RWORK
      INTEGER :: LOCIL, LOCJL, LOCIU, LOCJU, LOCL, LOCDIN, LOCU
!
!         Pull out the locations of the arrays holding the ILU
!         factorization.
!***FIRST EXECUTABLE STATEMENT  DSLUI
      LOCIL = IWORK(1)
      LOCJL = IWORK(2)
      LOCIU = IWORK(3)
      LOCJU = IWORK(4)
      LOCL = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU = IWORK(7)
!
!         Solve the system LUx = b
      CALL DSLUI2(N, B, X, IWORK(LOCIL), IWORK(LOCJL), RWORK(LOCL), &
           RWORK(LOCDIN), IWORK(LOCIU), IWORK(LOCJU), RWORK(LOCU) )
!         
      RETURN
!------------- LAST LINE OF DSLUI FOLLOWS ----------------------------
      END SUBROUTINE DSLUI
!DECK DSLUI2
      SUBROUTINE DSLUI2(N, B, X, IL, JL, L, DINV, IU, JU, U )
!***BEGIN PROLOGUE  DSLUI2
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSLUI2-S),
!             Non-Symmetric Linear system solve, Sparse, 
!             Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Back solve for LDU Factorization.
!            Routine  to  solve a system of the form  L*D*U X  =  B,
!            where L is a unit  lower  triangular  matrix,  D  is  a 
!            diagonal matrix, and U is a unit upper triangular matrix.
!***DESCRIPTION
! *Usage:
!     INTEGER N, IL(N+1), JL(NL), IU(NU), JU(N+1)
!     DOUBLE PRECISION B(N), X(N), L(NL), DINV(N), U(NU)
!
!     CALL DSLUI2( N, B, X, IL, JL, L, DINV, IU, JU, U )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right hand side.
! X      :OUT      Double Precision X(N).
!         Solution of L*D*U x = b.
! NEL    :IN       Integer.
!         Number of non-zeros in the EL array.
! IL     :IN       Integer IL(N+1).
! JL     :IN       Integer JL(NL).
!  L     :IN       Double Precision L(NL).
!         IL, JL, L contain the unit  lower triangular factor of the
!         incomplete decomposition of some matrix stored in SLAP Row
!         format.  The diagonal of ones *IS* stored.  This structure
!         can   be   set up  by   the  DSILUS routine.   See 
!         "DESCRIPTION", below  for more   details about   the  SLAP
!         format.
! DINV   :IN       Double Precision DINV(N).
!         Inverse of the diagonal matrix D.
! NU     :IN       Integer.
!         Number of non-zeros in the U array.     
! IU     :IN       Integer IU(N+1).
! JU     :IN       Integer JU(NU).
! U      :IN       Double Precision U(NU).
!         IU, JU, U contain the unit upper triangular factor  of the
!         incomplete decomposition  of  some  matrix stored in  SLAP
!         Column format.   The diagonal of ones  *IS* stored.   This
!         structure can be set up  by the DSILUS routine.  See
!         "DESCRIPTION", below   for  more   details about  the SLAP
!         format.
!
! *Description:
!       This routine is supplied with  the SLAP package as a routine
!       to  perform  the  MSOLVE operation  in   the  SIR and   SBCG
!       iteration routines for  the  drivers DSILUR and DSLUBC.   It
!       must  be called  via   the  SLAP  MSOLVE  calling   sequence
!       convention interface routine DSLUI.
!         **** THIS ROUTINE ITSELF DOES NOT CONFORM TO THE ****
!               **** SLAP MSOLVE CALLING CONVENTION ****
!
!       IL, JL, L should contain the unit lower triangular factor of
!       the incomplete decomposition of the A matrix  stored in SLAP
!       Row format.  IU, JU, U should contain  the unit upper factor
!       of the  incomplete decomposition of  the A matrix  stored in
!       SLAP Column format This ILU factorization can be computed by
!       the DSILUS routine.  The diagonals (which is all one's) are
!       stored.
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       ==================== S L A P Row format ====================
!       This routine requires  that the matrix A  be  stored  in the
!       SLAP  Row format.   In this format  the non-zeros are stored
!       counting across  rows (except for the diagonal  entry, which
!       must appear first in each "row") and  are stored in the 
!       double precision
!       array A.  In other words, for each row in the matrix put the
!       diagonal entry in  A.   Then   put  in the   other  non-zero
!       elements   going  across the  row (except   the diagonal) in
!       order.   The  JA array  holds   the column   index for  each
!       non-zero.   The IA  array holds the  offsets into  the JA, A
!       arrays  for   the   beginning  of   each  row.   That    is,
!       JA(IA(IROW)),  A(IA(IROW)) points  to  the beginning  of the
!       IROW-th row in JA and A.   JA(IA(IROW+1)-1), A(IA(IROW+1)-1)
!       points to the  end of the  IROW-th row.  Note that we always
!       have IA(N+1) =  NELT+1, where  N  is  the number of rows  in
!       the matrix  and NELT  is the  number   of  non-zeros in  the
!       matrix.
!       
!       Here is an example of the SLAP Row storage format for a  5x5
!       Matrix (in the A and JA arrays '|' denotes the end of a row):
!
!           5x5 Matrix         SLAP Row format for 5x5 matrix on left.
!                              1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 12 15 | 22 21 | 33 35 | 44 | 55 51 53
!       |21 22  0  0  0|  JA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  IA:  1  4  6    8  9   12
!       | 0  0  0 44  0|  
!       |51  0 53  0 55|  
!
!       With  the SLAP  format  the "inner  loops" of  this  routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!
! *Precision:           Double Precision
! *See Also:
!       DSILUS
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSLUI2
      USE double
      IMPLICIT NONE
      INTEGER :: N, I, ICOL, IROW, J, JBGN, JEND
      INTEGER, DIMENSION(1) :: IL, JL, IU, JU
      REAL(KIND=DP), DIMENSION(N) :: B, X, DINV 
      REAL(KIND=DP), DIMENSION(1) :: L, U
!         
!         Solve  L*Y = B,  storing result in X, L stored by rows.
!***FIRST EXECUTABLE STATEMENT  DSLUI2
      DO I = 1, N
         X(I) = B(I)
      END DO
      DO IROW = 2, N
         JBGN = IL(IROW)
         JEND = IL(IROW+1)-1
         IF( JBGN.LE.JEND ) THEN
!Commenting these POWER FORTRAN directives
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ ASSOC
!CVD$ NODEPCHK
            DO J = JBGN, JEND
               X(IROW) = X(IROW) - L(J)*X(JL(J))
            END DO
         ENDIF
      END DO
!         
!         Solve  D*Z = Y,  storing result in X.
      DO I=1,N
         X(I) = X(I)*DINV(I)
      END DO
!         
!         Solve  U*X = Z, U stored by columns.
      DO ICOL = N, 2, -1
         JBGN = JU(ICOL)
         JEND = JU(ICOL+1)-1
         IF( JBGN.LE.JEND ) THEN
!Commenting Power Fortran directives
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ NODEPCHK
            DO J = JBGN, JEND
               X(IU(J)) = X(IU(J)) - U(J)*X(ICOL)
            END DO
         ENDIF
      END DO
!         
      RETURN
!------------- LAST LINE OF DSLUI2 FOLLOWS ----------------------------
      END SUBROUTINE DSLUI2
!DECK DSLUTI
      SUBROUTINE DSLUTI(N, B, X, RWORK, IWORK)
!***BEGIN PROLOGUE  DSLUTI
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSLUTI-S),
!             Linear system solve, Sparse, Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP MTSOLV for LDU Factorization.
!            This routine acts as  an  interface  between  the  SLAP
!            generic MTSOLV calling convention and  the routine that  
!            actually computes:       -T
!                                (LDU)  B = X.
!***DESCRIPTION
!       See the "DESCRIPTION" of DSLUI4 for the gory details.
!***ROUTINES CALLED  DSLUI4
!***END PROLOGUE  DSLUTI
      USE double
      IMPLICIT NONE
      INTEGER :: N, LOCDIN, LOCIL, LOCIU, LOCJL, LOCJU, LOCL, LOCU
      INTEGER, DIMENSION(*) :: IWORK
      REAL(KIND=DP), DIMENSION(N) :: B, X
      REAL(KIND=DP), DIMENSION(*) :: RWORK
!         
!         Pull out the pointers to the L, D and U matricies and call
!         the workhorse routine.
!***FIRST EXECUTABLE STATEMENT  DSLUTI
      LOCIL = IWORK(1)
      LOCJL = IWORK(2)
      LOCIU = IWORK(3)
      LOCJU = IWORK(4)
      LOCL = IWORK(5)
      LOCDIN = IWORK(6)
      LOCU = IWORK(7)
!
      CALL DSLUI4(N, B, X, IWORK(LOCIL), IWORK(LOCJL), RWORK(LOCL), &
          RWORK(LOCDIN), IWORK(LOCIU), IWORK(LOCJU), RWORK(LOCU))
!         
      RETURN
!------------- LAST LINE OF DSLUTI FOLLOWS ----------------------------
      END SUBROUTINE DSLUTI
!DECK DSLUI4
      SUBROUTINE DSLUI4(N, B, X, IL, JL, L, DINV, IU, JU, U )
!***BEGIN PROLOGUE  DSLUI4
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSLUI4-S),
!             Non-Symmetric Linear system solve, Sparse, 
!             Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP back solve for LDU Factorization.
!            Routine to solve a system of the form  (L*D*U)' X =  B,
!            where L is a unit  lower  triangular  matrix,  D  is  a 
!            diagonal matrix, and  U  is  a  unit  upper  triangular 
!            matrix and ' denotes transpose.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NL, IL(N+1), JL(NL), NU, IU(N+1), JU(NU)
!     DOUBLE PRECISION B(N), X(N), L(NEL), DINV(N), U(NU)
!
!     CALL DSLUI4( N, B, X, IL, JL, L, DINV, IU, JU, U )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Right hand side.
! X      :OUT      Double Precision X(N).
!         Solution of (L*D*U)trans x = b.
! IL     :IN       Integer IL(N+1).
! JL     :IN       Integer JL(NL).
!  L     :IN       Double Precision L(NL).
!         IL, JL, L contain the unit lower triangular  factor of the
!         incomplete decomposition of some matrix stored in SLAP Row
!         format.  The diagonal of ones *IS* stored.  This structure
!         can    be set  up  by   the  DSILUS routine.   See  
!         "DESCRIPTION",  below for  more  details about  the   SLAP
!         format.
! DINV   :IN       Double Precision DINV(N).
!         Inverse of the diagonal matrix D.
! IU     :IN       Integer IU(N+1).
! JU     :IN       Integer JU(NU).
! U      :IN       Double Precision U(NU).
!         IU, JU, U contain the  unit upper triangular factor of the
!         incomplete  decomposition of some  matrix stored  in  SLAP
!         Column  format.   The diagonal of  ones *IS* stored.  This
!         structure can be set up by the  DSILUS routine.  See
!         "DESCRIPTION",  below for  more  details  about  the  SLAP
!         format.
!
! *Description:
!       This routine is supplied with the SLAP package as  a routine
!       to  perform  the  MTSOLV  operation  in  the SBCG  iteration
!       routine for the  driver DSLUBC.   It must  be called via the
!       SLAP  MTSOLV calling  sequence convention interface  routine
!       DSLUTI.
!         **** THIS ROUTINE ITSELF DOES NOT CONFORM TO THE ****
!               **** SLAP MSOLVE CALLING CONVENTION ****
!
!       IL, JL, L should contain the unit lower triangular factor of
!       the incomplete decomposition of the A matrix  stored in SLAP
!       Row format.  IU, JU, U should contain  the unit upper factor
!       of the  incomplete decomposition of  the A matrix  stored in
!       SLAP Column format This ILU factorization can be computed by
!       the DSILUS routine.  The diagonals (which is all one's) are
!       stored.
!
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       ==================== S L A P Row format ====================
!       This routine requires  that the matrix A  be  stored  in the
!       SLAP  Row format.   In this format  the non-zeros are stored
!       counting across  rows (except for the diagonal  entry, which
!       must appear first in each "row") and  are stored in the 
!       double precision
!       array A.  In other words, for each row in the matrix put the
!       diagonal entry in  A.   Then   put  in the   other  non-zero
!       elements   going  across the  row (except   the diagonal) in
!       order.   The  JA array  holds   the column   index for  each
!       non-zero.   The IA  array holds the  offsets into  the JA, A
!       arrays  for   the   beginning  of   each  row.   That    is,
!       JA(IA(IROW)),  A(IA(IROW)) points  to  the beginning  of the
!       IROW-th row in JA and A.   JA(IA(IROW+1)-1), A(IA(IROW+1)-1)
!       points to the  end of the  IROW-th row.  Note that we always
!       have IA(N+1) =  NELT+1, where  N  is  the number of rows  in
!       the matrix  and NELT  is the  number   of  non-zeros in  the
!       matrix.
!       
!       Here is an example of the SLAP Row storage format for a  5x5
!       Matrix (in the A and JA arrays '|' denotes the end of a row):
!
!           5x5 Matrix         SLAP Row format for 5x5 matrix on left.
!                              1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 12 15 | 22 21 | 33 35 | 44 | 55 51 53
!       |21 22  0  0  0|  JA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  IA:  1  4  6    8  9   12
!       | 0  0  0 44  0|  
!       |51  0 53  0 55|  
!
!       With  the SLAP  format  the "inner  loops" of  this  routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!
! *Precision:           Double Precision
! *See Also:
!       DSILUS
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSLUI4
      USE double
      IMPLICIT NONE
      INTEGER :: N, I, ICOL, IROW, J, JBGN, JEND
      INTEGER, DIMENSION(*) :: IL, JL, IU, JU
      REAL(KIND=DP), DIMENSION(N) :: B, X, DINV
      REAL(KIND=DP), DIMENSION(*) :: L, U
!         
!***FIRST EXECUTABLE STATEMENT  DSLUI4
      DO I=1,N
         X(I) = B(I)
      END DO
!         
!         Solve  U'*Y = X,  storing result in X, U stored by columns.
      DO IROW = 2, N
         JBGN = JU(IROW)
         JEND = JU(IROW+1) - 1
         IF( JBGN.LE.JEND ) THEN
!Commenting Power Fortran directives
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ ASSOC
!CVD$ NODEPCHK
            DO J = JBGN, JEND
               X(IROW) = X(IROW) - U(J)*X(IU(J))
            END DO
         ENDIF
      END DO
!         
!         Solve  D*Z = Y,  storing result in X.
      DO I = 1, N
         X(I) = X(I)*DINV(I)
      END DO
!         
!         Solve  L'*X = Z, L stored by rows.
      DO ICOL = N, 2, -1
         JBGN = IL(ICOL)
         JEND = IL(ICOL+1) - 1
         IF( JBGN.LE.JEND ) THEN
!Commenting Power Fortran directives
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ NODEPCHK
            DO J = JBGN, JEND
               X(JL(J)) = X(JL(J)) - L(J)*X(ICOL)
            END DO
         ENDIF
      END DO
      RETURN
!------------- LAST LINE OF DSLUI4 FOLLOWS ----------------------------
      END SUBROUTINE DSLUI4
!DECK xerprt
      subroutine xerprt(messg)
!***begin prologue  xerprt
!***date written   790801   (yymmdd)
!***revision date  851213   (yymmdd)
!***category no.  r3
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  print error messages.
!***description
!
!     abstract
!        print the hollerith message in messg,
!        on each file indicated by xgetua.
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  i1mach,xgetua
!***end prologue  xerprt
      IMPLICIT NONE
      integer, external :: i1mach
      integer, dimension(5) :: lun
      character*(*) messg
      integer :: ichar, iunit, kunit, last, lenmes, nunit
!     obtain unit numbers and write line to each unit
!***first executable statement  xerprt
      call xgetua(lun,nunit)
      lenmes = len(messg)
      do kunit=1,nunit
         iunit = lun(kunit)
         if (iunit.eq.0) iunit = i1mach(4)
         do ichar=1,lenmes,72
            last = min0(ichar+71 , lenmes)
            write (iunit,'(1x,a)') messg(ichar:last)
         end do
      end do
      return
      end subroutine xerprt
!DECK xerror
      subroutine xerror(messg,nmessg,nerr,level)
!***begin prologue  xerror
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  process an error (diagnostic) message.
!***description
!
!     abstract
!        xerror processes a diagnostic message, in a manner
!        determined by the value of level and the current value
!        of the library error control flag, kontrl.
!        (see subroutine xsetf for details.)
!
!     description of parameters
!      --input--
!        messg - the hollerith message to be processed, containing
!                no more than 72 characters.
!        nmessg- the actual number of characters in messg.
!        nerr  - the error number associated with this message.
!                nerr must not be zero.
!        level - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (i.e., it is
!                   non-fatal if xsetf has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!
!     examples
!        call xerror('smooth -- num was zero.',23,1,2)
!        call xerror('integ  -- less than full accuracy achieved.',
!    1                43,2,1)
!        call xerror('rooter -- actual zero of f found before interval f
!    1ully collapsed.',65,3,0)
!        call xerror('exp    -- underflows being set to zero.',39,1,-1)
!
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  xerrwv
!***end prologue  xerror
      USE double
      IMPLICIT NONE
      character*(*) messg
      integer, intent(in) :: nmessg, nerr, level
!***first executable statement  xerror
      call xerrwv(messg,nmessg,nerr,level,0,0,0,0,0._DP,0._DP)
      return
      end subroutine xerror
!DECK xerrwv
      subroutine xerrwv(messg,nmessg,nerr,level,ni,i1,i2,nr,r1,r2)
!***begin prologue  xerrwv
!***date written   800319   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  process an error message allowing 2 integer and 2 real
!            values to be included in the message.
!***description
!
!     abstract
!        xerrwv processes a diagnostic message, in a manner
!        determined by the value of level and the current value
!        of the library error control flag, kontrl.
!        (see subroutine xsetf for details.)
!        in addition, up to two integer values and two real
!        values may be printed along with the message.
!
!     description of parameters
!      --input--
!        messg - the hollerith message to be processed.
!        nmessg- the actual number of characters in messg.
!        nerr  - the error number associated with this message.
!                nerr must not be zero.
!        level - error category.
!                =2 means this is an unconditionally fatal error.
!                =1 means this is a recoverable error.  (i.e., it is
!                   non-fatal if xsetf has been appropriately called.)
!                =0 means this is a warning message only.
!                =-1 means this is a warning message which is to be
!                   printed at most once, regardless of how many
!                   times this call is executed.
!        ni    - number of integer values to be printed. (0 to 2)
!        i1    - first integer value.
!        i2    - second integer value.
!        nr    - number of real values to be printed. (0 to 2)
!        r1    - first real value.
!        r2    - second real value.
!
!     examples
!        call xerrwv('smooth -- num (=i1) was zero.',29,1,2,
!    1   1,num,0,0,0.,0.)
!        call xerrwv('quadxy -- requested error (r1) less than minimum (
!    1r2).,54,77,1,0,0,0,2,errreq,errmin)
!
!     latest revision ---  1 august 1985
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  fdump,i1mach,j4save,xerprt,xersav,
!                    xgetua
!***end prologue  xerrwv
      USE double
      IMPLICIT NONE
      integer :: j4save, i1mach
      character*(*) messg
      character*20 lfirst
      character*37 form
      integer, dimension(5) :: lun
      integer :: nmessg, nerr, level, ni, i1, i2, nr, isizef, isizei, &
         ifatal, iunit, junk, kdummy, kount, kunit, lerr, lkntrl, &
	 maxmes, mkntrl, nunit, i, llevel, lmessg
      REAL(KIND=DP) :: r1,r2
!     get flags
!***first executable statement  xerrwv
      lkntrl = j4save(2,0,.false.)
      maxmes = j4save(4,0,.false.)
!     check for valid input
      if ((nmessg.gt.0).and.(nerr.ne.0).and. &
          (level.ge.(-1)).and.(level.le.2)) go to 10
         if (lkntrl.gt.0) call xerprt('fatal error in...')
         call xerprt('xerror -- invalid input')
!        if (lkntrl.gt.0) call fdump
         if (lkntrl.gt.0) call xerprt('job abort due to fatal error.')
         if (lkntrl.gt.0) call xersav(' ',0,0,0,kdummy)
	stop
         return
   10 continue
!     record message
      junk = j4save(1,nerr,.true.)
      call xersav(messg,nmessg,nerr,level,kount)
!     let user override
      lfirst = messg
      lmessg = nmessg
      lerr = nerr
      llevel = level
!     reset to original values
      lmessg = nmessg
      lerr = nerr
      llevel = level
      lkntrl = max0(-2,min0(2,lkntrl))
      mkntrl = iabs(lkntrl)
!     decide whether to print message
      if ((llevel.lt.2).and.(lkntrl.eq.0)) go to 100
      if (((llevel.eq.(-1)).and.(kount.gt.min0(1,maxmes))) &
      .or.((llevel.eq.0)   .and.(kount.gt.maxmes)) &
      .or.((llevel.eq.1)   .and.(kount.gt.maxmes).and.(mkntrl.eq.1)) &
      .or.((llevel.eq.2)   .and.(kount.gt.max0(1,maxmes)))) go to 100
         if (lkntrl.le.0) go to 20
            call xerprt(' ')
!           introduction
            if (llevel.eq.(-1)) call xerprt('warning message...this message will only be printed once.')
            if (llevel.eq.0) call xerprt('warning in...')
            if (llevel.eq.1) call xerprt ('recoverable error in...')
            if (llevel.eq.2) call xerprt('fatal error in...')
   20    continue
!        message
         call xerprt(messg)
         call xgetua(lun,nunit)
         isizei = int(log10(float(i1mach(9))) + 1.0)
         isizef = int(log10(float(i1mach(10))**i1mach(11)) + 1.0)
         do kunit=1,nunit
            iunit = lun(kunit)
            if (iunit.eq.0) iunit = i1mach(4)
            do i=1,min(ni,2)
               write (form,21) i,isizei
   21          format ('(11x,21hin above message, i',i1,'=,i',i2,')   ')
               if (i.eq.1) write (iunit,form) i1
               if (i.eq.2) write (iunit,form) i2
            end do
            do i=1,min(nr,2)
               write (form,23) i,isizef+10,isizef
   23          format ('(11x,21hin above message, r',i1,'=,e', &
               i2,'.',i2,')')
               if (i.eq.1) write (iunit,form) r1
               if (i.eq.2) write (iunit,form) r2
            end do
            if (lkntrl.le.0) go to 40
!              error number
               write (iunit,30) lerr
   30          format (15h error number =,i10)
   40       continue
 	 end do
!        trace-back
!        if (lkntrl.gt.0) call fdump
  100 continue
      ifatal = 0
      if ((llevel.eq.2).or.((llevel.eq.1).and.(mkntrl.eq.2))) then
	 ifatal = 1
      end if
!     quit here if message is not fatal
      if (ifatal.le.0) return
      if ((lkntrl.le.0).or.(kount.gt.max0(1,maxmes))) go to 120
!        print reason for abort
         if (llevel.eq.1) call xerprt('job abort due to unrecovered error.')
         if (llevel.eq.2) call xerprt('job abort due to fatal error.')
!        print error summary
         call xersav(' ',-1,0,0,kdummy)
  120 continue
!     abort
      if ((llevel.eq.2).and.(kount.gt.max0(1,maxmes))) lmessg = 0
	stop
      return
      end subroutine xerrwv
!DECK xersav
      subroutine xersav(messg,nmessg,nerr,level,icount)
!***begin prologue  xersav
!***date written   800319   (yymmdd)
!***revision date  851213   (yymmdd)
!***category no.  r3
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  record that an error has occurred.
!***description
!
!     abstract
!        record that this error occurred.
!
!     description of parameters
!     --input--
!       messg, nmessg, nerr, level are as in xerror,
!       except that when nmessg=0 the tables will be
!       dumped and cleared, and when nmessg is less than zero the
!       tables will be dumped and not cleared.
!     --output--
!       icount will be the number of times this message has
!       been seen, or zero if the table has overflowed and
!       does not contain this message specifically.
!       when nmessg=0, icount will not be altered.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  i1mach,xgetua
!***end prologue  xersav
      IMPLICIT NONE
      integer :: nmessg, nerr, level, icount, i, ii, iunit, kountx, &
		kunit, nunit
      integer, dimension(5) :: lun
      character*(*) messg
      character*20 mestab(10),mes
      integer, dimension(10), save :: nertab, levtab, kount
      integer, external :: i1mach
      save mestab,kountx
!     next two data statements are necessary to provide a blank
!     error table initially
      data kount(1),kount(2),kount(3),kount(4),kount(5), &
           kount(6),kount(7),kount(8),kount(9),kount(10) &
           /0,0,0,0,0,0,0,0,0,0/
      data kountx/0/
!***first executable statement  xersav
      if (nmessg.gt.0) go to 80
!     dump the table
         if (kount(1).eq.0) return
!        print to each unit
         call xgetua(lun,nunit)
         do kunit=1,nunit
            iunit = lun(kunit)
            if (iunit.eq.0) iunit = i1mach(4)
!           print table header
            write (iunit,10)
   10       format (32h0          error message summary/ &
            51h message start             nerr     level     count)
!           print body of table
            do i=1,10
               if (kount(i).eq.0) go to 30
               write (iunit,15) mestab(i),nertab(i),levtab(i),kount(i)
   15          format (1x,a20,3i10)
   	    end do
   30       continue
!           print number of other errors
            if (kountx.ne.0) write (iunit,40) kountx
   40       format (41h0other errors not individually tabulated=,i10)
            write (iunit,50)
   50       format (1x)
   	 end do
         if (nmessg.lt.0) return
!        clear the error tables
         do i=1,10
            kount(i) = 0
         end do
         kountx = 0
         return
   80 continue
!     process a message...
!     search for this messg, or else an empty slot for this messg,
!     or else determine that the error table is full.
      mes = messg
      do 90 i=1,10
         ii = i
         if (kount(i).eq.0) go to 110
         if (mes.ne.mestab(i)) go to 90
         if (nerr.ne.nertab(i)) go to 90
         if (level.ne.levtab(i)) go to 90
         go to 100
   90 continue
!     three possible cases...
!     table is full
         kountx = kountx+1
         icount = 1
         return
!     message found in table
  100    kount(ii) = kount(ii) + 1
         icount = kount(ii)
         return
!     empty slot found for new message
  110    mestab(ii) = mes
         nertab(ii) = nerr
         levtab(ii) = level
         kount(ii)  = 1
         icount = 1
         return
      end subroutine xersav
      subroutine xgetf(kontrl)
!***begin prologue  xgetf
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return the current value of the error control flag.
!***description
!
!   abstract
!        xgetf returns the current value of the error control flag
!        in kontrl.  see subroutine xsetf for flag value meanings.
!        (kontrl is an output parameter only.)
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetf
!***first executable statement  xgetf
      implicit none
      integer :: kontrl
      integer, external :: j4save
      kontrl = j4save(2,0,.false.)
      return
      end subroutine xgetf
!DECK xgetua
      subroutine xgetua(iunita,n)
!***begin prologue  xgetua
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return unit number(s) to which error messages are being
!            sent.
!***description
!
!     abstract
!        xgetua may be called to determine the unit number or numbers
!        to which error messages are being sent.
!        these unit numbers may have been set by a call to xsetun,
!        or a call to xsetua, or may be a default value.
!
!     description of parameters
!      --output--
!        iunit - an array of one to five unit numbers, depending
!                on the value of n.  a value of zero refers to the
!                default unit, as defined by the i1mach machine
!                constant routine.  only iunit(1),...,iunit(n) are
!                defined by xgetua.  the values of iunit(n+1),...,
!                iunit(5) are not defined (for n .lt. 5) or altered
!                in any way by xgetua.
!        n     - the number of units to which copies of the
!                error messages are being sent.  n will be in the
!                range from 1 to 5.
!
!     latest revision ---  19 mar 1980
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetua
      IMPLICIT NONE
      integer, external :: j4save
      integer, dimension(5) :: iunita
      integer :: i, index, n
!***first executable statement  xgetua
      n = j4save(5,0,.false.)
      do i=1,n
         index = i+4
         if (i.eq.1) index = 3
         iunita(i) = j4save(index,0,.false.)
      end do
      return
      end subroutine xgetua
!DECK j4save
      function j4save(iwhich,ivalue,iset)
!***begin prologue  j4save
!***refer to  xerror
!***routines called  (none)
!***description
!
!     abstract
!        j4save saves and recalls several global variables needed
!        by the library error handling routines.
!
!     description of parameters
!      --input--
!        iwhich - index of item desired.
!                = 1 refers to current error number.
!                = 2 refers to current error control flag.
!                 = 3 refers to current unit number to which error
!                    messages are to be sent.  (0 means use standard.)
!                 = 4 refers to the maximum number of times any
!                     message is to be printed (as set by xermax).
!                 = 5 refers to the total number of units to which
!                     each error message is to be written.
!                 = 6 refers to the 2nd unit for error messages
!                 = 7 refers to the 3rd unit for error messages
!                 = 8 refers to the 4th unit for error messages
!                 = 9 refers to the 5th unit for error messages
!        ivalue - the value to be set for the iwhich-th parameter,
!                 if iset is .true. .
!        iset   - if iset=.true., the iwhich-th parameter will be
!                 given the value, ivalue.  if iset=.false., the
!                 iwhich-th parameter will be unchanged, and ivalue
!                 is a dummy parameter.
!      --output--
!        the (old) value of the iwhich-th parameter will be returned
!        in the function value, j4save.
!
!     written by ron jones, with slatec common math library subcommittee
!    adapted from bell laboratories port library error handler
!     latest revision ---  1 august 1985
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***end prologue  j4save
      IMPLICIT NONE
      integer :: j4save, iwhich, ivalue
      logical :: iset
      integer, dimension(9), save :: iparam
      data iparam(1),iparam(2),iparam(3),iparam(4)/0,2,0,10/
      data iparam(5)/1/
      data iparam(6),iparam(7),iparam(8),iparam(9)/0,0,0,0/
!***first executable statement  j4save
      j4save = iparam(iwhich)
      if (iset) iparam(iwhich) = ivalue
      return
      end function j4save
!DECK xerclr
      subroutine xerclr
!***begin prologue  xerclr
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  reset current error number to zero.
!***description
!
!     abstract
!        this routine simply resets the current error number to zero.
!        this may be necessary to do in order to determine that
!        a certain error has occurred again since the last time
!        numxer was referenced.
!
!     written by ron jones, with slatec common math library subcommittee
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xerclr
!***first executable statement  xerclr
      implicit none
      integer :: j4save
      integer :: junk
      junk = j4save(1,0,.true.)
      return
      end subroutine xerclr
      subroutine xerdmp
!***begin prologue  xerdmp
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  print the error tables and then clear them.
!***description
!
!     abstract
!        xerdmp prints the error tables, then clears them.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  xersav
!***end prologue  xerdmp
!***first executable statement  xerdmp
      implicit none
      integer :: kount
      call xersav(' ',0,0,0,kount)
      return
      end subroutine xerdmp
      subroutine xermax(max)
!***begin prologue  xermax
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set maximum number of times any error message is to be
!            printed.
!***description
!
!     abstract
!        xermax sets the maximum number of times any message
!        is to be printed.  that is, non-fatal messages are
!        not to be printed after they have occured max times.
!        such non-fatal messages may be printed less than
!        max times even if they occur max times, if error
!        suppression mode (kontrl=0) is ever in effect.
!
!     description of parameter
!      --input--
!        max - the maximum number of times any one message
!              is to be printed.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xermax
!***first executable statement  xermax
      implicit none
      integer :: max, junk
      integer :: j4save
      junk = j4save(4,max,.true.)
      return
      end subroutine xermax
      subroutine xgetun(iunit)
!***begin prologue  xgetun
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3c
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  return the (first) output file to which error messages
!            are being sent.
!***description
!
!     abstract
!        xgetun gets the (first) output file to which error messages
!        are being sent.  to find out if more than one file is being
!        used, one must use the xgetua routine.
!
!     description of parameter
!      --output--
!        iunit - the logical unit number of the  (first) unit to
!                which error messages are being sent.
!                a value of zero means that the default file, as
!                defined by the i1mach routine, is being used.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision --- 23 may 1979
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xgetun
!***first executable statement  xgetun
      implicit none
      integer :: iunit
      integer :: j4save
      iunit = j4save(3,0,.false.)
      return
      end subroutine xgetun
      subroutine xsetf(kontrl)
!***begin prologue  xsetf
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3a
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set the error control flag.
!***description
!
!     abstract
!        xsetf sets the error control flag value to kontrl.
!        (kontrl is an input parameter only.)
!        the following table shows how each message is treated,
!        depending on the values of kontrl and level.  (see xerror
!        for description of level.)
!
!        if kontrl is zero or negative, no information other than the
!        message itself (including numeric values, if any) will be
!        printed.  if kontrl is positive, introductory messages,
!        trace-backs, etc., will be printed in addition to the message.
!
!              iabs(kontrl)
!        level        0              1              2
!        value
!          2        fatal          fatal          fatal
!
!          1     not printed      printed         fatal
!
!          0     not printed      printed        printed
!
!         -1     not printed      printed        printed
!                                  only           only
!                                  once           once
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  19 mar 1980
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save,xerrwv
!***end prologue  xsetf
!***first executable statement  xsetf
      USE double
      implicit none
      integer :: junk, kontrl
      integer :: j4save
      if (.not.((kontrl.ge.(-2)).and.(kontrl.le.2))) then 
         call xerrwv('xsetf  -- invalid value of kontrl (i1).',33,1,2,1,kontrl,0,0,0.0_DP,0.0_DP)
         return
      end if
      junk = j4save(2,kontrl,.true.)
      return
      end subroutine xsetf
      subroutine xsetua(iunita,n)
!***begin prologue  xsetua
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3b
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set logical unit numbers (up to 5) to which error
!            messages are to be sent.
!***description
!
!     abstract
!        xsetua may be called to declare a list of up to five
!        logical units, each of which is to receive a copy of
!        each error message processed by this package.
!        the purpose of xsetua is to allow simultaneous printing
!        of each error message on, say, a main output file,
!        an interactive terminal, and other files such as graphics
!        communication files.
!
!     description of parameters
!      --input--
!        iunit - an array of up to five unit numbers.
!                normally these numbers should all be different
!                (but duplicates are not prohibited.)
!        n     - the number of unit numbers provided in iunit
!                must have 1 .le. n .le. 5.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  19 mar 1980
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save,xerrwv
!***end prologue  xsetua
      USE double
      implicit none
      integer :: i, index, junk, n
      integer :: j4save
      integer, dimension(5) :: iunita
!***first executable statement  xsetua
      if (.not.((n.ge.1).and.(n.le.5))) then
         call xerrwv('xsetua -- invalid value of n (i1).',34,1,2,1,n,0,0,0.0_DP,0.0_DP)
         return
      end if
      do i=1,n
         index = i+4
         if (i.eq.1) index = 3
         junk = j4save(index,iunita(i),.true.)
      end do
      junk = j4save(5,n,.true.)
      return
      end subroutine xsetua
      subroutine xsetun(iunit)
!***begin prologue  xsetun
!***date written   790801   (yymmdd)
!***revision date  851111   (yymmdd)
!***category no.  r3b
!***keywords  error,xerror package
!***author  jones, r. e., (snla)
!***purpose  set output file to which error messages are to be sent.
!***description
!
!     abstract
!        xsetun sets the output file to which error messages are to
!        be sent.  only one file will be used.  see xsetua for
!        how to declare more than one file.
!
!     description of parameter
!      --input--
!        iunit - an input parameter giving the logical unit number
!                to which error messages are to be sent.
!
!     written by ron jones, with slatec common math library subcommittee
!     latest revision ---  7 june 1978
!***references  jones r.e., kahaner d.k., 'xerror, the slatec error-
!                 handling package', sand82-0800, sandia laboratories,
!                 1982.
!***routines called  j4save
!***end prologue  xsetun
!***first executable statement  xsetun
      implicit none
      integer :: iunit, junk
      integer :: j4save
      junk = j4save(3,iunit,.true.)
      junk = j4save(5,1,.true.)
      return
      end subroutine xsetun
      FUNCTION RAND(R)
!***BEGIN PROLOGUE  RAND
!***DATE WRITTEN   770401   (YYMMDD)
!***REVISION DATE  861211   (YYMMDD)
!***CATEGORY NO.  L6A21
!***KEYWORDS  LIBRARY=SLATEC(FNLIB),TYPE=SINGLE PRECISION(RAND-S),
!             RANDOM NUMBER,SPECIAL FUNCTIONS,UNIFORM
!***AUTHOR  FULLERTON, W., (LANL)
!***PURPOSE  Generates a uniformly distributed random number.
!***DESCRIPTION
!
!      This pseudo-random number generator is portable among a wide
! variety of computers.  RAND(R) undoubtedly is not as good as many
! readily available installation dependent versions, and so this
! routine is not recommended for widespread usage.  Its redeeming
! feature is that the exact same random numbers (to within final round-
! off error) can be generated from machine to machine.  Thus, programs
! that make use of random numbers can be easily transported to and
! checked in a new environment.
!      The random numbers are generated by the linear congruential
! method described, e.g., by Knuth in Seminumerical Methods (p.9),
! Addison-Wesley, 1969.  Given the I-th number of a pseudo-random
! sequence, the I+1 -st number is generated from
!             X(I+1) = (A*X(I) + C) MOD M,
! where here M = 2**22 = 4194304, C = 1731 and several suitable values
! of the multiplier A are discussed below.  Both the multiplier A and
! random number X are represented in double precision as two 11-bit
! words.  The constants are chosen so that the period is the maximum
! possible, 4194304.
!      In order that the same numbers be generated from machine to
! machine, it is necessary that 23-bit integers be reducible modulo
! 2**11 exactly, that 23-bit integers be added exactly, and that 11-bit
! integers be multiplied exactly.  Furthermore, if the restart option
! is used (where R is between 0 and 1), then the product R*2**22 =
! R*4194304 must be correct to the nearest integer.
!      The first four random numbers should be .0004127026,
! .6750836372, .1614754200, and .9086198807.  The tenth random number
! is .5527787209, and the hundredth is .3600893021 .  The thousandth
! number should be .2176990509 .
!      In order to generate several effectively independent sequences
! with the same generator, it is necessary to know the random number
! for several widely spaced calls.  The I-th random number times 2**22,
! where I=K*P/8 and P is the period of the sequence (P = 2**22), is
! still of the form L*P/8.  In particular we find the I-th random
! number multiplied by 2**22 is given by
! I   =  0  1*P/8  2*P/8  3*P/8  4*P/8  5*P/8  6*P/8  7*P/8  8*P/8
! RAND=  0  5*P/8  2*P/8  7*P/8  4*P/8  1*P/8  6*P/8  3*P/8  0
! Thus the 4*P/8 = 2097152 random number is 2097152/2**22.
!      Several multipliers have been subjected to the spectral test
! (see Knuth, p. 82).  Four suitable multipliers roughly in order of
! goodness according to the spectral test are
!    3146757 = 1536*2048 + 1029 = 2**21 + 2**20 + 2**10 + 5
!    2098181 = 1024*2048 + 1029 = 2**21 + 2**10 + 5
!    3146245 = 1536*2048 +  517 = 2**21 + 2**20 + 2**9 + 5
!    2776669 = 1355*2048 + 1629 = 5**9 + 7**7 + 1
!
!      In the table below LOG10(NU(I)) gives roughly the number of
! random decimal digits in the random numbers considered I at a time.
! C is the primary measure of goodness.  In both cases bigger is better.
!
!                   LOG10 NU(I)              C(I)
!       A       I=2  I=3  I=4  I=5    I=2  I=3  I=4  I=5
!
!    3146757    3.3  2.0  1.6  1.3    3.1  1.3  4.6  2.6
!    2098181    3.3  2.0  1.6  1.2    3.2  1.3  4.6  1.7
!    3146245    3.3  2.2  1.5  1.1    3.2  4.2  1.1  0.4
!    2776669    3.3  2.1  1.6  1.3    2.5  2.0  1.9  2.6
!   Best
!    Possible   3.3  2.3  1.7  1.4    3.6  5.9  9.7  14.9
!
!             Input Argument --
! R      If R=0., the next random number of the sequence is generated.
!        If R .LT. 0., the last generated number will be returned for
!          possible use in a restart procedure.
!        If R .GT. 0., the sequence of random numbers will start with
!          the seed R mod 1.  This seed is also returned as the value of
!          RAND provided the arithmetic is done exactly.
!
!             Output Value --
! RAND   a pseudo-random number between 0. and 1.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  RAND
      USE double
      IMPLICIT NONE
      REAL(KIND=DP) :: R, RAND
      INTEGER :: IY0, IY1
      INTEGER, SAVE :: IA1 = 1536, IA0 = 1029, IA1MA0 = 507, &
          IC = 1731, IX1 = 0, IX0 = 0
!***FIRST EXECUTABLE STATEMENT  RAND
      IF (R.LT.0.) GO TO 10
      IF (R.GT.0.) GO TO 20
!
!           A*X = 2**22*IA1*IX1 + 2**11*(IA1*IX1 + (IA1-IA0)*(IX0-IX1)
!                   + IA0*IX0) + IA0*IX0
!
      IY0 = IA0*IX0
      IY1 = IA1*IX1 + IA1MA0*(IX0-IX1) + IY0
      IY0 = IY0 + IC
      IX0 = MOD (IY0, 2048)
      IY1 = IY1 + (IY0-IX0)/2048
      IX1 = MOD (IY1, 2048)
!
 10   RAND = IX1*2048 + IX0
      RAND = RAND / 4194304._DP
      RETURN
!
 20   IX1 = INT(DMOD(R,1._DP)*4194304._DP + 0.5_DP)
      IX0 = MOD (IX1, 2048)
      IX1 = (IX1-IX0)/2048
      GO TO 10
!
      END FUNCTION RAND
      FUNCTION D1MACH(I)
!
!  DOUBLE-PRECISION MACHINE CONSTANTS
!
!  D1MACH( 1) = B**(EMIN-1), THE SMALLEST POSITIVE MAGNITUDE.
!
!  D1MACH( 2) = B**EMAX*(1 - B**(-T)), THE LARGEST MAGNITUDE.
!
!  D1MACH( 3) = B**(-T), THE SMALLEST RELATIVE SPACING.
!
!  D1MACH( 4) = B**(1-T), THE LARGEST RELATIVE SPACING.
!
!  D1MACH( 5) = LOG10(B)
!
!  TO ALTER THIS FUNCTION FOR A PARTICULAR ENVIRONMENT,
!  THE DESIRED SET OF DATA STATEMENTS SHOULD BE ACTIVATED BY
!  REMOVING THE ! FROM COLUMN 1.
!  ON RARE MACHINES A STATIC STATEMENT MAY NEED TO BE ADDED.
!  (BUT PROBABLY MORE SYSTEMS PROHIBIT IT THAN REQUIRE IT.)
!
!  WHERE POSSIBLE, OCTAL OR HEXADECIMAL CONSTANTS HAVE BEEN USED
!  TO SPECIFY THE CONSTANTS EXACTLY WHICH HAS IN SOME CASES
!  REQUIRED THE USE OF EQUIVALENT INTEGER ARRAYS.
!
      USE double
      IMPLICIT NONE
      INTEGER :: I
      INTEGER :: I1MACH
      REAL(KIND=DP) :: D1MACH
      INTEGER SMALL(4)
      INTEGER LARGE(4)
      INTEGER RIGHT(4)
      INTEGER DIVER(4)
      INTEGER LOG10(4)
!
      REAL(KIND=DP) DMACH(5)
!
      EQUIVALENCE (DMACH(1),SMALL(1))
      EQUIVALENCE (DMACH(2),LARGE(1))
      EQUIVALENCE (DMACH(3),RIGHT(1))
      EQUIVALENCE (DMACH(4),DIVER(1))
      EQUIVALENCE (DMACH(5),LOG10(1))
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM.
!
!      DATA SMALL(1) / ZC00800000 /
!      DATA SMALL(2) / Z000000000 /
!
!      DATA LARGE(1) / ZDFFFFFFFF /
!      DATA LARGE(2) / ZFFFFFFFFF /
!
!      DATA RIGHT(1) / ZCC5800000 /
!      DATA RIGHT(2) / Z000000000 /
!
!      DATA DIVER(1) / ZCC6800000 /
!      DATA DIVER(2) / Z000000000 /
!
!      DATA LOG10(1) / ZD00E730E7 /
!      DATA LOG10(2) / ZC77800DC0 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM.
!
!      DATA SMALL(1) / O1771000000000000 /
!      DATA SMALL(2) / O0000000000000000 /
!
!      DATA LARGE(1) / O0777777777777777 /
!      DATA LARGE(2) / O0007777777777777 /
!
!      DATA RIGHT(1) / O1461000000000000 /
!      DATA RIGHT(2) / O0000000000000000 /
!
!      DATA DIVER(1) / O1451000000000000 /
!      DATA DIVER(2) / O0000000000000000 /
!
!      DATA LOG10(1) / O1157163034761674 /
!      DATA LOG10(2) / O0006677466732724 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS.
!
!      DATA SMALL(1) / O1771000000000000 /
!      DATA SMALL(2) / O7770000000000000 /
!
!      DATA LARGE(1) / O0777777777777777 /
!      DATA LARGE(2) / O7777777777777777 /
!
!      DATA RIGHT(1) / O1461000000000000 /
!      DATA RIGHT(2) / O0000000000000000 /
!
!      DATA DIVER(1) / O1451000000000000 /
!      DATA DIVER(2) / O0000000000000000 /
!
!      DATA LOG10(1) / O1157163034761674 /
!      DATA LOG10(2) / O0006677466732724 /
!
!     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES.
!
!      DATA SMALL(1) / 00604000000000000000B /
!      DATA SMALL(2) / 00000000000000000000B /
!
!      DATA LARGE(1) / 37767777777777777777B /
!      DATA LARGE(2) / 37167777777777777777B /
!
!      DATA RIGHT(1) / 15604000000000000000B /
!      DATA RIGHT(2) / 15000000000000000000B /
!
!      DATA DIVER(1) / 15614000000000000000B /
!      DATA DIVER(2) / 15010000000000000000B /
!
!      DATA LOG10(1) / 17164642023241175717B /
!      DATA LOG10(2) / 16367571421742254654B /
!
!     MACHINE CONSTANTS FOR CONVEX C-1
!
!      DATA SMALL(1),SMALL(2) / '00100000'X, '00000000'X /
!      DATA LARGE(1),LARGE(2) / '7FFFFFFF'X, 'FFFFFFFF'X /
!      DATA RIGHT(1),RIGHT(2) / '3CC00000'X, '00000000'X /
!      DATA DIVER(1),DIVER(2) / '3CD00000'X, '00000000'X /
!      DATA LOG10(1),LOG10(2) / '3FF34413'X, '509F79FF'X /
!
!     MACHINE CONSTANTS FOR THE CRAY 1
!
!      DATA SMALL(1) / 201354000000000000000B /
!      DATA SMALL(2) / 000000000000000000000B /
!
!      DATA LARGE(1) / 577767777777777777777B /
!      DATA LARGE(2) / 000007777777777777776B /
!
!      DATA RIGHT(1) / 376434000000000000000B /
!      DATA RIGHT(2) / 000000000000000000000B /
!
!      DATA DIVER(1) / 376444000000000000000B /
!      DATA DIVER(2) / 000000000000000000000B /
!
!      DATA LOG10(1) / 377774642023241175717B /
!      DATA LOG10(2) / 000007571421742254654B /
!
!     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!
!     NOTE - IT MAY BE APPROPRIATE TO INCLUDE THE FOLLOWING CARD -
!     STATIC DMACH(5)
!
!      DATA SMALL/20K,3*0/,LARGE/77777K,3*177777K/
!      DATA RIGHT/31420K,3*0/,DIVER/32020K,3*0/
!      DATA LOG10/40423K,42023K,50237K,74776K/
!
!     MACHINE CONSTANTS FOR THE HARRIS SLASH 6 AND SLASH 7
!
!      DATA SMALL(1),SMALL(2) / '20000000, '00000201 /
!      DATA LARGE(1),LARGE(2) / '37777777, '37777577 /
!      DATA RIGHT(1),RIGHT(2) / '20000000, '00000333 /
!      DATA DIVER(1),DIVER(2) / '20000000, '00000334 /
!      DATA LOG10(1),LOG10(2) / '23210115, '10237777 /
!
!     MACHINE CONSTANTS FOR THE HONEYWELL DPS 8/70 SERIES.
!
!      DATA SMALL(1),SMALL(2) / O402400000000, O000000000000 /
!      DATA LARGE(1),LARGE(2) / O376777777777, O777777777777 /
!      DATA RIGHT(1),RIGHT(2) / O604400000000, O000000000000 /
!      DATA DIVER(1),DIVER(2) / O606400000000, O000000000000 /
!      DATA LOG10(1),LOG10(2) / O776464202324, O117571775714 /
!
!     MACHINE CONSTANTS FOR AT&T 3B SERIES MACHINES.
!
!      DATA SMALL(1),SMALL(2) /    1048576,          0 /
!      DATA LARGE(1),LARGE(2) / 2146435071,         -1 /
!      DATA RIGHT(1),RIGHT(2) / 1017118720,          0 /
!      DATA DIVER(1),DIVER(2) / 1018167296,          0 /
!      DATA LOG10(1),LOG10(2) / 1070810131, 1352628735 /
!
!     MACHINE CONSTANTS FOR THE IBM PC AND OTHER 8087-ARITHMETIC MICROS
!
!      DATA SMALL(1),SMALL(2) /          0,    1048576 /
!      DATA LARGE(1),LARGE(2) /         -1, 2146435071 /
!      DATA RIGHT(1),RIGHT(2) /          0, 1017118720 /
!      DATA DIVER(1),DIVER(2) /          0, 1018167296 /
!      DATA LOG10(1),LOG10(2) / 1352628735, 1070810131 /
!
!     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!     THE XEROX SIGMA 5/7/9 AND THE SEL SYSTEMS 85/86.
!
!      DATA SMALL(1),SMALL(2) / Z00100000, Z00000000 /
!      DATA LARGE(1),LARGE(2) / Z7FFFFFFF, ZFFFFFFFF /
!      DATA RIGHT(1),RIGHT(2) / Z33100000, Z00000000 /
!      DATA DIVER(1),DIVER(2) / Z34100000, Z00000000 /
!      DATA LOG10(1),LOG10(2) / Z41134413, Z509F79FF /
!
!     MACHINE CONSTANTS FOR THE INTERDATA 8/32
!     WITH THE UNIX SYSTEM FORTRAN 77 COMPILER.
!
!     FOR THE INTERDATA FORTRAN VII COMPILER REPLACE
!     THE Z'S SPECIFYING HEX CONSTANTS WITH Y'S.
!
!      DATA SMALL(1),SMALL(2) / Z'00100000', Z'00000000' /
!      DATA LARGE(1),LARGE(2) / Z'7EFFFFFF', Z'FFFFFFFF' /
!      DATA RIGHT(1),RIGHT(2) / Z'33100000', Z'00000000' /
!      DATA DIVER(1),DIVER(2) / Z'34100000', Z'00000000' /
!      DATA LOG10(1),LOG10(2) / Z'41134413', Z'509F79FF' /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR).
!
!      DATA SMALL(1),SMALL(2) / "033400000000, "000000000000 /
!      DATA LARGE(1),LARGE(2) / "377777777777, "344777777777 /
!      DATA RIGHT(1),RIGHT(2) / "113400000000, "000000000000 /
!      DATA DIVER(1),DIVER(2) / "114400000000, "000000000000 /
!      DATA LOG10(1),LOG10(2) / "177464202324, "144117571776 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR).
!
!      DATA SMALL(1),SMALL(2) / "000400000000, "000000000000 /
!      DATA LARGE(1),LARGE(2) / "377777777777, "377777777777 /
!      DATA RIGHT(1),RIGHT(2) / "103400000000, "000000000000 /
!      DATA DIVER(1),DIVER(2) / "104400000000, "000000000000 /
!      DATA LOG10(1),LOG10(2) / "177464202324, "047674776746 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     32-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!
!      DATA SMALL(1),SMALL(2) /    8388608,           0 /
!      DATA LARGE(1),LARGE(2) / 2147483647,          -1 /
!      DATA RIGHT(1),RIGHT(2) /  612368384,           0 /
!      DATA DIVER(1),DIVER(2) /  620756992,           0 /
!      DATA LOG10(1),LOG10(2) / 1067065498, -2063872008 /
!
!      DATA SMALL(1),SMALL(2) / O00040000000, O00000000000 /
!      DATA LARGE(1),LARGE(2) / O17777777777, O37777777777 /
!      DATA RIGHT(1),RIGHT(2) / O04440000000, O00000000000 /
!      DATA DIVER(1),DIVER(2) / O04500000000, O00000000000 /
!      DATA LOG10(1),LOG10(2) / O07746420232, O20476747770 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     16-BIT INTEGERS (EXPRESSED IN INTEGER AND OCTAL).
!
!      DATA SMALL(1),SMALL(2) /    128,      0 /
!      DATA SMALL(3),SMALL(4) /      0,      0 /
!
!      DATA LARGE(1),LARGE(2) /  32767,     -1 /
!      DATA LARGE(3),LARGE(4) /     -1,     -1 /
!
!      DATA RIGHT(1),RIGHT(2) /   9344,      0 /
!      DATA RIGHT(3),RIGHT(4) /      0,      0 /
!
!      DATA DIVER(1),DIVER(2) /   9472,      0 /
!      DATA DIVER(3),DIVER(4) /      0,      0 /
!
!      DATA LOG10(1),LOG10(2) /  16282,   8346 /
!      DATA LOG10(3),LOG10(4) / -31493, -12296 /
!
!      DATA SMALL(1),SMALL(2) / O000200, O000000 /
!      DATA SMALL(3),SMALL(4) / O000000, O000000 /
!
!      DATA LARGE(1),LARGE(2) / O077777, O177777 /
!      DATA LARGE(3),LARGE(4) / O177777, O177777 /
!
!      DATA RIGHT(1),RIGHT(2) / O022200, O000000 /
!      DATA RIGHT(3),RIGHT(4) / O000000, O000000 /
!
!      DATA DIVER(1),DIVER(2) / O022400, O000000 /
!      DATA DIVER(3),DIVER(4) / O000000, O000000 /
!
!      DATA LOG10(1),LOG10(2) / O037632, O020232 /
!      DATA LOG10(3),LOG10(4) / O102373, O147770 /
!
!     MACHINE CONSTANTS FOR THE SUN MICROSYSTEMS UNIX F77 COMPILER.
!
      DATA DMACH(1) / 2.22507385850720D-300 /
      DATA DMACH(2) / 1.79769313486231D+300 /
      DATA DMACH(3) / 1.1101827117665D-16 /
      DATA DMACH(4) / 2.2203654423533D-16 /
      DATA DMACH(5) / 3.01029995663981E-1 /
!
!     MACHINE CONSTANTS FOR THE ALLIANT FX/8 UNIX FORTRAN COMPILER.
!
!$$$      DATA DMACH(1) / 2.22507385850721D-308 /
!$$$      DATA DMACH(2) / 1.79769313486231D+308 /
!$$$      DATA DMACH(3) / 1.1101827117665D-16 /
!$$$      DATA DMACH(4) / 2.2203654423533D-16 /
!$$$      DATA DMACH(5) / 3.01029995663981E-1 /
!
!     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES.
!
!      DATA SMALL(1),SMALL(2) / O000040000000, O000000000000 /
!      DATA LARGE(1),LARGE(2) / O377777777777, O777777777777 /
!      DATA RIGHT(1),RIGHT(2) / O170540000000, O000000000000 /
!      DATA DIVER(1),DIVER(2) / O170640000000, O000000000000 /
!      DATA LOG10(1),LOG10(2) / O177746420232, O411757177572 /
!
!     MACHINE CONSTANTS FOR THE VAX UNIX F77 COMPILER
!
!      DATA SMALL(1),SMALL(2) /        128,           0 /
!      DATA LARGE(1),LARGE(2) /     -32769,          -1 /
!      DATA RIGHT(1),RIGHT(2) /       9344,           0 /
!      DATA DIVER(1),DIVER(2) /       9472,           0 /
!      DATA LOG10(1),LOG10(2) /  546979738,  -805796613 /
!
!     MACHINE CONSTANTS FOR THE VAX-11 WITH
!     FORTRAN IV-PLUS COMPILER
!
!      DATA SMALL(1),SMALL(2) / Z00000080, Z00000000 /
!      DATA LARGE(1),LARGE(2) / ZFFFF7FFF, ZFFFFFFFF /
!      DATA RIGHT(1),RIGHT(2) / Z00002480, Z00000000 /
!      DATA DIVER(1),DIVER(2) / Z00002500, Z00000000 /
!      DATA LOG10(1),LOG10(2) / Z209A3F9A, ZCFF884FB /
!
!     MACHINE CONSTANTS FOR VAX/VMS VERSION 2.2
!
!      DATA SMALL(1),SMALL(2) /       '80'X,        '0'X /
!      DATA LARGE(1),LARGE(2) / 'FFFF7FFF'X, 'FFFFFFFF'X /
!      DATA RIGHT(1),RIGHT(2) /     '2480'X,        '0'X /
!      DATA DIVER(1),DIVER(2) /     '2500'X,        '0'X /
!      DATA LOG10(1),LOG10(2) / '209A3F9A'X, 'CFF884FB'X /
!
!     MACHINE CONSTANTS FOR THE SEQUENT BALANCE 8000
!
!      DATA SMALL(1),SMALL(2) / $00000000,  $00100000 /
!      DATA LARGE(1),LARGE(2) / $FFFFFFFF,  $7FEFFFFF /
!      DATA RIGHT(1),RIGHT(2) / $00000000,  $3CA00000 /
!      DATA DIVER(1),DIVER(2) / $00000000,  $3CB00000 /
!      DATA LOG10(1),LOG10(2) / $509F79FF,  $3FD34413 /
!
!     MACHINE CONSTANTS FOR SVS FORTRAN ON THE AT&T 7300 (UNIX PC)
!
!      DATA SMALL(1),SMALL(2) / $00100000, $00000000 /
!      DATA LARGE(1),LARGE(2) / $7FEFFFFF, $FFFFFFFF /
!      DATA RIGHT(1),RIGHT(2) / $3CA00000, $00000000 /
!      DATA DIVER(1),DIVER(2) / $3CB00000, $00000000 /
!      DATA LOG10(1),LOG10(2) / $3FD34413, $509F79FF /
!
!     MACHINE CONSTANTS FOR THE RM FORTRAN ON THE AT&T 7300 (UNIX PC)
!
!      DATA SMALL(1),SMALL(2) / Z'00100000', Z'00000000' /
!      DATA LARGE(1),LARGE(2) / Z'7FEFFFFF', Z'FFFFFFFF' /
!      DATA RIGHT(1),RIGHT(2) / Z'3CA00000', Z'00000000' /
!      DATA DIVER(1),DIVER(2) / Z'3CB00000', Z'00000000' /
!      DATA LOG10(1),LOG10(2) / Z'3FD34413', Z'509F79FF' /
!
      IF (I .LT. 1  .OR.  I .GT. 5) GOTO 999
      D1MACH = DMACH(I)
      RETURN
  999 WRITE(I1MACH(2),1999) I
 1999 FORMAT(' D1MACH - I OUT OF BOUNDS',I10)
      STOP
      END FUNCTION D1MACH
      FUNCTION I1MACH(I)
!
!  I/O UNIT NUMBERS.
!
!    I1MACH( 1) = THE STANDARD INPUT UNIT.
!
!    I1MACH( 2) = THE STANDARD OUTPUT UNIT.
!
!    I1MACH( 3) = THE STANDARD PUNCH UNIT.
!
!    I1MACH( 4) = THE STANDARD ERROR MESSAGE UNIT.
!
!  WORDS.
!
!    I1MACH( 5) = THE NUMBER OF BITS PER INTEGER STORAGE UNIT.
!
!    I1MACH( 6) = THE NUMBER OF CHARACTERS PER INTEGER STORAGE UNIT.
!
!  INTEGERS.
!
!    ASSUME INTEGERS ARE REPRESENTED IN THE S-DIGIT, BASE-A FORM
!
!               SIGN ( X(S-1)*A**(S-1) + ... + X(1)*A + X(0) )
!
!               WHERE 0 .LE. X(I) .LT. A FOR I=0,...,S-1.
!
!    I1MACH( 7) = A, THE BASE.
!
!    I1MACH( 8) = S, THE NUMBER OF BASE-A DIGITS.
!
!    I1MACH( 9) = A**S - 1, THE LARGEST MAGNITUDE.
!
!  FLOATING-POINT NUMBERS.
!
!    ASSUME FLOATING-POINT NUMBERS ARE REPRESENTED IN THE T-DIGIT,
!    BASE-B FORM
!
!               SIGN (B**E)*( (X(1)/B) + ... + (X(T)/B**T) )
!
!               WHERE 0 .LE. X(I) .LT. B FOR I=1,...,T,
!               0 .LT. X(1), AND EMIN .LE. E .LE. EMAX.
!
!    I1MACH(10) = B, THE BASE.
!
!  SINGLE-PRECISION
!
!    I1MACH(11) = T, THE NUMBER OF BASE-B DIGITS.
!
!    I1MACH(12) = EMIN, THE SMALLEST EXPONENT E.
!
!    I1MACH(13) = EMAX, THE LARGEST EXPONENT E.
!
!  DOUBLE-PRECISION
!
!    I1MACH(14) = T, THE NUMBER OF BASE-B DIGITS.
!
!    I1MACH(15) = EMIN, THE SMALLEST EXPONENT E.
!
!    I1MACH(16) = EMAX, THE LARGEST EXPONENT E.
!
!  TO ALTER THIS FUNCTION FOR A PARTICULAR ENVIRONMENT,
!  THE DESIRED SET OF DATA STATEMENTS SHOULD BE ACTIVATED BY
!  REMOVING THE ! FROM COLUMN 1.  ALSO, THE VALUES OF
!  I1MACH(1) - I1MACH(4) SHOULD BE CHECKED FOR CONSISTENCY
!  WITH THE LOCAL OPERATING SYSTEM.
!  ON RARE MACHINES A STATIC STATEMENT MAY NEED TO BE ADDED.
!  (BUT PROBABLY MORE SYSTEMS PROHIBIT IT THAN REQUIRE IT.)
!
      INTEGER :: OUTPUT, I, I1MACH
      INTEGER, DIMENSION(16) :: IMACH
!
      EQUIVALENCE (IMACH(4),OUTPUT)
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 1700 SYSTEM.
!
!      DATA IMACH( 1) /    7 /
!      DATA IMACH( 2) /    2 /
!      DATA IMACH( 3) /    2 /
!      DATA IMACH( 4) /    2 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   33 /
!      DATA IMACH( 9) / Z1FFFFFFFF /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -256 /
!      DATA IMACH(13) /  255 /
!      DATA IMACH(14) /   60 /
!      DATA IMACH(15) / -256 /
!      DATA IMACH(16) /  255 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 5700 SYSTEM.
!
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  48 /
!      DATA IMACH( 6) /   6 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  39 /
!      DATA IMACH( 9) / O0007777777777777 /
!      DATA IMACH(10) /   8 /
!      DATA IMACH(11) /  13 /
!      DATA IMACH(12) / -50 /
!      DATA IMACH(13) /  76 /
!      DATA IMACH(14) /  26 /
!      DATA IMACH(15) / -50 /
!      DATA IMACH(16) /  76 /
!
!     MACHINE CONSTANTS FOR THE BURROUGHS 6700/7700 SYSTEMS.
!
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  48 /
!      DATA IMACH( 6) /   6 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  39 /
!      DATA IMACH( 9) / O0007777777777777 /
!      DATA IMACH(10) /   8 /
!      DATA IMACH(11) /  13 /
!      DATA IMACH(12) / -50 /
!      DATA IMACH(13) /  76 /
!      DATA IMACH(14) /  26 /
!      DATA IMACH(15) / -32754 /
!      DATA IMACH(16) /  32780 /
!
!     MACHINE CONSTANTS FOR THE CDC 6000/7000 SERIES.
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   60 /
!      DATA IMACH( 6) /   10 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   48 /
!      DATA IMACH( 9) / 00007777777777777777B /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   48 /
!      DATA IMACH(12) / -974 /
!      DATA IMACH(13) / 1070 /
!      DATA IMACH(14) /   96 /
!      DATA IMACH(15) / -927 /
!      DATA IMACH(16) / 1070 /
!
!     MACHINE CONSTANTS FOR CONVEX !-1
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   53 /
!      DATA IMACH(15) /-1024 /
!      DATA IMACH(16) / 1023 /
!
!     MACHINE CONSTANTS FOR THE CRAY 1
!
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /   102 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    64 /
!      DATA IMACH( 6) /     8 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    46 /
!      DATA IMACH( 9) /  1777777777777777B /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    47 /
!      DATA IMACH(12) / -8189 /
!      DATA IMACH(13) /  8190 /
!      DATA IMACH(14) /    94 /
!      DATA IMACH(15) / -8099 /
!      DATA IMACH(16) /  8190 /
!
!     MACHINE CONSTANTS FOR THE DATA GENERAL ECLIPSE S/200
!
!      DATA IMACH( 1) /   11 /
!      DATA IMACH( 2) /   12 /
!      DATA IMACH( 3) /    8 /
!      DATA IMACH( 4) /   10 /
!      DATA IMACH( 5) /   16 /
!      DATA IMACH( 6) /    2 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   15 /
!      DATA IMACH( 9) /32767 /
!      DATA IMACH(10) /   16 /
!      DATA IMACH(11) /    6 /
!      DATA IMACH(12) /  -64 /
!      DATA IMACH(13) /   63 /
!      DATA IMACH(14) /   14 /
!      DATA IMACH(15) /  -64 /
!      DATA IMACH(16) /   63 /
!
!     MACHINE CONSTANTS FOR THE HARRIS SLASH 6 AND SLASH 7
!
!      DATA IMACH( 1) /       5 /
!      DATA IMACH( 2) /       6 /
!      DATA IMACH( 3) /       0 /
!      DATA IMACH( 4) /       6 /
!      DATA IMACH( 5) /      24 /
!      DATA IMACH( 6) /       3 /
!      DATA IMACH( 7) /       2 /
!      DATA IMACH( 8) /      23 /
!      DATA IMACH( 9) / 8388607 /
!      DATA IMACH(10) /       2 /
!      DATA IMACH(11) /      23 /
!      DATA IMACH(12) /    -127 /
!      DATA IMACH(13) /     127 /
!      DATA IMACH(14) /      38 /
!      DATA IMACH(15) /    -127 /
!      DATA IMACH(16) /     127 /
!
!     MACHINE CONSTANTS FOR THE HONEYWELL DPS 8/70 SERIES.
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /   43 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / O377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   63 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR IEEE ARITHMETIC MACHINES (E.G., AT&T 3B
!     SERIES COMPUTERS AND 8087-BASED MACHINES LIKE THE IBM PC).
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -125 /
!      DATA IMACH(13) /  128 /
!      DATA IMACH(14) /   53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE IBM 360/370 SERIES,
!     THE XEROX SIGMA 5/7/9 AND THE SEL SYSTEMS 85/86.
!
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   7 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  32 /
!      DATA IMACH( 6) /   4 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  31 /
!      DATA IMACH( 9) / Z7FFFFFFF /
!      DATA IMACH(10) /  16 /
!      DATA IMACH(11) /   6 /
!      DATA IMACH(12) / -64 /
!      DATA IMACH(13) /  63 /
!      DATA IMACH(14) /  14 /
!      DATA IMACH(15) / -64 /
!      DATA IMACH(16) /  63 /
!
!     MACHINE CONSTANTS FOR THE INTERDATA 8/32
!     WITH THE UNIX SYSTEM FORTRAN 77 COMPILER.
!
!     FOR THE INTERDATA FORTRAN VII COMPILER REPLACE
!     THE Z'S SPECIFYING HEX CONSTANTS WITH Y'S.
!
!      DATA IMACH( 1) /   5 /
!      DATA IMACH( 2) /   6 /
!      DATA IMACH( 3) /   6 /
!      DATA IMACH( 4) /   6 /
!      DATA IMACH( 5) /  32 /
!      DATA IMACH( 6) /   4 /
!      DATA IMACH( 7) /   2 /
!      DATA IMACH( 8) /  31 /
!      DATA IMACH( 9) / Z'7FFFFFFF' /
!      DATA IMACH(10) /  16 /
!      DATA IMACH(11) /   6 /
!      DATA IMACH(12) / -64 /
!      DATA IMACH(13) /  62 /
!      DATA IMACH(14) /  14 /
!      DATA IMACH(15) / -64 /
!      DATA IMACH(16) /  62 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KA PROCESSOR).
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    5 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / "377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   54 /
!      DATA IMACH(15) / -101 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE PDP-10 (KI PROCESSOR).
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    5 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / "377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   62 /
!      DATA IMACH(15) / -128 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     32-BIT INTEGER ARITHMETIC.
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   56 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
!     16-BIT INTEGER ARITHMETIC.
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   16 /
!      DATA IMACH( 6) /    2 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   15 /
!      DATA IMACH( 9) / 32767 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   56 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE SUN MICROSYSTEMS UNIX F77 COMPILER.
!
      DATA IMACH( 1) /     5 /
      DATA IMACH( 2) /     6 /
      DATA IMACH( 3) /     6 /
      DATA IMACH( 4) /     0 /
      DATA IMACH( 5) /    32 /
      DATA IMACH( 6) /     4 /
      DATA IMACH( 7) /     2 /
      DATA IMACH( 8) /    32 /
      DATA IMACH( 9) /2147483647/
      DATA IMACH(10) /     2 /
      DATA IMACH(11) /    24 /
      DATA IMACH(12) /  -126 /
      DATA IMACH(13) /   128 /
      DATA IMACH(14) /    53 /
      DATA IMACH(15) / -1022 /
      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE ALLIANT FX/8 UNIX FORTRAN COMPILER.
!
!$$$      DATA IMACH( 1) /     5 /
!$$$      DATA IMACH( 2) /     6 /
!$$$      DATA IMACH( 3) /     6 /
!$$$      DATA IMACH( 4) /     0 /
!$$$      DATA IMACH( 5) /    32 /
!$$$      DATA IMACH( 6) /     4 /
!$$$      DATA IMACH( 7) /     2 /
!$$$      DATA IMACH( 8) /    32 /
!$$$      DATA IMACH( 9) /2147483647/
!$$$      DATA IMACH(10) /     2 /
!$$$      DATA IMACH(11) /    24 /
!$$$      DATA IMACH(12) /  -126 /
!$$$      DATA IMACH(13) /   128 /
!$$$      DATA IMACH(14) /    53 /
!$$$      DATA IMACH(15) / -1022 /
!$$$      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE ALLIANT FX/8 UNIX FORTRAN COMPILER.
!     WITH THE -r8 COMMAND LINE OPTION.
!
!$$$      DATA IMACH( 1) /     5 /
!$$$      DATA IMACH( 2) /     6 /
!$$$      DATA IMACH( 3) /     6 /
!$$$      DATA IMACH( 4) /     0 /
!$$$      DATA IMACH( 5) /    32 /
!$$$      DATA IMACH( 6) /     4 /
!$$$      DATA IMACH( 7) /     2 /
!$$$      DATA IMACH( 8) /    32 /
!$$$      DATA IMACH( 9) /2147483647/
!$$$      DATA IMACH(10) /     2 /
!$$$      DATA IMACH(11) /    53 /
!$$$      DATA IMACH(12) / -1022 /
!$$$      DATA IMACH(13) /  1024 /
!$$$      DATA IMACH(14) /    53 /
!$$$      DATA IMACH(15) / -1022 /
!$$$      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES.
!
!     NOTE THAT THE PUNCH UNIT, I1MACH(3), HAS BEEN SET TO 7
!     WHICH IS APPROPRIATE FOR THE UNIVAC-FOR SYSTEM.
!     IF YOU HAVE THE UNIVAC-FTN SYSTEM, SET IT TO 1.
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   36 /
!      DATA IMACH( 6) /    6 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   35 /
!      DATA IMACH( 9) / O377777777777 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   27 /
!      DATA IMACH(12) / -128 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   60 /
!      DATA IMACH(15) /-1024 /
!      DATA IMACH(16) / 1023 /
!
!     MACHINE CONSTANTS FOR VAX
!
!      DATA IMACH( 1) /    5 /
!      DATA IMACH( 2) /    6 /
!      DATA IMACH( 3) /    7 /
!      DATA IMACH( 4) /    6 /
!      DATA IMACH( 5) /   32 /
!      DATA IMACH( 6) /    4 /
!      DATA IMACH( 7) /    2 /
!      DATA IMACH( 8) /   31 /
!      DATA IMACH( 9) / 2147483647 /
!      DATA IMACH(10) /    2 /
!      DATA IMACH(11) /   24 /
!      DATA IMACH(12) / -127 /
!      DATA IMACH(13) /  127 /
!      DATA IMACH(14) /   56 /
!      DATA IMACH(15) / -127 /
!      DATA IMACH(16) /  127 /
!
!     MACHINE CONSTANTS FOR THE SEQUENT BALANCE 8000 AND SVS FORTRAN ON
!     THE AT&T 7300 (UNIX PC)
!
!      DATA IMACH( 1) /     0 /
!      DATA IMACH( 2) /     0 /
!      DATA IMACH( 3) /     7 /
!      DATA IMACH( 4) /     0 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     1 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    31 /
!      DATA IMACH( 9) /  2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    24 /
!      DATA IMACH(12) /  -125 /
!      DATA IMACH(13) /   128 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
!     MACHINE CONSTANTS FOR THE RM FORTRAN ON THE AT&T 7300 (UNIX PC)
!
!      DATA IMACH( 1) /     5 /
!      DATA IMACH( 2) /     6 /
!      DATA IMACH( 3) /     7 /
!      DATA IMACH( 4) /     6 /
!      DATA IMACH( 5) /    32 /
!      DATA IMACH( 6) /     1 /
!      DATA IMACH( 7) /     2 /
!      DATA IMACH( 8) /    31 /
!      DATA IMACH( 9) /  2147483647 /
!      DATA IMACH(10) /     2 /
!      DATA IMACH(11) /    24 /
!      DATA IMACH(12) /  -125 /
!      DATA IMACH(13) /   128 /
!      DATA IMACH(14) /    53 /
!      DATA IMACH(15) / -1021 /
!      DATA IMACH(16) /  1024 /
!
      IF (I .LT. 1  .OR.  I .GT. 16) GO TO 999
      I1MACH=IMACH(I)
      RETURN
  999 WRITE(OUTPUT,1999) I
 1999 FORMAT(' I1MACH - I OUT OF BOUNDS',I10)
      STOP
      END FUNCTION I1MACH
      SUBROUTINE DCOPY(N,DX,INCX,DY,INCY)
!
!     COPY REAL(KIND=DP) DX TO REAL(KIND=DP) DY.
!     FOR I = 0 TO N-1, COPY DX(LX+I*INCX) TO DY(LY+I*INCY),
!     WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N, AND LY IS
!     DEFINED IN A SIMILAR WAY USING INCY.
!
      USE double
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(1) :: DX, DY
      INTEGER :: N, I, INCX, INCY, IX, IY, M, MP1, NS
      IF(N.LE.0)RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!        CODE FOR UNEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
        DY(IY) = DX(IX)
        IX = IX + INCX
        IY = IY + INCY
      END DO
      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 7.
!
   20 M = MOD(N,7)
      IF( M .EQ. 0 ) GO TO 40
      DO I = 1,M
        DY(I) = DX(I)
      END DO
      IF( N .LT. 7 ) RETURN
   40 MP1 = M + 1
      DO I = MP1,N,7
        DY(I) = DX(I)
        DY(I + 1) = DX(I + 1)
        DY(I + 2) = DX(I + 2)
        DY(I + 3) = DX(I + 3)
        DY(I + 4) = DX(I + 4)
        DY(I + 5) = DX(I + 5)
        DY(I + 6) = DX(I + 6)
      END DO
      RETURN
!
!        CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
!
   60 CONTINUE
      NS=N*INCX
          DO I=1,NS,INCX
          	DY(I) = DX(I)
          END DO
      RETURN
      END SUBROUTINE DCOPY
      FUNCTION DDOT(N, DX, INCX, DY, INCY)
!
!     RETURNS THE DOT PRODUCT OF DOUBLE PRECISION DX AND DY.
!     DDOT = SUM FOR I = 0 TO N-1 OF  DX(LX+I*INCX) * DY(LY+I*INCY)
!     WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N, AND LY IS
!     DEFINED IN A SIMILAR WAY USING INCY.
!
      USE double
      IMPLICIT NONE
      REAL(KIND=DP) :: DDOT
      REAL(KIND=DP), DIMENSION(1) :: DX, DY
      INTEGER :: INCX, INCY, I, IX, IY, M, MP1, N, NS
      DDOT = 0.D0
      IF(N.LE.0)RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!         CODE FOR UNEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
         DDOT = DDOT + DX(IX)*DY(IY)
        IX = IX + INCX
        IY = IY + INCY
      END DO
      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1.
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 5.
!
   20 M = MOD(N,5)
      IF( M .EQ. 0 ) GO TO 40
      DO I = 1,M
         DDOT = DDOT + DX(I)*DY(I)
      END DO
      IF( N .LT. 5 ) RETURN
   40 MP1 = M + 1
      DO I = MP1,N,5
         DDOT = DDOT + DX(I)*DY(I) + DX(I+1)*DY(I+1) + &
         DX(I + 2)*DY(I + 2) + DX(I + 3)*DY(I + 3) + DX(I + 4)*DY(I + 4)
      END DO
      RETURN
!
!         CODE FOR POSITIVE EQUAL INCREMENTS .NE.1.
!
   60 CONTINUE
      NS = N*INCX
          DO I=1,NS,INCX
          	DDOT = DDOT + DX(I)*DY(I)
          END DO
      RETURN
      END FUNCTION DDOT
      FUNCTION DNRM2 ( N, DX, INCX)
      USE double
      IMPLICIT NONE
      REAL(KIND=DP) :: DNRM2
      INTEGER :: I, J, N, NN, INCX, NEXT
      REAL(KIND=DP) :: HITEST, SUM, XMAX
      REAL(KIND=DP), DIMENSION(1) :: DX
      REAL(KIND=DP), PARAMETER :: ZERO = 0.0D0 , ONE = 1.0D0
      REAL(KIND=DP), PARAMETER :: CUTLO = 8.232D-11, CUTHI = 1.304D191
!
!     EUCLIDEAN NORM OF THE N-VECTOR STORED IN DX() WITH STORAGE
!     INCREMENT INCX .
!     IF    N .LE. 0 RETURN WITH RESULT = 0.
!     IF N .GE. 1 THEN INCX MUST BE .GE. 1
!
!           C.L.LAWSON, 1978 JAN 08
!
!     FOUR PHASE METHOD     USING TWO BUILT-IN CONSTANTS THAT ARE
!     HOPEFULLY APPLICABLE TO ALL MACHINES.
!         CUTLO = MAXIMUM OF  DSQRT(U/EPS)  OVER ALL KNOWN MACHINES.
!         CUTHI = MINIMUM OF  DSQRT(V)      OVER ALL KNOWN MACHINES.
!     WHERE
!         EPS = SMALLEST NO. SUCH THAT EPS + 1. .GT. 1.
!         U   = SMALLEST POSITIVE NO.   (UNDERFLOW LIMIT)
!         V   = LARGEST  NO.            (OVERFLOW  LIMIT)
!
!     BRIEF OUTLINE OF ALGORITHM..
!
!     PHASE 1    SCANS ZERO COMPONENTS.
!     MOVE TO PHASE 2 WHEN A COMPONENT IS NONZERO AND .LE. CUTLO
!     MOVE TO PHASE 3 WHEN A COMPONENT IS .GT. CUTLO
!     MOVE TO PHASE 4 WHEN A COMPONENT IS .GE. CUTHI/M
!     WHERE M = N FOR X() REAL AND M = 2*N FOR COMPLEX.
!
!     VALUES FOR CUTLO AND CUTHI..
!     FROM THE ENVIRONMENTAL PARAMETERS LISTED IN THE IMSL CONVERTER
!     DOCUMENT THE LIMITING VALUES ARE AS FOLLOWS..
!     CUTLO, S.P.   U/EPS = 2**(-102) FOR  HONEYWELL.  CLOSE SECONDS ARE
!                   UNIVAC AND DEC AT 2**(-103)
!                   THUS CUTLO = 2**(-51) = 4.44089E-16
!     CUTHI, S.P.   V = 2**127 FOR UNIVAC, HONEYWELL, AND DEC.
!                   THUS CUTHI = 2**(63.5) = 1.30438E19
!     CUTLO, D.P.   U/EPS = 2**(-67) FOR HONEYWELL AND DEC.
!                   THUS CUTLO = 2**(-33.5) = 8.23181D-11
!     CUTHI, D.P.   SAME AS S.P.  CUTHI = 1.30438D19
!     DATA CUTLO, CUTHI / 8.232D-11,  1.304D19 /
!     DATA CUTLO, CUTHI / 4.441E-16,  1.304E19 /
!     DATA CUTLO, CUTHI / 8.232D-11,  1.304D19 /
!
      IF(N .LT. 0) THEN
         DNRM2  = ZERO
         RETURN
      END IF
!
   10 ASSIGN 30 TO NEXT
      SUM = ZERO
      NN = N * INCX
!                                                 BEGIN MAIN LOOP
      I = 1
   20    GO TO NEXT,(30, 50, 70, 110)
   30 IF( DABS(DX(I)) .GT. CUTLO) GO TO 85
      ASSIGN 50 TO NEXT
      XMAX = ZERO
!
!                        PHASE 1.  SUM IS ZERO
!
   50 IF( DX(I) .EQ. ZERO) GO TO 200
      IF( DABS(DX(I)) .GT. CUTLO) GO TO 85
!
!                                PREPARE FOR PHASE 2.
      ASSIGN 70 TO NEXT
      GO TO 105
!
!                                PREPARE FOR PHASE 4.
!
  100 I = J
      ASSIGN 110 TO NEXT
      SUM = (SUM / DX(I)) / DX(I)
  105 XMAX = DABS(DX(I))
      GO TO 115
!
!                   PHASE 2.  SUM IS SMALL.
!                             SCALE TO AVOID DESTRUCTIVE UNDERFLOW.
!
   70 IF( DABS(DX(I)) .GT. CUTLO ) GO TO 75
!
!                     COMMON CODE FOR PHASES 2 AND 4.
!                     IN PHASE 4 SUM IS LARGE.  SCALE TO AVOID OVERFLOW.
!
  110 IF( DABS(DX(I)) .LE. XMAX ) GO TO 115
         SUM = ONE + SUM * (XMAX / DX(I))**2
         XMAX = DABS(DX(I))
         GO TO 200
!
  115 SUM = SUM + (DX(I)/XMAX)**2
      GO TO 200
!
!
!                  PREPARE FOR PHASE 3.
!
   75 SUM = (SUM * XMAX) * XMAX
!
!
!     FOR REAL OR D.P. SET HITEST = CUTHI/N
!     FOR COMPLEX      SET HITEST = CUTHI/(2*N)
!
   85 HITEST = CUTHI/FLOAT( N )
!
!                   PHASE 3.  SUM IS MID-RANGE.  NO SCALING.
!
      DO J =I,NN,INCX
	IF(DABS(DX(J)) .GE. HITEST) GO TO 100
        SUM = SUM + DX(J)**2
      END DO
      DNRM2 = DSQRT( SUM )
      GO TO 300
!
  200 CONTINUE
      I = I + INCX
      IF ( I .LE. NN ) GO TO 20
!
!              END OF MAIN LOOP.
!
!              COMPUTE SQUARE ROOT AND ADJUST FOR SCALING.
!
      DNRM2 = XMAX * DSQRT(SUM)
  300 CONTINUE
      RETURN
      END FUNCTION DNRM2
      SUBROUTINE DSMV( N, X, Y, NELT, IA, JA, A, ISYM )
!***BEGIN PROLOGUE  DSMV
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSMV-S),
!             Matrix Vector Multiply, Sparse
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Column Format Sparse Matrix Vector Product.
!            Routine to calculate the sparse matrix vector product:
!            Y = A*X.
!***DESCRIPTION
! *Usage:
!     INTEGER  N, NELT, IA(NELT), JA(N+1), ISYM
!     DOUBLE PRECISION X(N), Y(N), A(NELT)
!
!     CALL DSMV(N, X, Y, NELT, IA, JA, A, ISYM )
!         
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! X      :IN       Double Precision X(N).
!         The vector that should be multiplied by the matrix.
! Y      :OUT      Double Precision Y(N).
!         The product of the matrix and the vector.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(N+1).
! A      :IN       Integer A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below. 
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       With  the SLAP  format  the "inner  loops" of  this  routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!
! *Precision:           Double Precision
! *Cautions:
!     This   routine   assumes  that  the matrix A is stored in SLAP 
!     Column format.  It does not check  for  this (for  speed)  and 
!     evil, ugly, ornery and nasty things  will happen if the matrix 
!     data  structure  is,  in fact, not SLAP Column.  Beware of the 
!     wrong data structure!!!
!
! *See Also:
!       DSMTV
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSMV
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N, NELT, ISYM
      INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
      REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A 
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: X
      REAL(KIND=DP), INTENT(OUT), DIMENSION(N) :: Y
      INTEGER :: I, IBGN, ICOL, IEND, IROW, J, JBGN, JEND
!
!         Zero out the result vector.
!***FIRST EXECUTABLE STATEMENT  DSMV
      DO I = 1, N
         Y(I) = 0.0D0
      END DO
!
!         Multiply by A.
!
!Commenting Power Fortran directive
!CVD$R NOCONCUR
      DO ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
!Commenting Power Fortran directive
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ NODEPCHK
         DO I = IBGN, IEND
            Y(IA(I)) = Y(IA(I)) + A(I)*X(ICOL)
         END DO
      END DO
!
      IF( ISYM.EQ.1 ) THEN
!
!         The matrix is non-symmetric.  Need to get the other half in...
!         This loops assumes that the diagonal is the first entry in
!         each column.
!
         DO IROW = 1, N
            JBGN = JA(IROW)+1
            JEND = JA(IROW+1)-1
            IF( JBGN.GT.JEND ) EXIT
            DO J = JBGN, JEND
               Y(IROW) = Y(IROW) + A(J)*X(IA(J))
            END DO
         END DO
      ENDIF
      RETURN
!------------- LAST LINE OF DSMV FOLLOWS ----------------------------
      END SUBROUTINE DSMV
!DECK DSMTV
      SUBROUTINE DSMTV( N, X, Y, NELT, IA, JA, A, ISYM )
!***BEGIN PROLOGUE  DSMTV
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSMTV-S),
!             Matrix transpose Vector Multiply, Sparse
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Column Format Sparse Matrix (transpose) Vector Prdt.
!            Routine to calculate the sparse matrix vector product:
!            Y = A'*X, where ' denotes transpose.
!***DESCRIPTION
! *Usage:
!     INTEGER  N, NELT, IA(NELT), JA(N+1), ISYM
!     DOUBLE PRECISION X(N), Y(N), A(NELT)
!
!     CALL DSMTV(N, X, Y, NELT, IA, JA, A, ISYM )
!         
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! X      :IN       Double Precision X(N).
!         The vector that should be multiplied by the transpose of 
!         the matrix.
! Y      :OUT      Double Precision Y(N).
!         The product of the transpose of the matrix and the vector.
! NELT   :IN       Integer.
!         Number of Non-Zeros stored in A.
! IA     :IN       Integer IA(NELT).
! JA     :IN       Integer JA(N+1).
! A      :IN       Integer A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below. 
! ISYM   :IN       Integer.
!         Flag to indicate symmetric storage format.
!         If ISYM=0, all nonzero entries of the matrix are stored.
!         If ISYM=1, the matrix is symmetric, and only the upper
!         or lower triangle of the matrix is stored.
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       With  the SLAP  format  the "inner  loops" of  this  routine
!       should vectorize   on machines with   hardware  support  for
!       vector gather/scatter operations.  Your compiler may require
!       a  compiler directive  to  convince   it that there  are  no
!       implicit vector  dependencies.  Compiler directives  for the
!       Alliant FX/Fortran and CRI CFT/CFT77 compilers  are supplied
!       with the standard SLAP distribution.
!       
! *Precision:           Double Precision
! *Cautions:
!     This   routine   assumes  that  the matrix A is stored in SLAP 
!     Column format.  It does not check  for  this (for  speed)  and 
!     evil, ugly, ornery and nasty things  will happen if the matrix 
!     data  structure  is,  in fact, not SLAP Column.  Beware of the 
!     wrong data structure!!!
!
! *See Also:
!       DSMV
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSMTV
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N, NELT, ISYM
      INTEGER, INTENT(IN), DIMENSION(NELT) :: IA, JA
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: X
      REAL(KIND=DP), INTENT(OUT), DIMENSION(N) :: Y 
      REAL(KIND=DP), INTENT(IN), DIMENSION(NELT) :: A
      INTEGER :: I, IBGN, ICOL, IEND, IROW, J, JBGN, JEND
!
!         Zero out the result vector.
!***FIRST EXECUTABLE STATEMENT  DSMTV
      DO I = 1, N
         Y(I) = 0.0D0
      END DO
!
!         Multiply by A-Transpose.
!         A-Transpose is stored by rows...
!Commenting Power Fortran directive
!CVD$R NOCONCUR
      DO IROW = 1, N
         IBGN = JA(IROW)
         IEND = JA(IROW+1)-1
!Commenting Power Fortran directive
!CVD$ ASSOC
         DO I = IBGN, IEND
            Y(IROW) = Y(IROW) + A(I)*X(IA(I))
         END DO
      END DO
!
      IF( ISYM.EQ.1 ) THEN
!
!         The matrix is non-symmetric.  Need to get the other half in...
!         This loops assumes that the diagonal is the first entry in
!         each column.
!
         DO ICOL = 1, N
            JBGN = JA(ICOL)+1
            JEND = JA(ICOL+1)-1
            IF( JBGN.GT.JEND ) GOTO 50
!Commenting Power Fortran directive
!CLLL. OPTION ASSERT (NOHAZARD)
!CDIR$ IVDEP
!CVD$ NODEPCHK
            DO J = JBGN, JEND
               Y(IA(J)) = Y(IA(J)) + A(J)*X(ICOL)
            END DO
 50      END DO
      ENDIF
      RETURN
!------------- LAST LINE OF DSMTV FOLLOWS ----------------------------
      END SUBROUTINE DSMTV
      SUBROUTINE DSDI(N, B, X, RWORK, IWORK)
!***BEGIN PROLOGUE  DSDI
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213  (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSDI-S),
!             Linear system solve, Sparse, Iterative Precondition
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Diagonal Matrix Vector Multiply.
!            Routine to calculate the product  X = DIAG*B,  
!            where DIAG is a diagonal matrix.
!***DESCRIPTION
! *Usage:
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! B      :IN       Double Precision B(N).
!         Vector to multiply the diagonal by.
! X      :OUT      Double Precision X(N).
!         Result of DIAG*B.
! RWORK  :IN       Double Precision RWORK(USER DEFINABLE).
!         Work array holding the diagonal of some matrix to scale
!         B by.  This array must be set by the user or by a call
!         to the slap routine DSDS or DSD2S.  The length of RWORK
!         must be > IWORK(4)+N.
! IWORK  :IN       Integer IWORK(10).
!         IWORK(4) holds the offset into RWORK for the diagonal matrix
!         to scale B by.  This is usually set up by the SLAP pre-
!         conditioner setup routines DSDS or DSD2S.
!
! *Description:
!         This routine is supplied with the SLAP package to perform
!         the  MSOLVE  operation for iterative drivers that require
!         diagonal  Scaling  (e.g., DSDCG, DSDBCG).   It  conforms
!         to the SLAP MSOLVE CALLING CONVENTION  and hence does not
!         require an interface routine as do some of the other pre-
!         conditioners supplied with SLAP.
!
! *Precision:           Double Precision
! *See Also:
!       DSDS, DSD2S
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSDI
      USE double
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: N
      INTEGER, INTENT(IN), DIMENSION(10) :: IWORK
      REAL(KIND=DP), INTENT(IN), DIMENSION(N) :: B
      REAL(KIND=DP), INTENT(OUT), DIMENSION(N) :: X 
      REAL(KIND=DP), DIMENSION(1) :: RWORK
      INTEGER :: I, LOCD
!
!         Determine where the inverse of the diagonal 
!         is in the work array and then scale by it.
!***FIRST EXECUTABLE STATEMENT  DSDI
      LOCD = IWORK(4) - 1
      DO I = 1, N
         X(I) = RWORK(LOCD+I)*B(I)
      END DO
      RETURN
!------------- LAST LINE OF DSDI FOLLOWS ----------------------------
      END SUBROUTINE DSDI
      SUBROUTINE DSDS(N, NELT, JA, A, DINV)
!***BEGIN PROLOGUE  DSDS
!***DATE WRITTEN   890404   (YYMMDD)
!***REVISION DATE  890404   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DSDS-D),
!             SLAP Sparse, Diagonal
!***AUTHOR  Greenbaum, Anne, Courant Institute
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  Diagonal Scaling Preconditioner SLAP Set Up.
!            Routine to compute the inverse of the diagonal of a matrix
!            stored in the SLAP Column format.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, JA(NELT)
!     DOUBLE PRECISION A(NELT), DINV(N)
!
!     CALL DSDS( N, NELT, JA, A, DINV )
!
! *Arguments:
! N      :IN       Integer.
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of elements in arrays IA, JA, and A.
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in the SLAP Column
!         format.  See "Description", below. 
! DINV   :OUT      Double Precision DINV(N).
!         Upon return this array holds 1./DIAG(A).
!
! *Description
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       With the SLAP  format  all  of  the   "inner  loops" of this
!       routine should vectorize  on  machines with hardware support
!       for vector   gather/scatter  operations.  Your compiler  may
!       require a compiler directive to  convince it that  there are
!       no  implicit  vector  dependencies.  Compiler directives for
!       the Alliant    FX/Fortran and CRI   CFT/CFT77 compilers  are
!       supplied with the standard SLAP distribution.
!
! *Precision:           Double Precision
!
! *Cautions:
!       This routine assumes that the diagonal of A is all  non-zero
!       and that the operation DINV = 1.0/DIAG(A) will not underflow
!       or overflow.    This  is done so that the  loop  vectorizes.
!       Matricies with zero or near zero or very  large entries will
!       have numerical difficulties  and  must  be fixed before this 
!       routine is called.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***END PROLOGUE  DSDS
      USE double
      IMPLICIT NONE
      INTEGER :: ICOL, N, NELT
      INTEGER, DIMENSION(NELT) :: JA
      REAL(KIND=DP), DIMENSION(NELT) :: A 
      REAL(KIND=DP), DIMENSION(N) :: DINV
!
!         Assume the Diagonal elements are the first in each column.
!         This loop should *VECTORIZE*.  If it does not you may have
!         to add a compiler directive.  We do not check for a zero
!         (or near zero) diagonal element since this would interfere 
!         with vectorization.  If this makes you nervous put a check
!         in!  It will run much slower.
!***FIRST EXECUTABLE STATEMENT  DSDS
      DO ICOL = 1, N
         DINV(ICOL) = 1.0D0/A(JA(ICOL))
      END DO
!         
      RETURN
!------------- LAST LINE OF DSDS FOLLOWS ----------------------------
      END SUBROUTINE DSDS
      SUBROUTINE DAXPY(N,DA,DX,INCX,DY,INCY)
!
!     OVERWRITE DOUBLE PRECISION DY WITH DOUBLE PRECISION DA*DX + DY.
!     FOR I = 0 TO N-1, REPLACE  DY(LY+I*INCY) WITH DA*DX(LX+I*INCX) +
!       DY(LY+I*INCY), WHERE LX = 1 IF INCX .GE. 0, ELSE LX = (-INCX)*N,
!       AND LY IS DEFINED IN A SIMILAR WAY USING INCY.
!
      USE double
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(1) :: DX, DY
      REAL(KIND=DP) :: DA
      INTEGER :: I, N, INCX, INCY, IX, IY, M, MP1, NS
      IF(N.LE.0.OR.DA.EQ.0.D0) RETURN
      IF(INCX.EQ.INCY) IF(INCX-1) 5,20,60
    5 CONTINUE
!
!        CODE FOR NONEQUAL OR NONPOSITIVE INCREMENTS.
!
      IX = 1
      IY = 1
      IF(INCX.LT.0)IX = (-N+1)*INCX + 1
      IF(INCY.LT.0)IY = (-N+1)*INCY + 1
      DO I = 1,N
        DY(IY) = DY(IY) + DA*DX(IX)
        IX = IX + INCX
        IY = IY + INCY
      END DO
      RETURN
!
!        CODE FOR BOTH INCREMENTS EQUAL TO 1
!
!
!        CLEAN-UP LOOP SO REMAINING VECTOR LENGTH IS A MULTIPLE OF 4.
!
   20 M = MOD(N,4)
      IF( M .EQ. 0 ) GO TO 40
      DO I = 1,M
        DY(I) = DY(I) + DA*DX(I)
      END DO
      IF( N .LT. 4 ) RETURN
   40 MP1 = M + 1
      DO I = MP1,N,4
        DY(I) = DY(I) + DA*DX(I)
        DY(I + 1) = DY(I + 1) + DA*DX(I + 1)
        DY(I + 2) = DY(I + 2) + DA*DX(I + 2)
        DY(I + 3) = DY(I + 3) + DA*DX(I + 3)
      END DO
      RETURN
!
!        CODE FOR EQUAL, POSITIVE, NONUNIT INCREMENTS.
!
   60 CONTINUE
      NS = N*INCX
      DO I=1,NS,INCX
      	DY(I) = DA*DX(I) + DY(I)
      END DO
      RETURN
      END SUBROUTINE DAXPY
      SUBROUTINE DS2Y(N, NELT, IA, JA, A)
!***BEGIN PROLOGUE  DS2Y
!***DATE WRITTEN   871119   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  D2A4, D2B4
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DS2Y-D),
!             Linear system, SLAP Sparse
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP Triad to SLAP Column Format Converter.
!            Routine to convert from the SLAP Triad to SLAP Column
!            format.
!***DESCRIPTION
! *Usage:
!     INTEGER N, NELT, IA(NELT), JA(NELT)
!     DOUBLE PRECISION A(NELT)
!
!     CALL DS2Y( N, NELT, IA, JA, A )
!
! *Arguments:
! N      :IN       Integer
!         Order of the Matrix.
! NELT   :IN       Integer.
!         Number of non-zeros stored in A.
! IA     :INOUT    Integer IA(NELT).
! JA     :INOUT    Integer JA(NELT).
! A      :INOUT    Double Precision A(NELT).
!         These arrays should hold the matrix A in either the SLAP
!         Triad format or the SLAP Column format.  See "LONG 
!         DESCRIPTION", below.  If the SLAP Triad format is used
!         this format is translated to the SLAP Column format by
!         this routine.
!
! *Precision:           Double Precision
!
!***LONG DESCRIPTION
!       The Sparse Linear Algebra Package (SLAP) utilizes two matrix
!       data structures: 1) the  SLAP Triad  format or  2)  the SLAP
!       Column format.  The user can hand this routine either of the
!       of these data structures.  If the SLAP Triad format is give
!       as input then this routine transforms it into SLAP Column
!       format.  The way this routine tells which format is given as
!       input is to look at JA(N+1).  If JA(N+1) = NELT+1 then we
!       have the SLAP Column format.  If that equality does not hold
!       then it is assumed that the IA, JA, A arrays contain the 
!       SLAP Triad format.
!       
!       =================== S L A P Triad format ===================
!       This routine requires that the  matrix A be   stored in  the
!       SLAP  Triad format.  In  this format only the non-zeros  are
!       stored.  They may appear in  *ANY* order.  The user supplies
!       three arrays of  length NELT, where  NELT is  the number  of
!       non-zeros in the matrix: (IA(NELT), JA(NELT), A(NELT)).  For
!       each non-zero the user puts the row and column index of that
!       matrix element  in the IA and  JA arrays.  The  value of the
!       non-zero   matrix  element is  placed  in  the corresponding
!       location of the A array.   This is  an  extremely  easy data
!       structure to generate.  On  the  other hand it   is  not too
!       efficient on vector computers for  the iterative solution of
!       linear systems.  Hence,   SLAP changes   this  input    data
!       structure to the SLAP Column format  for  the iteration (but
!       does not change it back).
!       
!       Here is an example of the  SLAP Triad   storage format for a
!       5x5 Matrix.  Recall that the entries may appear in any order.
!
!           5x5 Matrix       SLAP Triad format for 5x5 matrix on left.
!                              1  2  3  4  5  6  7  8  9 10 11
!       |11 12  0  0 15|   A: 51 12 11 33 15 53 55 22 35 44 21
!       |21 22  0  0  0|  IA:  5  1  1  3  1  5  5  2  3  4  2
!       | 0  0 33  0 35|  JA:  1  2  1  3  5  3  5  2  5  4  1
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!       =================== S L A P Column format ==================
!       This routine requires  that the  matrix  A be  stored in the
!       SLAP Column format.  In this format the non-zeros are stored
!       counting down columns (except for  the diagonal entry, which
!       must appear first in each  "column")  and are stored  in the
!       double precision array A.   In other words,  for each column
!       in the matrix put the diagonal entry in  A.  Then put in the
!       other non-zero  elements going down  the column (except  the
!       diagonal) in order.   The  IA array holds the  row index for
!       each non-zero.  The JA array holds the offsets  into the IA,
!       A arrays  for  the  beginning  of each   column.   That  is,
!       IA(JA(ICOL)),  A(JA(ICOL)) points   to the beginning  of the
!       ICOL-th   column    in    IA and   A.      IA(JA(ICOL+1)-1),
!       A(JA(ICOL+1)-1) points to  the  end of the   ICOL-th column.
!       Note that we always have  JA(N+1) = NELT+1,  where N is  the
!       number of columns in  the matrix and NELT  is the number  of
!       non-zeros in the matrix.
!       
!       Here is an example of the  SLAP Column  storage format for a
!       5x5 Matrix (in the A and IA arrays '|'  denotes the end of a 
!       column):
!       
!       5x5 Matrix      SLAP Column format for 5x5 matrix on left.
!       1  2  3    4  5    6  7    8    9 10 11
!       |11 12  0  0 15|   A: 11 21 51 | 22 12 | 33 53 | 44 | 55 15 35
!       |21 22  0  0  0|  IA:  1  2  5 |  2  1 |  3  5 |  4 |  5  1  3
!       | 0  0 33  0 35|  JA:  1  4  6    8  9   12
!       | 0  0  0 44  0|
!       |51  0 53  0 55|
!       
!***REFERENCES  (NONE)
!***ROUTINES CALLED  QS2I1D
!***END PROLOGUE  DS2Y
      USE double
      IMPLICIT NONE
      INTEGER I, IBGN, ICOL, IEND, ITEMP, J 
      INTEGER, INTENT(IN) :: N, NELT
      INTEGER, INTENT(INOUT), DIMENSION(NELT) :: IA, JA
      REAL(KIND=DP), INTENT(INOUT), DIMENSION(NELT) :: A
      REAL(KIND=DP) :: TEMP
!
!         Check to see if the (IA,JA,A) arrays are in SLAP Column 
!         format.  If it's not then transform from SLAP Triad.
!***FIRST EXECUTABLE STATEMENT  DS2LT
      IF( JA(N+1).EQ.NELT+1 ) RETURN
!
!         Sort into ascending order by COLUMN (on the ja array).
!         This will line up the columns.
!
      CALL QS2I1D( JA, IA, A, NELT, 1 )
!         
!         Loop over each column to see where the column indicies change 
!         in the column index array ja.  This marks the beginning of the
!         next column.
!   
!Commenting Power Fortran directive      
!CVD$R NOVECTOR
      JA(1) = 1
 outer: DO ICOL = 1, N-1
         DO J = JA(ICOL)+1, NELT
            IF( JA(J).NE.ICOL ) THEN
               JA(ICOL+1) = J
               CYCLE outer
            ENDIF
         END DO
        END DO outer
      JA(N+1) = NELT+1
!         
!         Mark the n+2 element so that future calls to a SLAP routine 
!         utilizing the YSMP-Column storage format will be able to tell.
!         
      JA(N+2) = 0
!
!         Now loop thru the ia(i) array making sure that the Diagonal
!         matrix element appears first in the column.  Then sort the
!         rest of the column in ascending order.
!
      DO ICOL = 1, N
         IBGN = JA(ICOL)
         IEND = JA(ICOL+1)-1
         DO I = IBGN, IEND
            IF( IA(I).EQ.ICOL ) THEN
!         Swap the diag element with the first element in the column.
               ITEMP = IA(I)
               IA(I) = IA(IBGN)
               IA(IBGN) = ITEMP
               TEMP = A(I)
               A(I) = A(IBGN)
               A(IBGN) = TEMP
               GOTO 40
            ENDIF
         END DO
 40      IBGN = IBGN + 1
         IF( IBGN.LT.IEND ) THEN
            DO I = IBGN, IEND
               DO J = I+1, IEND
                  IF( IA(I).GT.IA(J) ) THEN
                     ITEMP = IA(I)
                     IA(I) = IA(J)
                     IA(J) = ITEMP
                     TEMP = A(I)
                     A(I) = A(J)
                     A(J) = TEMP
                  ENDIF
               END DO
            END DO
         ENDIF
      END DO
      RETURN
!------------- LAST LINE OF DS2Y FOLLOWS ----------------------------
      END SUBROUTINE DS2Y
      SUBROUTINE QS2I1D( IA, JA, A, N, KFLAG )
!***BEGIN PROLOGUE  QS2I1D
!***DATE WRITTEN   761118   (YYMMDD)
!***REVISION DATE  890125   (YYMMDD)
!***CATEGORY NO.  N6A2A
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=INTEGER(QS2I1D-I),
!             QUICKSORT,DOUBLETON QUICKSORT,SORT,SORTING
!***AUTHOR  Jones, R. E., (SNLA)
!           Kahaner, D. K., (NBS)
!           Seager, M. K., (LLNL) seager@lll-crg.llnl.gov
!           Wisniewski, J. A., (SNLA)
!***PURPOSE  Sort an integer array also moving an integer and DP array
!            This routine sorts the integer  array  IA and makes the
!            same interchanges   in the integer   array  JA  and the
!            double precision array A.  The  array IA may be  sorted
!            in increasing order or decreas- ing  order.  A slightly
!            modified QUICKSORT algorithm is used.
!
!***DESCRIPTION
!     Written by Rondall E Jones
!     Modified by John A. Wisniewski to use the Singleton QUICKSORT
!     algorithm. date 18 November 1976.
!
!     Further modified by David K. Kahaner
!     National Bureau of Standards
!     August, 1981
!
!     Even further modification made to bring the code up to the 
!     Fortran 77 level and make it more readable and to carry
!     along one integer array and one double precision array during 
!     the sort by
!     Mark K. Seager
!     Lawrence Livermore National Laboratory
!     November, 1987
!     This routine was adapted from the ISORT routine.
!
!     ABSTRACT
!         This routine sorts an integer array IA and makes the same
!         interchanges in the integer array JA and the double precision
!          array A.  
!         The array a may be sorted in increasing order or decreasing 
!         order.  A slightly modified quicksort algorithm is used.
!
!     DESCRIPTION OF PARAMETERS
!        IA - Integer array of values to be sorted.
!        JA - Integer array to be carried along.
!         A - Double Precision array to be carried along.
!         N - Number of values in integer array IA to be sorted.
!     KFLAG - Control parameter
!           = 1 means sort IA in INCREASING order.
!           =-1 means sort IA in DECREASING order.
!
!***REFERENCES
!     Singleton, R. !., Algorithm 347, "An Efficient Algorithm for 
!     Sorting with Minimal Storage", cacm, Vol. 12, No. 3, 1969, 
!     Pp. 185-187.
!***ROUTINES CALLED  XERROR
!***END PROLOGUE  QS2I1D
      USE double
      IMPLICIT NONE
!Commenting Power Fortran directive
!CVD$R NOVECTOR
!CVD$R NOCONCUR
      INTEGER, DIMENSION(21) :: IL, IU
      INTEGER :: I, IJ, IT, IIT, J, JT, JJT, K, KK, L, M, NN 
      INTEGER, DIMENSION(N) :: IA, JA
      REAL(KIND=DP), DIMENSION(N) :: A 
      REAL(KIND=DP) :: TA, TTA, R
      INTEGER, INTENT(IN) :: N, KFLAG
!
!***FIRST EXECUTABLE STATEMENT  QS2I1D
      NN = N
      IF (NN.LT.1) THEN
         CALL XERROR ( 'QS2I1D- the number of values to be sorted was not positive.',59,1,1)
         RETURN
      ENDIF
      IF( N.EQ.1 ) RETURN
      KK = IABS(KFLAG)
      IF ( KK.NE.1 ) THEN
         CALL XERROR ( 'QS2I1D- the sort control parameter, k, was not 1 or -1.',55,2,1)
         RETURN
      ENDIF
!
!     Alter array IA to get decreasing order if needed.
!
      IF( KFLAG.LT.1 ) THEN
         DO I=1,NN
            IA(I) = -IA(I)
         END DO
      ENDIF
!
!     Sort IA and carry JA and A along.
!     And now...Just a little black magic...
      M = 1
      I = 1
      J = NN
      R = .375_DP
 210  IF( R.LE.0.5898437_DP ) THEN
         R = R + 3.90625E-2
      ELSE
         R = R-.21875_DP
      ENDIF
 225  K = I
!
!     Select a central element of the array and save it in location 
!     it, jt, at.
!
      IJ = I + IDINT( DBLE(J-I)*R )
      IT = IA(IJ)
      JT = JA(IJ)
      TA = A(IJ)
!
!     If first element of array is greater than it, interchange with it.
!
      IF( IA(I).GT.IT ) THEN
         IA(IJ) = IA(I)
         IA(I)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(I)
         JA(I)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(I)
         A(I)   = TA
         TA     = A(IJ)
      ENDIF
      L=J
!                           
!     If last element of array is less than it, swap with it.
!
      IF( IA(J).LT.IT ) THEN
         IA(IJ) = IA(J)
         IA(J)  = IT
         IT     = IA(IJ)
         JA(IJ) = JA(J)
         JA(J)  = JT
         JT     = JA(IJ)
         A(IJ)  = A(J)
         A(J)   = TA
         TA     = A(IJ)
!
!     If first element of array is greater than it, swap with it.
!
         IF ( IA(I).GT.IT ) THEN
            IA(IJ) = IA(I)
            IA(I)  = IT
            IT     = IA(IJ)
            JA(IJ) = JA(I)
            JA(I)  = JT
            JT     = JA(IJ)
            A(IJ)  = A(I)
            A(I)   = TA
            TA     = A(IJ)
         ENDIF
      ENDIF
!
!     Find an element in the second half of the array which is 
!     smaller than it.
!
  240 L=L-1
      IF( IA(L).GT.IT ) GO TO 240
!
!     Find an element in the first half of the array which is 
!     greater than it.
!
  245 K=K+1
      IF( IA(K).LT.IT ) GO TO 245
!
!     Interchange these elements.
!
      IF( K.LE.L ) THEN
         IIT   = IA(L)
         IA(L) = IA(K)
         IA(K) = IIT
         JJT   = JA(L)
         JA(L) = JA(K)
         JA(K) = JJT
         TTA   = A(L)
         A(L)  = A(K)
         A(K)  = TTA
         GOTO 240
      ENDIF
!
!     Save upper and lower subscripts of the array yet to be sorted.
!
      IF( L-I.GT.J-K ) THEN
         IL(M) = I
         IU(M) = L
         I = K
         M = M+1
      ELSE
         IL(M) = K
         IU(M) = J
         J = L
         M = M+1
      ENDIF
      GO TO 260
!
!     Begin again on another portion of the unsorted array.
!                                  
  255 M = M-1
      IF( M.EQ.0 ) GO TO 300
      I = IL(M)
      J = IU(M)
  260 IF( J-I.GE.1 ) GO TO 225
      IF( I.EQ.J ) GO TO 255
      IF( I.EQ.1 ) GO TO 210
      I = I-1
  265 I = I+1
      IF( I.EQ.J ) GO TO 255
      IT = IA(I+1)
      JT = JA(I+1)
      TA =  A(I+1)
      IF( IA(I).LE.IT ) GO TO 265
      K=I
  270 IA(K+1) = IA(K)
      JA(K+1) = JA(K)
      A(K+1)  =  A(K)
      K = K-1
      IF( IT.LT.IA(K) ) GO TO 270
      IA(K+1) = IT
      JA(K+1) = JT
      A(K+1)  = TA
      GO TO 265
!
!     Clean up, if necessary.
!
  300 IF( KFLAG.LT.1 ) THEN
         DO I=1,NN
            IA(I) = -IA(I)
         END DO
      ENDIF
      RETURN
!------------- LAST LINE OF QS2I1D FOLLOWS ----------------------------
      END SUBROUTINE QS2I1D
      SUBROUTINE DCHKW( NAME, LOCIW, LENIW, LOCW, LENW, &
          IERR, ITER, ERR )
!***BEGIN PROLOGUE  DCHKW
!***DATE WRITTEN   880225   (YYMMDD)
!***REVISION DATE  881213   (YYMMDD)
!***CATEGORY NO.  R2
!***KEYWORDS  LIBRARY=SLATEC(SLAP),
!             TYPE=DOUBLE PRECISION(DCHKW-D),
!             SLAP, Error Checking, Workspace Checking
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (415) 423-3141
!             seager@lll-crg.llnl.gov
!***PURPOSE  SLAP WORK/IWORK Array Bounds Checker.
!            This routine checks the work array lengths  and  inter-
!            faces to the SLATEC  error  handler  if  a  problem  is 
!            found.
!***DESCRIPTION
! *Usage:
!     CHARACTER*(*) NAME
!     INTEGER LOCIW, LENIW, LOCW, LENW, IERR, ITER
!     DOUBLE PRECISION ERR
!
!     CALL DCHKW( NAME, LOCIW, LENIW, LOCW, LENW, IERR, ITER, ERR )
!
! *Arguments:
! NAME   :IN       Character*(*).
!         Name of the calling routine.  This is used in the output
!         message, if an error is detected.
! LOCIW  :IN       Integer.
!         Location of the first free element in the integer workspace
!         array.
! LENIW  :IN       Integer.
!         Length of the integer workspace array.
! LOCW   :IN       Integer.
!         Location of the first free element in the double precision 
!         workspace array.
! LENRW  :IN       Integer.
!         Length of the double precision workspace array.
! IERR   :OUT      Integer.
!         Return error flag.
!               IERR = 0 => All went well.
!               IERR = 1 => Insufficient storage allocated for 
!                           WORK or IWORK.
! ITER   :OUT      Integer.
!         Set to 0 if an error is detected.
! ERR    :OUT      Double Precision.
!         Set to a very large number if an error is detected.
!
! *Precision:           Double Precision
!
!***REFERENCES  (NONE)
!***ROUTINES CALLED  D1MACH, XERRWV
!***END PROLOGUE  DCHKW
      USE double
      IMPLICIT NONE
      CHARACTER*(*) NAME
      CHARACTER*72 MESG
      INTEGER :: LOCIW, LENIW, LOCW, LENW, IERR, ITER
      REAL(KIND=DP) :: ERR, D1MACH
!
!         Check the Integer workspace situation.
!***FIRST EXECUTABLE STATEMENT  DCHKW
      IERR = 0
      IF( LOCIW.GT.LENIW ) THEN
         IERR = 1
         ITER = 0
         ERR = D1MACH(2)
         MESG = NAME // ': INTEGER work array too short. '// &
              ' IWORK needs i1: have allocated i2.'
         CALL XERRWV( MESG, LEN(MESG), 1, 1, 2, LOCIW, LENIW, &
              0, 0.0_DP, 0.0_DP )
      ENDIF
!
!         Check the Double Precision workspace situation.
      IF( LOCW.GT.LENW ) THEN
         IERR = 1
         ITER = 0
         ERR = D1MACH(2)
         MESG = NAME // ': DOUBLE PRECISION work array too short. '// &
              ' RWORK needs i1: have allocated i2.'
         CALL XERRWV( MESG, LEN(MESG), 1, 1, 2, LOCW, LENW, &
              0, 0.0_DP, 0.0_DP )
      ENDIF
      RETURN
!------------- LAST LINE OF DCHKW FOLLOWS ----------------------------
      END SUBROUTINE DCHKW

