      SUBROUTINE ADVTL(FB,F,FF)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE

!     THIS SUBROUTINE INTEGRATES CONSERVATIVE CONSTITUENT EQUATIONS
!     PASSIVE AND ACTIVE.
!  It uses a conservative upstream advection scheme developed in
!  Lin, S.J., W.!. Chao, Y.!. Sud, and G.K. Walker, "A Class of the van Leer
!  type transport schemes and its application to the moisture transport in a
!  general circulation model",  Mon. Wea. Rev., 122, 1575-1593, 1994.

!     INCLUDE 'comblk98.h' 
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: FB, F, FF, XFLUX, YFLUX, ZFLUX, &
                     delfiavg, delfi, f0 
      REAL(KIND=DP), DIMENSION(:,:,:), POINTER :: fimin, fimax
      REAL(KIND=DP) :: co
      INTEGER :: i, j, k, is1, is2, k1, k2
      REAL(KIND=DP) :: trel, sour
!      EQUIVALENCE (fimin,A),(fimax,C)
      fimin => A
      fimax => C
      

!***************************************************
	is1=10
	is2=im-is1
	k1=int(kb*15._DP/75._DP)
	k2=int(kb*20._DP/75._DP)
	sour=0
	trel=1
!***************************************************
	
	DO i=1,is1
		DO j=1,jm
			DO k=1,kb
				F0(I,J,K)=23
			END DO
		END DO
	END DO
	DO i=is2,im
		DO j=1,jm
			DO k=1,kb
				F0(I,J,K)=15
			END DO
		END DO
	END DO

      FF=0.
      F(:,:,KB)=F(:,:,KBM1)
      FB(:,:,KB)=FB(:,:,KBM1)

      XFLUX=0._DP
      YFLUX=0._DP
      ZFLUX=0._DP
      delfiavg=0._DP
      delfi=0._DP
      fimin=0._DP
      fimax=0._DP

!  solving equation du/dt+vc*du/dx=0
!  to satisfy CFL-criterion: vc*dtx<1.0
! let dt(i,j)=h(i,j)+et(i,j)        

!     ------------  ADVECTIVE FLUXES -----------
!  ----------x-direction:-----------
      DO i=2,im-1
      	DO j=1,jm
      		DO k=1,kb
      			delfiavg(i,j,k)=0.5_DP*(f(i+1,j,k)*dt(i+1,j)-f(i-1,j,k)*dt(i-1,j))
      			fimin(i,j,k)=min((f(i-1,j,k)*dt(i-1,j)),(f(i,j,k)*dt(i,j)), &
     			(f(i+1,j,k)*dt(i+1,j)))
      			fimax(i,j,k)=max((f(i-1,j,k)*dt(i-1,j)),(f(i,j,k)*dt(i,j)), &
     			(f(i+1,j,k)*dt(i+1,j)))
      			delfi(i,j,k)=sign(1.0_DP,delfiavg(i,j,k))*min(abs(delfiavg(i,j,k)) &
     			,2*dim((f(i,j,k)*dt(i,j)),fimin(i,j,k)),2*dim(fimax(i,j,k) &
     			,(f(i,j,k)*dt(i,j))))
      		END DO
      	END DO
      END DO
! set border values:
      DO j=1,jm
      	DO k=1,kb
        	delfi(1,j,k)=delfi(2,j,k)
        	delfi(im,j,k)=delfi(imm1,j,k)
      	END DO
      END DO
         
      DO i=2,im
      	DO j=1,jm
        	DO k=1,kb-1
!  wall at i:
      			IF (u(i,j,k).ge.0.) then
        			co=0.5_DP*(1.-u(i,j,k)*dti/dx(i-1,j))
        			xflux(i,j,k)=u(i,j,k)* &
     				(f(i-1,j,k)*dt(i-1,j)+delfi(i-1,j,k)*co)
      			ELSE
        			co=0.5_DP*(1.+u(i,j,k)*dti/dx(i,j))
        			xflux(i,j,k)=u(i,j,k)* &
     				(f(i,j,k)*dt(i,j)-delfi(i,j,k)*co)
      			END IF
      		END DO
      	END DO
      END DO

! ----------- y-direction:--------------------
      delfiavg=0._DP
      delfi=0._DP
      fimin=0._DP
      fimax=0._DP
      DO i=1,im
      	DO j=2,jmm1
      		DO k=1,kb
      			delfiavg(i,j,k)=0.5_DP*(f(i,j+1,k)*dt(i,j+1)-f(i,j-1,k)*dt(i,j-1))
      			fimin(i,j,k)=min((f(i,j-1,k)*dt(i,j-1)),(f(i,j,k)*dt(i,j)), &
     			(f(i,j+1,k)*dt(i,j+1)))
      			fimax(i,j,k)=max((f(i,j-1,k)*dt(i,j-1)),(f(i,j,k)*dt(i,j)), &
     			(f(i,j+1,k)*dt(i,j+1)))
      			delfi(i,j,k)=sign(1.0_DP,delfiavg(i,j,k))*min(abs(delfiavg(i,j,k)) &
     			,2*dim((f(i,j,k)*dt(i,j)),fimin(i,j,k)),2*dim(fimax(i,j,k) &
     			,(f(i,j,k)*dt(i,j))))
      		END DO
      	END DO
      END DO
      DO i=1,im
      	DO k=1,kb
        	delfi(i,1,k)=delfi(i,2,k)
        	delfi(i,jm,k)=delfi(i,jmm1,k)
      	END DO
      END DO
!
!  wall at j:
      DO i=1,im
      	DO j=2,jm
      		DO k=1,kb-1
      			IF (v(i,j,k).ge.0.) THEN
        			co=0.5_DP*(1.-v(i,j,k)*dti/dy(i,j-1))
        			yflux(i,j,k)=v(i,j,k)* &
     				((f(i,j-1,k)*dt(i,j-1))+delfi(i,j-1,k)*co)
      			ELSE
        			co=0.5_DP*(1.+v(i,j,k)*dti/dy(i,j))
        			yflux(i,j,k)=v(i,j,k)* &
     				((f(i,j,k)*dt(i,j))-delfi(i,j,k)*co)
      			END IF
      		END DO
      	END DO
     END DO
!--------------ADD HORIZONTAL DIFFUSION-------------------------
!  since we are only time stepping once, we must either use F
!  instead of FB in this calculation, or multiply by 2 since we
!  are only jumping one timestep. Lin claims he runs without
!  any diffusion at all.
      DO k=1,kbm1
      	DO j=2,jm
      		DO i=2,im
!         xflux(i,j,k)=xflux(i,j,k)
!     1    -0.5*(AAM(I,J,K)+AAM(I-1,J,K))*(H(I,J)+H(I-1,J))
!     2    *(FB(I,J,K)-FB(I-1,J,K))*DUM(I,J)/(TPRNU*(DX(I,J)+DX(I-1,J)))
!         yflux(i,j,k)=yflux(i,j,k)
!     1    -0.5*(AAM(I,J,K)+AAM(I,J-1,K))*(H(I,J)+H(I,J-1))
!     2    *(FB(I,J,K)-FB(I,J-1,K))*DVM(I,J)/(TPRNU*(DY(I,J)+DY(I,J-1)))
!
         		xflux(i,j,k)=0.5_DP*(dy(i,j)+dy(i-1,j))*xflux(i,j,k)
         		yflux(i,j,k)=0.5_DP*(dx(i,j)+dx(i,j-1))*yflux(i,j,k)
         	END DO
         END DO
      END DO
!--------------VERTICAL ADVECTION------------------
      delfiavg=0._DP
      delfi=0._DP
      fimin=0._DP
      fimax=0._DP
!  z-direction:
      DO i=1,im
        DO j=1,jm
        	DO k=2,kb-1
      			delfiavg(i,j,k)=0.5_DP*(f(i,j,k-1)-f(i,j,k+1))
      			fimin(i,j,k)=min(f(i,j,k-1),f(i,j,k),f(i,j,k+1))
      			fimax(i,j,k)=max(f(i,j,k-1),f(i,j,k),f(i,j,k+1))
      			delfi(i,j,k)=sign(1.0_DP,delfiavg(i,j,k))*min(abs(delfiavg(i,j,k)) &
     			,2*dim(f(i,j,k),fimin(i,j,k)),2*dim(fimax(i,j,k) &
     			,f(i,j,k)))
      		END DO
      	END DO
      END DO
      DO i=1,im
      	DO j=1,jm
         	delfi(i,j,1)=delfi(i,j,2)
         	delfi(i,j,kb)=delfi(i,j,kbm1)
      	END DO
      END DO
!  wall at k:
      k=1
      DO j=1,jm
      	DO i=1,im
      		IF (w(i,j,k).ge.0.) THEN
        		co=0.5_DP*(1.-w(i,j,k)*dti/(dz(k)*dt(i,j)))
        		zflux(i,j,k)=w(i,j,k)* &
     			(f(i,j,k)+delfi(i,j,k)*co)
      		ELSE
        		zflux(i,j,k)=0.0_DP
      		END IF
      	END DO
      END DO

      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			IF (w(i,j,k).ge.0.) THEN
         			co=0.5*(1.-w(i,j,k)*dti/(dz(k)*dt(i,j)))
        			zflux(i,j,k)=w(i,j,k)* &
     				(f(i,j,k)+delfi(i,j,k)*co)
      			ELSE
        			co=0.5_DP*(1.+w(i,j,k)*dti/(dz(k-1)*dt(i,j)))
        			zflux(i,j,k)=w(i,j,k)* &
     				(f(i,j,k-1)-delfi(i,j,k-1)*co)
      			END IF
      		END DO
      	END DO
      END DO

!  wall at kb:
      k=kb
      DO J=1,JM
      	DO I=1,IM
      		IF (w(i,j,k).ge.0.) THEN
         		zflux(i,j,k)=0.0_DP
      		ELSE
        		co=0.5_DP*(1.+w(i,j,k)*dti/(dz(k-1)*dt(i,j)))
        		zflux(i,j,k)=w(i,j,k)* &
     			(f(i,j,k-1)-delfi(i,j,k-1)*co)
      		END IF
      	END DO
      END DO

! 668  format(16(2x,e10.4))
      DO K=1,KBM1
      	DO J=1,JMM1
      		DO I=1,IMM1
 			ff(i,j,k)=1./dz(k)*(zflux(i,j,k)-zflux(i,j,k+1))
 		END DO
 	END DO
      END DO
!-----------ADD NET HORIZONTAL FLUXES------------------
!  divide by dxdy:
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
  			FF(I,J,K)=FF(I,J,K) &
     			+(XFLUX(I+1,J,K)-XFLUX(I,J,K) &
     			+ YFLUX(I,J+1,K)-YFLUX(I,J,K))/ART(I,J)
     		END DO
     	END DO
      END DO
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
!     ----------  STEP FORWARD ---------
  			FF(I,J,K)=(f(I,J,K)*(H(I,J)+ET(I,J))-DTI*FF(I,J,K)) &
     			/(H(I,J)+ETF(I,J))
     		END DO
     	END DO
      END DO
      IF (sour==1) THEN
      	DO K=1,KBM1
      		DO J=2,JMM1
      			DO I=1,is1

 				FF(I,J,K)=FF(I,J,K)-2.*DTI*( &
     				1+dt(i,j)*(f(i,j,k)-f0(i,j,k))/Trel*ART(I,J)) &
     				/((H(I,J)+ETF(I,J))*ART(I,J))
			END DO
		END DO
	END DO
	DO K=1,KBM1
      		DO J=2,JMM1
      			DO I=is2,im-1

 				FF(I,J,K)=FF(I,J,K)-2.*DTI*( &
     				1+dt(i,j)*(f(i,j,k)-f0(i,j,k))/Trel*ART(I,J)) &
     				/((H(I,J)+ETF(I,J))*ART(I,J))
     			END DO
     		END DO
     	END DO
      END IF

      RETURN
      END SUBROUTINE ADVTL


