!************************************************************************
!     This code is based on POM model.                       		*	
!     BLUMBERG, A.F. AND G.L. MELLOR, A DESCRIPTION OF A THREE          *
!     COASTAL OCEAN CIRCULATION MODEL, THREE DIMENSIONAL SHELF          *
!     MODELS, COASTAL AND ESTUARINE SCIENCES, 5, N.HEAPS, ED.,          *
!     AMERICAN GEOPHYSICAL UNION, 1987.                                 *
!                                                                       * 
!	The non-hydrostatic algorithm is presented in paper             *
!     KANARSKA Y. AND MADERICH V., A non-hydrostatic numerical model    *
!     for calculating free-surface stratified flows, Ocean Dynamics,    *
!     2003. V. 53. P. 176-185.                                          *
!     This version is configured for lock-exchange test                 *
!                                        Yuliya Kanarska February '2005 *
!************************************************************************
	PROGRAM MAIN
	USE BLK1D
	USE BLK2D
	USE BLK33D
	USE BLK3D
	USE BLKCON
	IMPLICIT NONE
!	INCLUDE 'comblk98.h'
	LOGICAL SEAMT
	INTEGER :: mask, I, IEND, IEXT, IMAX, IP, IPRTD2, ISK, &
	   ISPADV, ISPLIT, ISWTCH, J, JMAX, JSK, K, MODE, N, neym, NREAD
        REAL(KIND=DP), DIMENSION(IM,JM) :: UTB, VTB, UTF, VTF, ADVUA, ADVVA,  &
	   TSURF, SSURF, DRX2D, DRY2D, ADX2D, ADY2D, SWRAD
        REAL(KIND=DP), DIMENSION(IM,JM,KB) :: ADVX, ADVY, advw, &
           DRHOX, DRHOY, q, asm, br

	REAL(KIND=DP) :: horcon, ALPHA, ATOT, CBCmax, CBCmin, DAREA, DAYS, DTE2, &
	   DTI2, DVOL, EAVER, PERIOD, PRTD1, SAVER, smag, SMOTH, TAVER, &
	   TIME0, VAMAX, VMAXL, VTOT, Z0B

! --- fede
      REAL(KIND=DP) varxz
      DIMENSION varxz(im,kb)

!--------------------------------------------------------------------
      REAL(KIND=DP) ISPI,ISP2I
      REAL(KIND=DP), PARAMETER :: PI = 3.141592654, SMALL = 1.E-6, KAPPA = 0.4

      GRAV=9.806
      TBIAS=0.
      SBIAS=0.
      IINT=0
      TIME=0.


      RHOREF=1025.
!     mask=1. nonhydrostatic problem
!     mask=0. hydrostatic problem

      neym=1
      mask=1
      smag=0.    
      SEAMT=.TRUE.
      DTE=0.01
      ISPLIT=10    
      DAYS=3.
      PRTD1=1./86400.   
      ISPADV=5
      SMOTH=0.10
      HORCON=0.1
!  N.B.: TPRNI=0. yields zero  horizontal diffusivity !!
      TPRNI=0.  
      UMOL=0.

      MODE=3
      NREAD=0
!
      ISWTCH=100000
      IPRTD2=1

      DTI=DTE*FLOAT(ISPLIT)
      DTE2=DTE*2
      DTI2=DTI*2
      IEND=INT(DAYS*24*3600/DTI+2)
      IPRINT=INT(PRTD1*24*3600/DTI)
!      ISWTCH=ISWTCH*24*3600/INT(DTI)
      WRITE(6,'(''DAYS ='',F10.2)') DAYS
      WRITE(6,7030) MODE,DTE,DTI,IEND,ISPLIT,ISPADV,IPRINT,SMOTH, &
         HORCON,TPRNI   
 7030 FORMAT(//,' MODE =',I3, &
       '  DTE =',F7.2,'   DTI =',F9.1,'     IEND =',I6, &
        '   ISPLIT =',I6,'    ISPADV =',I6,'   IPRINT =',I6,/, &
        '   SMOTH =',F7.2,'   HORCON =',F7.3,'   TPRNI ='F7.3,/)
      WRITE(6,'('' NREAD ='',I5,//)') NREAD
!
      ISPI=1.E0/FLOAT(ISPLIT)
      ISP2I=1.E0/(2.E0*FLOAT(ISPLIT))
!----------------------------------------------------------------------
!             ESTABLISH PROBLEM CHARACTERISTICS
!          ****** ALL UNITS IN M.K.S. SYSTEM ******
!      F,BLANK AND B REFERS TO FORWARD,CENTRAL AND BACKWARD TIME LEVELS.
!----------------------------------------------------------------------
      IP=IM
      ISK=2
      JSK=2
!
!     READ IN GRID DATA AND INITIAL AND LATERAL BOUNDARY CONDITIONS
!------------------------------------------------------------------------
!      IF(SEAMT) THEN         
        CALL SEAMOUNT(TSURF,SSURF)
!      ELSE
!      REWIND(40)
!      READ(40) KBB,Z,ZZ,DZ,DZZ,IMM,JMM,ALON,ALAT,DX,DY,H,FSM,
!     1    DUM,DVM,ART,ARU,ARV,COR,TB,SB,UAB,VAB,ELB,ETB,UB,VB,
!     2    RMEAN,TBE,TBW,TBN,TBS,SBE,SBW,SBN,SBS,
!     3    UABE,UABW,VABN,VABS,RFE,RFW,RFN,RFS
!      ENDIF
      TIME0=0.
!
!********************************************************************
!  Note that lateral thermodynamic boundary conditions are often set equal
!  to the initial conditions and are held constant thereafter. Users can
!  of course create varable boundary conditions.
!********************************************************************
!
!  Additional initial conditions
      DO K=1,KB
	  DO J=1,JM
	      DO I=1,IM
		  Q2B(I,J,K)=1.E-8
		  Q2LB(I,J,K)=1.E-8
		  KH(I,J,K)=1.E-8
		  KM(I,J,K)=1.e-6
		  L(I,J,K)=Q2LB(I,J,K)/(Q2B(I,J,K)+SMALL)
		  KQ(I,J,K)=0.2E0*L(I,J,K)*SQRT(Q2B(I,J,K))
		  AAM(I,J,K)=1.e-6
	      END DO
	  END DO
      END DO

	
      DO I=1,IM
	  DO J=1,JM
	      UA(I,J)=UAB(I,J)
	      VA(I,J)=VAB(I,J)

	      EL(I,J)=ELB(I,J)
	      ET(I,J)=ETB(I,J)
	      D(I,J)=H(I,J)+EL(I,J)
	      DT(I,J)=H(I,J)+ET(I,J)
	      DRX2D(I,J)=0.
	      DRY2D(I,J)=0.
	  END DO
      END DO
      DO K=1,KBM1
	  DO I=1,IM
	      DO J=1,JM
		  T(I,J,K)=TB(I,J,K)
		  S(I,J,K)=SB(I,J,K)
		  U(I,J,K)=UB(I,J,K)
		  V(I,J,K)=VB(I,J,K)
	      END DO
	  END DO
      END DO
      CALL DENS(S,T,RHO)

      RAMP=1.
      CALL BAROPG(DRHOX,DRHOY)
      DO K=1,KBM1
	  DO J=1,JM
	      DO I=1,IM 
		  DRX2D(I,J)=DRX2D(I,J)+DRHOX(I,J,K)*DZ(K)
		  DRY2D(I,J)=DRY2D(I,J)+DRHOY(I,J,K)*DZ(K)
	      ENDDO
	  ENDDO
      ENDDO
    


      
! -----------------------------------------------------------------
!     An empirical specification of the roughness parameter
!   * If the height of the nearest bottom grid < Z0B
!     then set CBC to CBCmax

         Z0B    = .01              ! Roughness parameter (in m)
         CBCmin = .0025            ! min. Drag Coef. Bottom log layer
                                   ! not resolved.
         CBCmax = 1.000            ! max. Drag Coef
! -----------------------------------------------------------------
         DO j = 1, JM
           DO i = 1, IM
            DT(I,J)=H(I,J)
            CBC(I,J) = (KAPPA/LOG((1.+ZZ(KBM1))*H(I,J)/Z0B))**2 
            CBC(I,J) = MAX(CBCmin,CBC(I,J))  
            CBC(I,J)=  MIN(CBCmax,CBC(I,J))  !If this statement is invoked
          END DO                             !probably the wrong choice 
         END DO                              !of Z0B or vertical spacing has 
					     !been made

   
        DO J=1,jM
	    DO I=1,IM
		TPS(I,J)=1./SQRT(1.E0/(DX(I,J)*DX(I,J))+1.E0/(DY(I,J)*DY(I,J))) &
		/SQRT(GRAV*H(I,J))/3.
	    END DO
	END DO	
	dte=0.2
      
!      ISWTCH=ISWTCH*24*3600/INT(DTI)

      DO J=3,JMm1
	  DO I=3,IMm1
	      IF (dte>=tps(i,j)) THEN
		dte=tps(i,j)
	      END IF
	  END DO
      END DO
      ISWTCH=100000
      IPRTD2=1

      DTI=DTE*FLOAT(ISPLIT)
      DTE2=DTE*2
      DTI2=DTI*2
      IEND=INT(DAYS*24*3600/DTI+2)
      IPRINT=INT(PRTD1*24*3600/DTI)
	
      DO N=1,NREAD
	  READ(70) TIME0, &
	  WUBOT,WVBOT,AAM2D,UA,UAB,VA,VAB,EL,ELB,ET,ETB,EGB, &
	  UTB,VTB,U,UB,W,V,VB,T,TB,S,SB,RHO,ADX2D,ADY2D,ADVUA,ADVVA, &
	  KM,KH,KQ,L,Q2,Q2B,AAM,Q2L,Q2LB
      END DO
      DO J=1,JM
	  DO I=1,IM
	      D(I,J)=H(I,J)+EL(I,J)
	      DT(I,J)=H(I,J)+ET(I,J)
          END DO
      END DO
!
      TIME=TIME0

      PERIOD=(2.E0*PI)/ABS(COR(IM/2,JM/2))/86400.

! --- fede
      WRITE(1001) EL

      DO i=1,IM
	  DO k=1,KB
	    varxz(i,k)=RHO(i,3,k)
	  END DO
      END DO
      WRITE(1009) varxz

!***********************************************************************
!                                                                      *
!                  BEGIN NUMERICAL INTEGRATION                         *
!                                                                      *
!*****************************************************************
      WRITE(6,*) dte
      DO IINT=1,IEND
	    
	  TIME=DTI*FLOAT(IINT)/86400.+TIME0
	  RAMP=TIME/PERIOD
	  IF(RAMP.GT.1.E0) RAMP=1.
	  RAMP=1.
	    
	  DO J=2,JMM1
	      DO I=2,IMM1
		  WTSURF(I,J)=0.E0
		  WUSURF(I,J)=0.E0
		  WVSURF(I,J)=0.E0
		  SWRAD(I,J) =0.E0
	      END DO
	  END DO
    !------------------------------------------------------------------------
    !
	  IF(MODE.EQ.2) GOTO 8001
	  CALL ADVCT(ADVX,ADVY)

	  CALL BAROPG(DRHOX,DRHOY)
    !**********************************************************************
    !     HOR VISC = HORCON*DX*DY*SQRT((DU/DX)**2+(DV/DY)**2
    !                                 +.5*(DU/DY+DV/DX)**2)
    !**********************************************************************
    !  If MODE.EQ.2 then initial values of AAM2D are used. If one wishes 
    !  Smagorinsky lateral viscosity and diffusion for an external mode 
    !  calculation, then appropiate code can be adapted from that below
    !  and installed after s.n 102 and before s.n. 5000 in subroutine ADVAVE.

    !  If MODE.EQ.2 then initial values of AAM2D are used. If one wishes 
    !  Smagorinsky lateral viscosity and diffusion for an external mode 
    !  calculation, then appropiate code can be adapted from that below
    !  and installed after s.n 102 and before s.n. 5000 in subroutine ADVAVE.
	    IF (smag==1) THEN
		DO K=2,KBM1
		    DO J=2,JMM1
			DO I=2,IMM1
			    AsM(I,J,K)= &
			    2.*(((U(I+1,J,K)-U(I,J,K))/DX(I,J))**2 &
			    +((V(I,J+1,K)-V(I,J,K))/DY(I,J))**2 &
			    +.5E0*(.25E0*(U(I,J+1,K)+U(I+1,J+1,K)-U(I,J-1,K)-U(I+1,J-1,K)) &
			    /DY(I,J) &
			    +.25E0*(V(I+1,J,K)+V(I+1,J+1,K)-V(I-1,J,K)-V(I-1,J+1,K)) &
			    /DX(I,J))**2+ &
			    ((ww(I,J,K)-ww(I,J,K+1))/(Dz(k)*h(i,j)))**2  &
			    +.5E0*(.25E0*(v(I,J,K-1)+v(I,J+1,K-1)-v(I,J,K+1)-v(I,J+1,K+1)) &
			    /(Dz(k)*h(i,j)) &
			    +.25E0*(ww(I,J+1,K)+ww(I,J+1,K+1)-ww(I,J-1,K)-ww(I,J-1,K+1)) &
			    /Dy(I,J))**2 &
			    +.5E0*(.25E0*(u(I,J,K-1)+u(I+1,J,K-1)-u(I,J,K+1)-u(I+1,J,K+1)) &
			    /(Dz(k)*h(i,j)) &
			    +.25E0*(ww(I+1,J,K)+ww(I+1,J,K+1)-ww(I-1,J,K)-ww(I-1,J,K+1)) &
			    /Dx(I,J))**2)
			END DO
		    END DO
		END DO
		DO K=2,KBM1
		    DO J=2,JMM1
			DO I=2,IMM1
			    br(i,j,k)=-grav/1000. &
			    *(rho(i,j,k-1)-rho(i,j,k+1)/2./dz(k)/h(i,j))
			END DO
		    END DO
		END DO
		DO K=1,KB
		    DO J=1,JM
			DO I=1,IM
			    aam(i,j,k)=1.e-6
			END DO
		    END DO
		END DO
		DO K=2,KBM1
		    DO J=2,JMM1
			DO I=2,IMM1
			    IF (asm(i,j,k)>br(i,j,k)) THEN
				AAM(I,J,K)=(0.1*(DX(I,J)*DY(I,J)*dz(k)*h(i,j))**(1./3.))**2* &
				SQRT(asm(i,j,k)-br(i,j,k))+1.e-6
			    ELSE
				aam(i,j,k)=1.e-6
			    END IF	
			    IF (aam(i,j,k)<=1.e-6) THEN
				aam(i,j,k)=1.e-6
			    END IF
			END DO
		    END DO
		END DO	

		DO  K=1,KB
		    DO  J=1,JM
			DO  I=1,IM
			    aam(1,j,k)=aam(2,j,k)
			    aam(i,1,k)=aam(i,2,k)
			    aam(i,j,1)=aam(i,j,2)
			    aam(im,j,k)=aam(imm1,j,k)
			    aam(i,jm,k)=aam(i,jmm1,k)
			    aam(i,j,kb)=aam(i,j,kbm1)
			END DO
		    END DO
		END DO
		km=aam*0.1
		kh=km
		DO i=1,im
		    DO j=1,jm
			DO k=1,kb
			    IF(km(i,j,k)<1.e-6)THEN
				km(i,j,k)=1.e-6
			    END IF
			END DO
		    END DO
		END DO
	    END IF
    ! --  Form vertical averages of 3-D fields for use in external mode --
	    DO J=1,JM
		DO I=1,IM
		    ADX2D(I,J)=0.
		    ADY2D(I,J)=0.
		    DRX2D(I,J)=0.
		    DRY2D(I,J)=0.
		    AAM2D(I,J)=0.
		END DO
	    END DO
	    DO K=1,KBM1
		DO J=1,JM
		    DO I=1,IM
			ADX2D(I,J)=ADX2D(I,J)+ADVX(I,J,K)*DZ(K)
			ADY2D(I,J)=ADY2D(I,J)+ADVY(I,J,K)*DZ(K)
			DRX2D(I,J)=DRX2D(I,J)+DRHOX(I,J,K)*DZ(K)
			DRY2D(I,J)=DRY2D(I,J)+DRHOY(I,J,K)*DZ(K)
			AAM2D(I,J)=AAM2D(I,J)+AAM(I,J,K)*DZ(K)
		    END DO
		END DO
	    END DO
    !       CALL PRXY(' DRX2D     ',TIME,DRX2D,IP,ISK,JM,JSK,0.0)
    ! ----------------------------------------------------------------------
	    IF (mask==1) THEN
		Ua=0.
		Va=0.
		DO K=2,KB
		    DO J=1,JM
			DO I=1,IM
			    Ua(I,J)=Ua(I,J)+.5*(U(I,J,K)+U(i,j,k-1))*dz(k-1)
			    Va(I,J)=Va(I,J)+.5*(V(I,J,K)+v(i,j,k-1))*DZ(K-1)
			END DO
		    END DO
		END DO
	!	if(isplit.ne.1)then
	!	uab=ua
	!	vab=va
	    END IF
	    CALL ADVAVE(ADVUA,ADVVA)
	    DO J=1,JM
		DO I=1,IM
		    ADX2D(I,J)=ADX2D(I,J)-ADVUA(I,J)
		    ADY2D(I,J)=ADY2D(I,J)-ADVVA(I,J)
		END DO
	    END DO
    !
     8001 CONTINUE
    !------------------------------------------------------------------------
	    DO J=1,JM
		DO I=1,IM
		    EGF(I,J)=EL(I,J)*ISPI
		END DO
	    END DO
	    
	    
	    DO i=2,im
		DO j=2,jm

		    utf(i,j)=ua(i,j)*(d(i,j)+d(i-1,j))*isp2i
		    vtf(i,j)=va(i,j)*(d(i,j)+d(i,j-1))*isp2i
		END DO
	    END DO
	    
    !********** BEGIN EXTERNAL MODE *v**************************************
			  DO 8000 IEXT=1,ISPLIT
    !     write(6,'('' IEXT,TIME ='',I5,F9.2)') IEXT,TIME      
	  DO J=2,JM
	      DO I=2,IM
		  FLUXUA(I,J)=.25E0*(D(I,J)+D(I-1,J))*(DY(I,J)+DY(I-1,J))*UA(I,J)
		  FLUXVA(I,J)=.25E0*(D(I,J)+D(I,J-1))*(DX(I,J)+DX(I,J-1))*VA(I,J)
	      END DO
	  END DO
    !
	  DO J=2,JMM1
	      DO I=2,IMM1
		  ELF(I,J)=ELB(I,J) &
		  -DTE2*(FLUXUA(I+1,J)-FLUXUA(I,J)+FLUXVA(I,J+1)-FLUXVA(I,J)) &
		  /ART(I,J)
	      END DO
	  END DO
    !
	  CALL BCOND1(1)
    !
	  IF (MOD(IEXT,ISPADV).EQ.0) CALL ADVAVE(ADVUA,ADVVA)
    !  Note that ALPHA = 0. is perfectly acceptable. The value, ALPHA = .225
    !  permits a longer time step.
	  ALPHA=0.225     
	  DO J=2,JMM1
	      DO I=2,IM
		  UAF(I,J)=ADX2D(I,J)+ADVUA(I,J) &
		  -ARU(I,J)*.25*(  COR(I,J)*D(I,J)*(VA(I,J+1)+VA(I,J)) &
		  +COR(I-1,J)*D(I-1,J)*(VA(I-1,J+1)+VA(I-1,J)) ) &
		  +.25E0*GRAV*(DY(I,J)+DY(I-1,J))*(D(I,J)+D(I-1,J)) &
		  *( (1.E0-2.E0*ALPHA)*(EL(I,J)-EL(I-1,J)) &
		  +ALPHA*(ELB(I,J)-ELB(I-1,J)+ELF(I,J)-ELF(I-1,J)) )
	      END DO
	  END DO
    !	  5			+drx2d(i,j)+
    !     6      +ARU(I,J)*( WUSURF(I,J)-WUBOT(I,J))
	  DO J=2,JMM1
	      DO I=2,IM
		  UAF(I,J)= &
		  ((H(I,J)+ELB(I,J)+H(I-1,J)+ELB(I-1,J))*ARU(I,J)*UAB(I,J) &
		  -4.E0*DTE*UAF(I,J)) &
		  /((H(I,J)+ELF(I,J)+H(I-1,J)+ELF(I-1,J))*ARU(I,J))
	      END DO
	  END DO
	  DO J=2,JM
	      DO I=2,IMM1
		  VAF(I,J)=ADY2D(I,J)+ADVVA(I,J) &
		  +ARV(I,J)*.25*(  COR(I,J)*D(I,J)*(UA(I+1,J)+UA(I,J)) &
		  +COR(I,J-1)*D(I,J-1)*(UA(I+1,J-1)+UA(I,J-1)) ) &
		  +.25E0*GRAV*(DX(I,J)+DX(I,J-1))*(D(I,J)+D(I,J-1)) &
		  *( (1.E0-2.E0*ALPHA)*(EL(I,J)-EL(I,J-1)) &
		  +ALPHA*(ELB(I,J)-ELB(I,J-1)+ELF(I,J)-ELF(I,J-1)))
	    !	  5			+dry2d(i,j)+
	    !     6    + ARV(I,J)*( WVSURF(I,J)-WVBOT(I,J)   )
	      END DO
	  END DO
	  DO J=2,JM
	      DO I=2,IMM1
		  VAF(I,J)= &
		  ((H(I,J)+ELB(I,J)+H(I,J-1)+ELB(I,J-1))*VAB(I,J)*ARV(I,J) &
		  -4.E0*DTE*VAF(I,J)) &
		  /((H(I,J)+ELF(I,J)+H(I,J-1)+ELF(I,J-1))*ARV(I,J))
	      END DO
	  END DO
	  CALL BCOND1(2)

	  IF (isplit.ne.1) THEN
	      IF (IEXT.LT.(ISPLIT-2)) GO TO 440
	      IF (IEXT.EQ.(ISPLIT-2)) THEN
		  DO J=1,JM
		      DO I=1,IM
			ETF(I,J)=.25*SMOTH*ELF(I,J)
		      END DO
		  END DO
	      ENDIF
	      IF(IEXT.EQ.(ISPLIT-1)) THEN
		  DO J=1,JM
		      DO I=1,IM
			ETF(I,J)=ETF(I,J)+.5*(1.-.5*SMOTH)*ELF(I,J)
		      END DO
		  END DO
	      ENDIF
	      IF(IEXT.EQ.(ISPLIT-0)) THEN
		  DO J=1,JM
		      DO I=1,IM
			ETF(I,J)=(ETF(I,J)+.5*ELF(I,J))
		      END DO
		  END DO
	      END IF
	  ELSE
	    etf=elf
	    egf=elf
	  ENDIF
     440  CONTINUE
    !
    !  TEST FOR CFL VIOLATION. IF SO, PRINT AND STOP
    !
	  VMAXL=100.
	  VAMAX=0.       
	  DO J=1,JM
	      DO I=1,IM
		  IF(ABS(VAF(I,J)).GE.VAMAX) THEN
		    VAMAX=ABS(VAF(I,J))   
		    IMAX=I
		    JMAX=J
		  END IF
	      END DO
	  END DO
	  IF(VAMAX.GT.VMAXL) GO TO 9001
    !    
    !       APPLY FILTER TO REMOVE TIME SPLIT. RESET TIME SEQUENCE.
	  DO J=1,JM
	      DO I=1,IM
		  UA(I,J)=UA(I,J)+.5E0*SMOTH*(UAB(I,J)-2.E0*UA(I,J)+UAF(I,J))
		  VA(I,J)=VA(I,J)+.5E0*SMOTH*(VAB(I,J)-2.E0*VA(I,J)+VAF(I,J))
		  EL(I,J)=EL(I,J)+.5E0*SMOTH*(ELB(I,J)-2.E0*EL(I,J)+ELF(I,J))
		  ELB(I,J)=EL(I,J)
		  EL(I,J)=ELF(I,J)
		  D(I,J)=H(I,J)+EL(I,J)
		  UAB(I,J)=UA(I,J)
		  UA(I,J)=UAF(I,J)
		  VAB(I,J)=VA(I,J)
		  VA(I,J)=VAF(I,J)
	      END DO
	  END DO
    !
	  IF(IEXT.EQ.ISPLIT) GO TO 8000
	  DO J=2,JM
	      DO I=2,IM
		  EGF(I,J)=EGF(I,J)+EL(I,J)*ISPI
		  UTF(I,J)=UTF(I,J)+UA(I,J)*(D(I,J)+D(I-1,J))*ISP2I
		  VTF(I,J)=VTF(I,J)+VA(I,J)*(D(I,J)+D(I,J-1))*ISP2I
	      END DO
	  END DO
    !
    !
     8000 CONTINUE
	  IF(VAMAX.GT.VMAXL) GO TO 9001
    !---------------------------------------------------------------------
    !          END EXTERNAL (2-D) MODE CALCULATION
    !     AND CONTINUE WITH INTERNAL (3-D) MODE CALCULATION
    !---------------------------------------------------------------------
	  IF(IINT.EQ.1.AND.TIME0.EQ.0.) GO TO 8200
	  IF(MODE.EQ.2) GO TO 8200
    !---------------------------------------------------------------------
    !      ADJUST U(Z) AND V(Z) SUCH THAT
    !      VERTICAL AVERAGE OF (U,V) = (UA,VA)
    !---------------------------------------------------------------------
	  IF (mask==0) THEN
	      DO J=1,JM
		  DO I=1,IM
		    TPS(I,J)=0.E0
		  END DO
	      END DO
	      DO K=1,KBM1
		  DO J=1,JM
		      DO I=1,IM
			TPS(I,J)=TPS(I,J)+U(I,J,K)*DZ(K)
		      END DO
		  END DO
	      END DO
	      DO K=1,KBM1
		  DO J=1,JM
		      DO I=2,IM
			  U(I,J,K)=(U(I,J,K)-TPS(I,J))+ &
			  (UTB(I,J)+UTF(I,J))/(DT(I,J)+DT(I-1,J))
		      END DO
		  END DO
	      END DO
	      DO J=1,JM
		  DO I=1,IM
		    TPS(I,J)=0.
		  END DO
	      END DO
	      DO K=1,KBM1
		  DO J=1,JM
		      DO I=1,IM
			TPS(I,J)=TPS(I,J)+V(I,J,K)*DZ(K)
		      END DO
		  END DO
	      END DO
	      DO K=1,KBM1
		  DO J=2,JM
		      DO I=1,IM
			  V(I,J,K)=(V(I,J,K)-TPS(I,J))+ &
			  (VTB(I,J)+VTF(I,J))/(DT(I,J)+DT(I,J-1))
		      END DO
		  END DO
	      END DO
	  END IF
	    
	  IF (mask==0) THEN
	    CALL VERTVL(DTI2)
	    CALL BCOND1(5)
	  END IF

    !----------------------------------------------------------------
    !     VERTVL INPUT = U,V,DT(=H+ET),ETF,ETB; OUTPUT = W
    !----------------------------------------------------------------
	  IF (MODE.EQ.4) GO TO 360
    !   revisar 
	  uf=0
	  vf=0
    !      CALL ADVT(sB,s,DTI2,vF)
	  CALL ADVTl(tB,s,uF)
	  CALL ADVTl(sB,s,vF)
    !      CALL ADVTsm(SB,SCLIM,DTI2,VF,4)
	  CALL PROFT(UF,DTI2)
	  CALL PROFT(VF,DTI2)
	  CALL BCOND1(4)
    !
	  DO K=1,KB
	      DO J=1,JM
		  DO I=1,IM
		      T(I,J,K)=T(I,J,K)+.5*SMOTH*(UF(I,J,K) &
		      +TB(I,J,K)-2.*T(I,J,K))
		      S(I,J,K)=S(I,J,K)+.5*SMOTH*(VF(I,J,K) &
		      +SB(I,J,K)-2.*S(I,J,K))
		      TB(I,J,K)=T(I,J,K)
		      T(I,J,K)=UF(I,J,K)
		      SB(I,J,K)=S(I,J,K)
		      S(I,J,K)=VF(I,J,K)
		  END DO
	      END DO
	  END DO
	  CALL DENS(S,T,RHO)
	  CALL bcond1(4)
      360 CONTINUE


	  dtf=etf+h
	    

	  uf=0.
	  vf=0.
	  CALL ADVU(DRHOX,ADVX,DTI2)
	  CALL ADVV(DRHOY,ADVY,DTI2)

	  CALL profU(DTI2)
	  CALL profV(DTI2)
	  CALL BCOND1(3)


		    
		    
	  IF (mask==1.) THEN

	      CALL advctw2(advw)
	      CALL advew2(advw,dti2)
	      CALL wvert(dti2)
	      DO K=1,KB
		  DO J=1,JM
		      DO I=1,IM
			Wwf(I,J,K)=Wwf(I,J,K)*FSM(I,J)
		      END DO
		  END DO
	      END DO

	      CALL pressure1(q)
	      CALL faz(q)

	  END IF

	    

	    
	  DO J=1,JM
	      DO I=1,IM
		TPS(I,J)=0.E0
	      END DO
	  END DO
	  DO K=1,KB
	      DO J=1,JM
		  DO I=1,IM
		    TPS(I,J)=TPS(I,J)+(UF(I,J,K)+UB(I,J,K)-2.E0*U(I,J,K))*DZ(K)
		  END DO
	      END DO
	  END DO
	  DO K=1,KBM1
	      DO J=1,JM
		  DO I=1,IM
		      U(I,J,K)=U(I,J,K)+.5*SMOTH*(UF(I,J,K)+UB(I,J,K)-2.E0*U(I,J,K) &
		      -TPS(I,J))
		  END DO
	      END DO
	  END DO
	  DO J=1,JM
	      DO I=1,IM
		TPS(I,J)=0.E0
	      END DO
	  END DO
	  DO K=1,KB
	      DO J=1,JM
		  DO I=1,IM
		    TPS(I,J)=TPS(I,J)+(VF(I,J,K)+VB(I,J,K)-2.E0*V(I,J,K))*DZ(K)
		  END DO
	      END DO
	  END DO
	  DO K=1,KB
	      DO J=1,JM
		  DO I=1,IM
		      V(I,J,K)=V(I,J,K)+.5*SMOTH*(VF(I,J,K)+VB(I,J,K)-2.E0*V(I,J,K) &
		      -TPS(I,J))
		  END DO
	      END DO
	  END DO
	  DO K=1,KB
	      DO J=1,JM
		  DO I=1,IM
		      UB(I,J,K)=U(I,J,K)
		      U(I,J,K)=UF(I,J,K)
		      VB(I,J,K)=V(I,J,K)
		      V(I,J,K)=VF(I,J,K)
		  END DO
	      END DO
	  END DO
	  IF (mask==1) THEN
	      DO K=1,KB
		  DO J=1,JM
		      DO I=1,IM
			  W(I,J,K)=W(I,J,K)+.5*SMOTH*(WF(I,J,K)+WB(I,J,K)-2.*W(I,J,K))
			  WW(I,J,K)=WW(I,J,K)+.5*SMOTH*(WWF(I,J,K)+WWB(I,J,K)-2.*WW(I,J,K))
		      END DO
		  END DO
	      END DO
	      WWB=WW
	      WW=WWF
	      wb=w
	      w=wf

	  END IF
     8200 CONTINUE
    !
	  DO J=1,JM
	      DO I=1,IM
		  EGB(I,J)=EGF(I,J)
		  ETB(I,J)=ET(I,J)
		  ET(I,J)=ETF(I,J)
		  DT(I,J)=H(I,J)+ET(I,J)
		  UTB(I,J)=UTF(I,J)
		  VTB(I,J)=VTF(I,J)
	      END DO
	  END DO
    !
    !
    !--------------------------------------------------------------------
    !           BEGIN PRINT SECTION
    !--------------------------------------------------------------------


	  IF (MOD(IINT,IPRINT).NE.0) GO TO 7000

     9001 CONTINUE

    ! -- fede
	  WRITE(6,*) 'salvando...'
	  WRITE(1001) EL

	  DO i=1,IM
	      DO k=1,KB
		varxz(i,k)=RHO(i,3,k)
	      END DO
	  END DO
	  WRITE(1009) varxz
      
	  VTOT=0.
	  ATOT=0.
	  TAVER=0.
	  SAVER=0.
	  EAVER=0.
	  DO K=1,KBM1
	      DO J=1,JM
		  DO I=1,IM
		      DAREA=DX(I,J)*DY(I,J)*FSM(I,J)
		      DVOL =DAREA          *DT(I,J)*DZ(K)          
		      VTOT=VTOT+DVOL    
		      TAVER=TAVER+TB(I,J,K)*DVOL    
		      SAVER=SAVER+SB(I,J,K)*DVOL
		  END DO
	      END DO
	  END DO
	  DO J=1,JM
	      DO I=1,IM
		  DAREA=DX(I,J)*DY(I,J)*FSM(I,J)
		  ATOT=ATOT+DAREA
		  EAVER=EAVER+ET(I,J)*DAREA
	      END DO
	  END DO
	  TAVER=TAVER/VTOT
	  SAVER=SAVER/VTOT
	  EAVER=EAVER/ATOT
	  WRITE(6,'(''  VTOT ='',E20.8,''      ATOT ='',E20.8, ''      EAVER ='',E20.8 )') VTOT,ATOT,EAVER            
	  WRITE(6,'(''  TAVER ='',E20.8, ''     SAVER ='',E20.8/)') TAVER,SAVER
	  IF (VAMAX.GT.VMAXL) THEN
	    
	      WRITE(6,'(//////////////////)')
	      WRITE(6,'(''************************************************'')')
	      WRITE(6,'(''************ ABNORMAL JOB END ******************'')')
	      WRITE(6,'(''************* USER TERMINATED ******************'')')
	      WRITE(6,'(''************************************************'')')
	      WRITE(6,'('' VAMAX ='',E12.3,''   IMAX,JMAX ='',2I5)') &
		       VAMAX,IMAX,JMAX
	      WRITE(6,'('' IINT,IEXT ='',2I6)') IINT,IEXT
	      STOP
	  ENDIF
     7000 CONTINUE

    !     IF(MOD(IINT,IPRINT).EQ.0)
    !    1  WRITE(40) TIME,Z,ZZ,X,Y,DX,DY,H,FSM,U,V,UA,VA,T,S,EL
    !
    !--------------------------------------------------------------------
    !             END PRINT SECTION
    !--------------------------------------------------------------------
	  CALL prin(dti2)
 
      END DO
!***********************************************************************
!                                                                      *
!                  END NUMERICAL INTEGRATION                           *
!                                                                      *
!***********************************************************************
!        CALL EPRXYZ('W VEL. COMP.',TIME,W,IM,ISK,JM,JSK,KB,.00001E0)
!       CALL EPRXYZ('Q2         ',TIME,Q2,IM,ISK,JM,JSK,KB,0.0)
!             SAVE THIS DATA FOR A SEAMLESS RESTART
!     WRITE(140) TIME,
!    1  WUBOT,WVBOT,AAM2D,UA,UAB,VA,VAB,EL,ELB,ET,ETB,EGB,
!    2  UTB,VTB,U,UB,W,V,VB,T,TB,S,SB,RHO,ADX2D,ADY2D,ADVUA,ADVVA,
!    3  KM,KH,KQ,L,Q2,Q2B,AAM,Q2L,Q2LB
!
      STOP
      END PROGRAM MAIN
!
