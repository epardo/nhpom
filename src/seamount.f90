      SUBROUTINE SEAMOUNT(TSURF,SSURF)
      USE BDRY
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'

      REAL(KIND=DP), DIMENSION(IM,JM) :: TSURF, SSURF, hg
      REAL(KIND=DP), PARAMETER :: PI = 3.141593, RAD = .01745329, RE = 6371.E3
      REAL(KIND=DP) curv,lp,ls
      INTEGER :: I,ik, IP, ISK, J, JSK, K
      REAL(KIND=DP) :: DELH, DELX, ELEJMID, ELWJMID, RA, VEL

!     Lock-exchange configuration

      GRAV = 9.807
      DELX=1./im
      DELH=0.5
      RA=10.E3
      H0=.1
      curv=0
! Flow magnitude
      VEL=0.
      CALL DEPTH(Z,ZZ,DZ,DZZ,KB)
	
      DO J=2,JM-1
      	DO I=2,IM-1
      		DX(I,J)=0.5*SQRT((x(I+1,J)-x(I-1,J))**2 &
         	+(y(I+1,J)-y(I-1,J))**2)
      		DY(I,J)=0.5*SQRT((x(I,J+1)-x(I,J-1))**2 &
         	+(y(I,J+1)-y(I,J-1))**2)
      		ART(I,J)=DX(I,J)*DY(I,J)
      		COR(I,J)=0.
      	END DO
      END DO   

      if(curv==0)then
      	DO J=1,JM
      		DO I=1,IM
      			DX(I,J)=DELX   
      			DY(I,J)=0.15/jm

      			ART(I,J)=DX(I,J)*DY(I,J)
      			COR(I,J)=0.
      		END DO
      	END DO
     
      	X(1,:)=0.
      	DO I=2,IM
      		X(I,:)=X(I-1,:)+DX(I-1,:)
      	ENDDO
      	Y(:,1)=0.
      	DO J=2,JM
      		Y(:,j)=Y(:,j-1)+DY(:,J-1)  
      	ENDDO
      endif
      
      DO  J=1,JM
      	DO  I=1,IM
		dx(1,j)=dx(2,j)
		dx(im,j)=dx(imm1,j)
		dy(1,j)=dy(2,j)
		dy(im,j)=dy(imm1,j)

		dx(i,1)=dx(i,2)
		dx(i,jm)=dx(i,jmm1)
		dy(i,1)=dy(i,2)
		dy(i,jm)=dy(i,jmm1)
	END DO
      END DO
      lp=.4
      ls=.5
      DO I=1,IM
      	DO J=1,JM
		if ((x(i,jm/2)-ls)/lp<=1/2..and.(x(i,jm/2)-ls)/lp>=-1/2.) then
      			H(I,J)=h0*(1.-DELH*cos(3.14*(x(i,jm/2)-ls)/lp)* &
			cos(3.14*(x(i,jm/2)-ls)/lp))
		else
			h(i,j)=h0
		endif
	END DO
      END DO
      hg=h
      DO I=1,IM
	DO j=1,jm
		H(Im,j)=10000.
        	H(1,J)=10000.      
        	H(I,1)=10000.
        	H(I,JM)=10000.
        END DO
      END DO

      write(1000) H
!      stop

      DO J=2,JM
      	DO I=2,IM
      		ARU(I,J)=.25*(DX(I,J)+DX(I-1,J))*(DY(I,J)+DY(I-1,J))
  		ARV(I,J)=.25*(DX(I,J)+DX(I,J-1))*(DY(I,J)+DY(I,J-1))
  	END DO
      END DO
      DO J=1,JM
      	ARU(1,J)=ARU(2,J)
      	ARV(1,J)=ARV(2,J)
      END DO        
      DO I=1,IM
      	ARU(I,1)=ARU(I,2)
      	ARV(I,1)=ARV(I,2)
      END DO       
!
!
      IP=IM
      ISK=2
      JSK=2
!     CALL PRXY('  TOPOGRAPHY 1 ',TIME, H,IP,ISK,JM,JSK,10.)
!
      DO J=1,JM
      	DO I=1,IM
      		DT(I,J)=H(I,J)
      		FSM(I,J)=0.
      		DUM(I,J)=0.
      		DVM(I,J)=0.
      		IF (H(I,J).ne.10000.) FSM(I,J)=1.
      	END DO
      END DO
      DO J=2,JM
      	DO I=2,IM
      		DUM(I,J)=FSM(I,J)*FSM(I-1,J)                                     
      		DVM(I,J)=FSM(I,J)*FSM(I,J-1)                                     
      	END DO
      END DO
!
!  Adjust bottom topography so that cell to cell variations
!  in H do not exceed parameter that is set in SLPMIN
!
!     CALL SLPMIN (H,IM,JM,FSM,TPS)
!
!---------------------------------------------------------------------
!  Set initial conditions for seamount problem.
      ik=INT(im/(7.))

      DO K=1,KBm1
      	DO J=1,JM
      		DO I=1,IM
      			if (i<=im/2) then
      				sb(I,J,K)=17.9
      			else      
      				sb(I,J,K)=18.1
      			end if
      		END DO
      	END DO
      END DO

      CALL DENS(SB,TB,RHO)
!
!  In this application, the initial condition for density is 
!  a function of z (cartesian). When this is not so, make sure
!  that RMEAN has been area averaged before transfer to sigma 
!  coordinates.
      DO K=1,KBM1
      	DO J=1,JM
      		DO I=1,IM
  			RMEAN(I,J,K)=RHO(I,J,K)
  		END DO
  	END DO
      END DO
      DO J=1,JM
      	DO I=1,IM
      		TSURF(I,J)=TB(I,J,1)
  		SSURF(I,J)=SB(I,J,1)
  	END DO
      END DO
      DO J=1,JM
      	DO I=1,IM
      		UAB(I,J)=VEL*DUM(I,J)   
      		VAB(I,J)=0.
      		ELB(I,J)=0.0           
      		ETB(I,J)=0.0           
      		AAM2D(I,J)=AAM(I,J,1)
      	END DO
      END DO
!------------------------------------------------
!  Set lateral boundary conditions for use in S.R.BCOND. 
!  In the seamount problem the east and west BCs are open 
!  whereas the south and north BCs are closed through specification
!  of the masks, FSM, DUM and DVM.
!------------------------------------------------
      RFE=1.
      RFW=1.
      RFN=1.
      RFS=1.
      ELE(1)=0.
      ELW(1)=0.
      DO J=2,JMM1
      	UABW(J)=UAB(2,J)
      	UABE(J)=UAB(IMM1,J)
!  Set geostrophically conditioned elevations at the boundaries.
      	ELE(J)=ELE(J-1)-COR(IMM1,J)*UAB(IMM1,J)/GRAV*DY(IMM1,J-1)
      	ELW(J)=ELW(J-1)-COR(2,J)*UAB(2,J)/GRAV*DY(2,J-1)
      END DO
!  Adjust boundary elevations so they are zero in the middle 
!  of the channel.
      ELEJMID=ELE(JMM1/2)
      ELWJMID=ELW(JMM1/2)
      DO J=2,JMM1
      	ELE(J)=(ELE(J)-ELEJMID)*FSM(IM,J)
      	ELW(J)=(ELW(J)-ELWJMID)*FSM(2,J)
      END DO
!
!********************************************************************
!  For the seamount and  (other possible applications) lateral
!  thermodynamic  boundary conditions are set equal to the initial conditions
!  and are held constant thereafter. Users can of course create varable 
!  boundary conditions.
!********************************************************************
      DO K=1,KBM1
      	DO J=1,JM
      		TBE(J,K)=TB(IM,J,K)
      		TBW(J,K)=TB(1,J,K)
      		SBE(J,K)=SB(IM,J,K)
      		SBW(J,K)=SB(1,J,K)
      	END DO
      	DO I=1,IM
      		TBN(I,K)=TB(I,JM,K)
      		TBS(I,K)=TB(I,1 ,K)
      		SBN(I,K)=SB(I,JM,K)
      		SBS(I,K)=SB(I,1 ,K)
      	END DO
      END DO
	h=hg

2101	format(1x,f9.5,2x,e20.5)
2102	format(1x,f9.5,2x,f9.5,e20.5)
      RETURN
      END SUBROUTINE SEAMOUNT
