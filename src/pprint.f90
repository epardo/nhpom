	SUBROUTINE prin(dti2)
	USE BLK1D
	USE BLK2D
        USE BLK3D
	USE BLKCON
	IMPLICIT NONE
!	INCLUDE 'comblk98.h'   
	REAL(KIND=DP) :: dti2
	INTEGER :: i, k
	
		
!------------------------------------------------------------------------
!                    DENSITY
		
      	IF (iint==600.) THEN

	OPEN(30, FILE='rho.dat')
	    DO i=2,imm1
		DO k=2,kbm1  	 
		    WRITE(30,2102)x(i,jm/2),h(i,jm/2)*zz(k),rho(i,jm/2,k)	 
		END DO
	    END DO
	    CLOSE(30)
	END IF
      
      
2102	FORMAT(1x,f9.5,2X,f9.5,2x,e20.5)
	PRINT*,"Time step","",iint
	PRINT*,"Time","",iint*dti2,"s"
	END SUBROUTINE prin

