      SUBROUTINE PROFU(DT2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'    
      REAL(KIND=DP), DIMENSION(IM,JM) :: DH
      REAL(KIND=DP) :: DT2
      INTEGER :: I, J, K, KI
!***********************************************************************
!                                                                      *
!        THE FOLLOWING SECTION SOLVES THE EQUATION                     *
!         DT2*(KM*U')'-U=-UB                                           *
!                                                                      *
!***********************************************************************
      DO J=1,JM
      	DO I=1,IM
    		DH(I,J)=1.0
    	END DO
      END DO
 
      DO J=2,JM
      	DO I=2,IM
   		DH(I,J)=.5E0*(H(I,J)+ETF(I,J)+H(I-1,J)+ETF(I-1,J))
   	END DO
      END DO
      DO K=1,KB
      	DO J=2,JM
      		DO I=2,IM
   			C(I,J,K)=(KM(I,J,K)+KM(I-1,J,K))*.5E0
   		END DO
   	END DO
      END DO
      DO K=2,KB
      	DO J=1,JM
      		DO I=1,IM
      			A(I,J,K-1)=-DT2*(C(I,J,K)+UMOL)/(DZ(K-1)*DZZ(K-1)*DH(I,J)* &
     			DH(I,J))
      			C(I,J,K)=-DT2*(C(I,J,K)+UMOL)/(DZ(K)*DZZ(K-1)*DH(I,J)* &
     			DH(I,J))
     		END DO
     	END DO
      END DO
!
      DO J=1,JM
      	DO I=1,IM
      		EE(I,J,1)=1.
!	A(I,J,1)/(A(I,J,1)-1.E0)
      		GG(I,J,1)=0.
!	(-DT2*WUSURF(I,J)/(-DZ(1)*DH(I,J))-UF(I,J,1))
!     1   /(A(I,J,1)-1.E0)
	END DO
      END DO
      DO K=2,KB
      	DO J=1,JM
      		DO I=1,IM
      			GG(I,J,K)=1.E0/(A(I,J,K)+C(I,J,K)*(1.-EE(I,J,K-1))-1.)
      			EE(I,J,K)=A(I,J,K)*GG(I,J,K)
      			GG(I,J,K)=(C(I,J,K)*GG(I,J,K-1)-UF(I,J,K))*GG(I,J,K)
      		END DO
      	END DO
      END DO
      DO J=2,JMM1
      	DO I=2,IMM1
      		TPS(I,J)=0.5E0*(CBC(I,J)+CBC(I-1,J)) &
     		*SQRT(UB(I,J,KBM1)**2+(.25E0*(VB(I,J,KBM1) &
     		+VB(I,J+1,KBM1)+VB(I-1,J,KBM1)+VB(I-1,J+1,KBM1)))**2)
      		UF(I,J,KBM1)=(C(I,J,KBM1)*GG(I,J,KBM2)-UF(I,J,KBM1))/(TPS(I,J) &
     		*DT2/(-DZ(KBM1)*DH(I,J))-1.E0-(EE(I,J,KBM2)-1.E0)*C(I,J,KBM1))
  		UF(I,J,KBm1)=UF(I,J,KBM1)*DUM(I,J)
  	END DO
      END DO
      DO K=2,KBM1
      	KI=KB-K
      	DO J=2,JMM1
      		DO I=2,IMM1
      			UF(I,J,KI)=(EE(I,J,KI)*UF(I,J,KI+1)+GG(I,J,KI))*DUM(I,J)
      		END DO
      	END DO
      END DO
!
      DO J=2,JMM1
      	DO I=2,IMM1
		WUBOT(I,J)=-TPS(I,J)*UF(I,J,KBM1)
	END DO
      END DO
      RETURN
      END SUBROUTINE PROFU
!            
      SUBROUTINE PROFV(DT2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'    
      REAL(KIND=DP), DIMENSION(IM,JM) :: DH
      REAL(KIND=DP) :: DT2
      INTEGER :: I, J, K, KI
!***********************************************************************
!                                                                      *
!        THE FOLLOWING SECTION SOLVES THE EQUATION                     *
!         DT2*(KM*U')'-U=-UB                                           *
!                                                                      *
!***********************************************************************
!
      DO J=1,JM
      	DO I=1,IM
		DH(I,J)=1.
	END DO
      END DO
      
      DO J=2,JM
      	DO I=2,IM
   		DH(I,J)=.5E0*(H(I,J)+ETF(I,J)+H(I,J-1)+ETF(I,J-1))
   	END DO
      END DO
      
      DO K=1,KB
      	DO J=2,JM
      		DO I=2,IM
   			C(I,J,K)=(KM(I,J,K)+KM(I,J-1,K))*.5E0
   		END DO
   	END DO
      END DO
      
      DO K=2,KB
      	DO J=1,JM
      		DO I=1,IM
      			A(I,J,K-1)=-DT2*(C(I,J,K)+UMOL  )/(DZ(K-1)*DZZ(K-1)*DH(I,J) &
     			*DH(I,J))
      			C(I,J,K)=-DT2*(C(I,J,K)+UMOL  )/(DZ(K)*DZZ(K-1)*DH(I,J) &
     			*DH(I,J))
     		END DO
     	END DO
      END DO
!
      DO J=1,JM
      	DO I=1,IM
      		EE(I,J,1)=1.
!	A(I,J,1)/(A(I,J,1)-1.E0)
      		GG(I,J,1)=0.
!	(-DT2*WVSURF(I,J)/(-DZ(1)*DH(I,J))-VF(I,J,1))
!     1   /(A(I,j,1)-1.E0)
	END DO
      END DO

      DO K=2,KB
      	DO J=1,JM
      		DO I=1,IM
      			GG(I,J,K)=1.E0/(A(I,J,K)+C(I,J,K)*(1.E0-EE(I,J,K-1))-1.E0)
      			EE(I,J,K)=A(I,J,K)*GG(I,J,K)
      			GG(I,J,K)=(C(I,J,K)*GG(I,J,K-1)-VF(I,J,K))*GG(I,J,K)
      		END DO
      	END DO
      END DO

      DO J=2,JMM1
      	DO I=2,IMM1
      		TPS(I,J)=0.5E0*(CBC(I,J)+CBC(I,J-1)) &
     		*SQRT((.25E0*(UB(I,J,KBM1)+UB(I+1,J,KBM1) &
     		+UB(I,J-1,KBM1)+UB(I+1,J-1,KBM1)))**2+VB(I,J,KBM1)**2)
      		VF(I,J,KBm1)=(C(I,J,KBM1)*GG(I,J,KBM2)-VF(I,J,KBM1))/(TPS(I,J) &
     		*DT2/(-DZ(KBM1)*DH(I,J))-1.E0-(EE(I,J,KBM2)-1.E0)*C(I,J,KBM1))
  		VF(I,J,KBm1)=gg(i,j,kb-1)/(1-ee(i,j,kb-1))
  	END DO
      END DO
!  VF(I,J,KBM1)*DVM(I,J)
      DO K=2,KBM1
      	KI=KB-K
      	DO J=2,JMM1
      		DO I=2,IMM1
      			VF(I,J,KI)=(EE(I,J,KI)*VF(I,J,KI+1)+GG(I,J,KI))*DVM(I,J)
      		END DO
      	END DO
      END DO
!
      DO J=2,JMM1
      	DO I=2,IMM1
		WVBOT(I,J)=-TPS(I,J)*VF(I,J,KBM1)
	END DO
      END DO
      RETURN
      END SUBROUTINE PROFV


	SUBROUTINE WVERT(DT2)
	USE BLK1D
	USE BLK2D
	USE BLK33D
	USE BLK3D
	IMPLICIT NONE
!	INCLUDE 'comblk98.h' 
	REAL(KIND=DP), DIMENSION(IM,JM) :: UAR, DH
	REAL(KIND=DP), DIMENSION(KB) :: U111, U011, U211,PRU
	REAL(KIND=DP) RK,RK1
	REAL(KIND=DP) :: DT2
        INTEGER :: I, J, K
	DO I=1,iM
		DO J=2,JM
			DH(I,J)=(ETF(I,J)+H(I,J))
		END DO
	END DO

	DO i=1,iM
		DO j=1,JM
			uar(i,j)=dt2/(DH(i,j)*DH(i,j))
		END DO
	END DO

	CALL coef2(aaf,bbf,ccf,fff)
	                   
	DO J=2,JM 
		DO i=2,iM
			DO k=2,kB-1
				rk=KM(i,j,k)/(dzz(k-1)*dz(k))
				rk1=KM(i,j,k+1)/(dz(k)*dzZ(k))	
				u111(k)=1.0+uar(i,j)*(rk+rk1)
				u011(k)=-uar(i,j)*rk
				u211(k)=-uar(i,j)*rk1

				pru(k)=wwF(i,j,k)
			END DO
			u111(1)=1.
			u211(1)=0.

			pru(1)=fff(i,j,1)
	
			u111(kB)=1.
			u011(kB)=0.
			pru(kB)=fff(i,j,kb)
  			CALL SUBINV(1,kB,u011,u111,u211,pru)
			DO k=1,kB
				wwF(i,j,k)=pru(k)
				!wwF(i,j,1)=wwF(i,j,2)
			END DO
		END DO
	END DO

	RETURN
	END SUBROUTINE WVERT
	
	SUBROUTINE SUBINV(il,iu,Pb,Pd,Pa,Pc)
	USE double
	IMPLICIT NONE
	REAL(KIND=DP) Pb(iu),Pd(iu),Pa(iu),Pc(iu) 
	INTEGER :: il, iu, i, j, lp
	REAL(KIND=DP) :: R
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! This subroutine invert of 3-diagonal matrice                            
! Output: cc(k)
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC	    
        lp=il+1
	DO i=lp,iu
		R=Pb(i)/Pd(i-1)
		Pd(i)=Pd(i)-R*Pa(i-1)
		Pc(i)=Pc(i)-R*Pc(i-1)
	END DO
        Pc(iu)=Pc(iu)/Pd(iu)
	DO i=lp,iu
		j=iu-i+il
		Pc(j)=(Pc(j)-Pa(j)*Pc(j+1))/Pd(j)	
	END DO
        RETURN 
        END SUBROUTINE SUBINV
             
      SUBROUTINE PROFQ(DT2)
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE PROM
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'    
      REAL(KIND=DP) KAPPA        
      REAL(KIND=DP) :: DT2, A1, A2, B1, B2, C1, COEF1, COEF2, COEF3, COEF4, COEF5, &
           CONST1, E1, E2, E3, GHC, P, SEF, SMALL, SP, SQ, TP
      INTEGER :: I, J, K, KI
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: GH, PROD, KN, BOYGR, STF
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: SM, SH, CC
      REAL(KIND=DP), DIMENSION(:, :), POINTER :: DH
      EQUIVALENCE (PROD,KN)

      DATA A1,B1,A2,B2,C1/0.92,16.6,0.74,10.1,0.08/
      DATA E1/1.8/,E2/1.33/,E3/1.0/
      DATA KAPPA/0.40/,SQ/0.20/,SEF/1./
      DATA SMALL/1.E-8/
!
!
      SM => A
      SH => C
      DH => TPS
      CC => DTEF
      DO J=1,JM
      	DO I=1,IM
   		DH(I,J)=H(I,J)+ETF(I,J)
   	END DO
      END DO
      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			A(I,J,K)=-DT2*(KQ(I,J,K+1)+KQ(I,J,K)+2.*UMOL)*.5 &
     			/(DZZ(K-1)*DZ(K)*DH(I,J)*DH(I,J))
      			C(I,J,K)=-DT2*(KQ(I,J,K-1)+KQ(I,J,K)+2.*UMOL)*.5 &
     			/(DZZ(K-1)*DZ(K-1)*DH(I,J)*DH(I,J))
     		END DO
     	END DO
      END DO 
!***********************************************************************
!                                                                      *
!        THE FOLLOWING SECTION SOLVES THE EQUATION                     *
!        DT2*(KQ*Q2')' - Q2*(2.*DT2*CC+1.) = -Q2B                    *
!                                                                      *
!***********************************************************************
!------  SURFACE AND BOTTOM B.!.S ------------
      CONST1=16.6**.6666667*SEF
      DO J=1,JMM1
      	DO I=1,IMM1
      		EE(I,J,1)=0.
      		GG(I,J,1)=SQRT((.5*(WUSURF(I,J)+WUSURF(I+1,J)))**2 &
     		+(.5*(WVSURF(I,J)+WVSURF(I,J+1)))**2)*CONST1
      		UF(I,J,KB)=SQRT((.5*(WUBOT(I,J)+WUBOT(I+1,J)))**2 &
     		+(.5*(WVBOT(I,J)+WVBOT(I,J+1)))**2)*CONST1
     	END DO
      END DO
!----- Calculate speed of sound squared ----------------------------
      DO K=1,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			TP=T(I,J,K)+TBIAS
      			SP=S(I,J,K)+SBIAS
!      Calculate pressure in units of decibars
      			P=-GRAV*RHOREF*ZZ(K)*DH(I,J)*1.E-4
      			CC(I,J,K)=1449.1+.00821*P+4.55*TP-.045*TP**2 &
     			+1.34*(SP- 35.0)  
      			CC(I,J,K)=CC(I,J,K)/SQRT((1.-.01642*P/CC(I,J,K)) &
     			*(1.-0.40*P/CC(I,J,K)**2))
     		END DO
     	END DO
      END DO
!----- Calculate buoyancy gradient ---------------------------------
      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			Q2B(I,J,K)=ABS(Q2B(I,J,K))
      			Q2LB(I,J,K)=ABS(Q2LB(I,J,K))
      			BOYGR(I,J,K)=GRAV*((RHO(I,J,K-1)-RHO(I,J,K))/(DZZ(K-1)*DH(I,J))) &
     			+GRAV**2*2./(CC(I,J,K-1)**2+CC(I,J,K)**2)
     		END DO
     	END DO
      END DO
 
      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			L(I,J,K)=Q2LB(I,J,K)/Q2B(I,J,K)
      			GH(I,J,K)=L(I,J,K)**2/Q2B(I,J,K)*BOYGR(I,J,K)              
      			GH(I,J,K)=MIN(GH(I,J,K),.028)
      		END DO
      	END DO
      END DO

      DO J=1,JM
      	DO I=1,IM
      		L(I,J,1)=0.   
      		L(I,J,KB)=0.   
      		GH(I,J,1)=0.  
 		GH(I,J,KB)=0.
 	END DO
      END DO
!------ CALC. T.K.E. PRODUCTION -----------------------------------
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=1,IMM1
      			PROD(I,J,K)=KM(I,J,K)*.25*SEF &
     			*( (U(I,J,K)-U(I,J,K-1)+U(I+1,J,K)-U(I+1,J,K-1))**2 &
     			+(V(I,J,K)-V(I,J,K-1)+V(I,J+1,K)-V(I,J+1,K-1))**2 ) &
     			/(DZZ(K-1)*DH(I,J))**2
  			PROD(I,J,K)=PROD(I,J,K)+KH(I,J,K)*BOYGR(I,J,K)
  		END DO
  	END DO
      END DO
      GHC=-6.0
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			STF(I,J,K)=1.
      			IF (GH(I,J,K).LT.0.) STF(I,J,K)=1.0-0.9*(GH(I,J,K)/GHC)**1.5
      			IF(GH(I,J,K).LT.GHC) STF(I,J,K)=0.1
  			CC(I,J,K)=Q2B(I,J,K)*SQRT(Q2B(I,J,K))/(B1*Q2LB(I,J,K)+SMALL) &
     			*STF(I,J,K)
     		END DO
     	END DO
      END DO
      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			GG(I,J,K)=1./(A(I,J,K)+C(I,J,K)*(1.-EE(I,J,K-1)) &
     			-(2.*DT2*CC(I,J,K)+1.) )
      			EE(I,J,K)=A(I,J,K)*GG(I,J,K)
      			GG(I,J,K)=(-2.*DT2*PROD(I,J,K) &
     			+C(I,J,K)*GG(I,J,K-1)-UF(I,J,K))*GG(I,J,K)
     		END DO
     	END DO
      END DO

      DO K=1,KBM1
      	KI=KB-K
      	DO J=1,JM
      		DO I=1,IM
      			UF(I,J,KI)=EE(I,J,KI)*UF(I,J,KI+1)+GG(I,J,KI)
      		END DO
      	END DO
      END DO

!***********************************************************************
!                                                                      *
!        THE FOLLOWING SECTION SOLVES THE EQUATION                     *
!        DT2(KQ*Q2L')' - Q2L*(DT2*CC+1.) = -Q2LB                     *
!                                                                      *
!***********************************************************************
      DO J=1,JM
      	DO I=1,IM
      		IF (UF(I,J,2).LT.SMALL) UF(I,J,2)=SMALL
      		EE(I,J,2)=0.
      		GG(I,J,2)=-KAPPA*Z(2)*DH(I,J)*UF(I,J,2)
      		VF(I,J,KB)=0.
      	END DO
      END DO
  
      DO K=3,KBM1 
      	DO J=1,JM
      		DO I=1,IM
      			CC(I,J,K) =CC(I,J,K)*(1.+E2*((1./ABS(Z(K)-Z(1))+ &
     			1./ABS(Z(K)-Z(KB))) *L(I,J,K)/(DH(I,J)*KAPPA))**2)
      			GG(I,J,K)=1./(A(I,J,K)+C(I,J,K)*(1.-EE(I,J,K-1)) &
     			-(DT2*CC(I,J,K)+1.))
      			EE(I,J,K)=A(I,J,K)*GG(I,J,K)
      			GG(I,J,K)=(DT2*(-PROD(I,J,K) &
     			*L(I,J,K)*E1)+C(I,J,K)*GG(I,J,K-1)-VF(I,J,K))*GG(I,J,K)
     		END DO
     	END DO
      END DO

      DO K=1,KB-2
      	KI=KB-K
      	DO J=1,JM
      		DO I=1,IM
      			VF(I,J,KI)=EE(I,J,KI)*VF(I,J,KI+1)+GG(I,J,KI)
      		END DO
      	END DO
      END DO

      DO K=2,KBM1
      	DO J=1,JM
      		DO I=1,IM
      			IF (UF(I,J,K).GT.SMALL.AND.VF(I,J,K).GT.SMALL) CYCLE
          		UF(I,J,K)=SMALL
          		VF(I,J,K)=SMALL          		
          	END DO
  	END DO
      END DO
!***********************************************************************
!                                                                      *
!               THE FOLLOWING SECTION SOLVES FOR KM AND KH             *
!                                                                      *
!***********************************************************************
      COEF4=18.*A1*A1+9.*A1*A2
      COEF5=9.*A1*A2
! NOTE THAT SM,SH LIMIT TO INFINITY WHEN GH APPROACHES 0.0288
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			COEF1=A2*(1.-6.*A1/B1*STF(I,J,K))
      			COEF2=3.*A2*B2/STF(I,J,K)+18.*A1*A2
      			COEF3=A1*(1.-3.*C1-6.*A1/B1*STF(I,J,K))
      			SH(I,J,K)=COEF1/(1.-COEF2*GH(I,J,K))
      			SM(I,J,K)=COEF3+SH(I,J,K)*COEF4*GH(I,J,K)
      			SM(I,J,K)=SM(I,J,K)/(1.-COEF5*GH(I,J,K))
      		END DO
      	END DO
      END DO
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			KN(I,J,K)=L(I,J,K)*SQRT(ABS(Q2(I,J,K)))
      			KQ(I,J,K)=(KN(I,J,K)*.41*SH(I,J,K)+KQ(I,J,K))*.5
      			KM(I,J,K)=(KN(I,J,K)*SM(I,J,K)+KM(I,J,K))*.5
      			KH(I,J,K)=(KN(I,J,K)*SH(I,J,K)+KH(I,J,K))*.5
      		END DO
      	END DO
      END DO
      RETURN   
      END SUBROUTINE PROFQ
