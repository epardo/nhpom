      SUBROUTINE ADVTsm(FB,FCLIM,DTI2,FF,NItera)

!     INCLUDE 'mycomblk98.h'
      USE BLK1D
      USE BLK2D
      USE BLK3D
      USE BLKCON
      USE double
!
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: FB, FF, FCLIM, FBMem, &
            XMassFlux, YMassFlux, ZWFlux, XFlux, YFlux, ZFlux
      REAL(KIND=DP), DIMENSION(IM,JM) :: Eta
      REAL(KIND=DP) :: DTI2
      INTEGER :: I, J, K, ITERA, NItera
	
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			XMassFlux(I,J,K)=0.
      			YMassFlux(I,J,K)=0.
      		END DO
      	END DO
      END DO

!
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IM
      			XMassFlux(I,J,K)=0.25*(DY(I-1,J)+DY(I,J))*(DT(I-1,J)+DT(I,J))* &
     			U(I,J,K)
     		END DO
     	END DO
  
      	DO J=2,JM
      		DO I=2,IMM1
      			YMassFlux(I,J,K)=0.25*(DX(I,J-1)+DX(I,J))*(DT(I,J-1)+DT(I,J))* &
     			V(I,J,K)
     		END DO
     	END DO
      END DO
!
      DO J=1,JM
      	DO I=1,IM
		FB(I,J,KB)=FB(I,J,KBM1)
  		Eta(I,J)=ETB(I,J)
  	END DO
      END DO
!
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
			ZWFlux(I,J,K)=W(I,J,K)
  			FBMem(I,J,K)=FB(I,J,K)
  		END DO
  	END DO
      END DO
!-----------------------------------------------------------------------
!   		       START
!	   	 SMOLARKIEWICZ SCHEME 
!-----------------------------------------------------------------------
      DO ITERA=1,NItera
!-----------------------------------------------------------------------
!           UPWIND ADVECTION SCHEME 
!-----------------------------------------------------------------------
      	DO K=1,KBM1
      		DO J=2,JM
      			DO I=2,IM
      				XFlux(I,J,K)=0.5*( &
     				(XMassFlux(I,J,K)+ABS(XMassFlux(I,J,K))) &
     				*FBMem(I-1,J,K)+ &
     				(XMassFlux(I,J,K)-ABS(XMassFlux(I,J,K))) &
     				*FBMem(I,J,K)) 
!
      				YFlux(I,J,K)=0.5*( &
     				(YMassFlux(I,J,K)+ABS(YMassFlux(I,J,K))) &
     				*FBMem(I,J-1,K)+ &
     				(YMassFlux(I,J,K)-ABS(YMassFlux(I,J,K))) &
     				*FBMem(I,J,K)) 
     			END DO
     		END DO
      	END DO

!
      	DO J=2,JMM1
      		DO I=2,IMM1
      			ZFlux(I,J,1)=0.0
  			ZFlux(I,J,KB)=0.0
		END DO
	END DO
!
      	DO K=2,KBM1
      		DO J=2,JMM1
      			DO I=2,IMM1
      				ZFlux(I,J,K)=0.5*( &
     				(ZWFlux(I,J,K)+ABS(ZWFlux(I,J,K))) &
     				*FBMem(I,J,K)+ &
     				(ZWFlux(I,J,K)-ABS(ZWFlux(I,J,K))) &
     				*FBMem(I,J,K-1))
  				ZFlux(I,J,K)=ZFlux(I,J,K)*ART(I,J)
  			END DO
  		END DO
  	END DO
!-----------------------------------------------------------------------
!        ADD NET ADVECTIVE FLUXES; THEN STEP FORWARD IN TIME 
!-----------------------------------------------------------------------
      	DO K=1,KBM1
      		DO J=2,JMM1
      			DO I=2,IMM1
      				FF(I,J,K) = XFlux(I+1,J,K)-XFlux(I,J,K) &
     				+YFlux(I,J+1,K)-YFlux(I,J,K) &
     				+(ZFlux(I,J,K)-ZFlux(I,J,K+1))/DZ(K)
     				
  				FF(I,J,K)=(FBMem(I,J,K)*(H(I,J)+Eta(I,J))*ART(I,J)- &
     				DTI2*FF(I,J,K))/((H(I,J)+ETF(I,J))*ART(I,J))
     			END DO
     		END DO
     	END DO
!-----------------------------------------------------------------------
!                ANTIDIFFUSION VELOCITY 
!-----------------------------------------------------------------------
      	CALL SMOL_ADIF(XMassFlux,YMassFlux,ZWFlux,FF,DTI2)
!
      	DO J=1,JM
      		DO I=1,IM
      			DO K=1,KB
      				Eta(I,J)=ETF(I,J)
      		    		FBMem(I,J,K)=FF(I,J,K)
      		    	END DO
	    	END DO
    	END DO
!-----------------------------------------------------------------------
!   		          END
!	   	 SMOLARKIEWICZ SCHEME 
!-----------------------------------------------------------------------
      END DO
!-----------------------------------------------------------------------
!         ADD HORIZONTAL DIFFUSIVE FLUXES 
!-----------------------------------------------------------------------
      DO K=1,KB   
      	DO J=1,JM
      		DO I=1,IM
 			FB(I,J,K)=FB(I,J,K)-FCLIM(I,J,K)
 		END DO
 	END DO
      END DO
!
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IM
      			XMassFlux(I,J,K)=0.5*(AAM(I,J,K)+AAM(I-1,J,K))
      			YMassFlux(I,J,K)=0.5*(AAM(I,J,K)+AAM(I,J-1,K))
      		END DO
      	END DO
      END DO
! 
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IM
      			XFlux(I,J,K)= &
     			-XMassFlux(I,J,K)*(H(I,J)+H(I-1,J))*TPRNI &
     			*(FB(I,J,K)-FB(I-1,J,K))*DUM(I,J)/(DX(I,J)+DX(I-1,J)) &
     			*0.5*(DY(I,J)+DY(I-1,J))
     			
      			YFlux(I,J,K)= &
     			-YMassFlux(I,J,K)*(H(I,J)+H(I,J-1))*TPRNI &
     			*(FB(I,J,K)-FB(I,J-1,K))*DVM(I,J)/(DY(I,J)+DY(I,J-1)) &
     			*0.5*(DX(I,J)+DX(I,J-1))
     		END DO
     	END DO
      END DO
!
      DO K=1,KB   
      	DO J=1,JM
      		DO I=1,IM
 			FB(I,J,K)=FB(I,J,K)+FCLIM(I,J,K)
 		END DO
 	END DO
      END DO
!
!----- ADD NET HORIZONTAL FLUXES; THEN STEP FORWARD IN TIME --------
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
  			FF(I,J,K)=FF(I,J,K)-DTI2*( &
     			+XFlux(I+1,J,K)-XFlux(I,J,K) &
     			+YFlux(I,J+1,K)-YFlux(I,J,K)) &
     			/((H(I,J)+ETF(I,J))*ART(I,J))
     		END DO
     	END DO
      END DO
!--------------------------------------------------------------------
      RETURN
      END SUBROUTINE ADVTsm

      SUBROUTINE SMOL_ADIF(XFlux,YFlux,ZFlux,FF,DTI2)
!**********************************************************************
!   THIS SUBROUTINE CALCULATES THE ANTIDIFFUSIVE VELOCITY USED TO     *
!   REDUCE THE NUMERICAL DIFFUSION ASSOCIATED WITH THE UPSTREAM       *
!   DIFFERENCING SCHEME. A SWITCH OPTION IS ALSO IMPLEMENTED FOR      *
!   THE SHOCK-TYPE INITIAL CONDITIONS                  	    *	
!**********************************************************************
!
      USE BLK1D
      USE BLK2D
      USE double
      IMPLICIT NONE
!     INCLUDE 'mycomblk98.h'
      
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: FF, XFlux, YFlux, ZFlux, &
                      SW_U, SW_V, SW_Z
      REAL(KIND=DP) :: Mol, DTI2, A1, A2, Abs_1, Abs_2, SW, U2Dt, UDx, V2Dt, VDy, &
              W2Dt, WDz
      INTEGER :: I, J, K
      REAL(KIND=DP), PARAMETER :: VALUE_MAX=1.E20, VALUE_MIN=1.E-9
      REAL(KIND=DP), PARAMETER :: EPSILON=1.0E-14, ES=.15E-2
!
!------------------------------------------------------------------------
!                  TEMPERATURE AND SALINITY MASK 
!------------------------------------------------------------------------
      DO K=1,KB
      	DO I=1,IM
      		DO J=1,JM
      			FF(I,J,K)=FF(I,J,K)*FSM(I,J)
      		END DO
      	END DO
      END DO
 
!-----------------------------------------------------------------------
!                        SWITCH OPTION
!-----------------------------------------------------------------------
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IM
      			A2=(FF(I,J,K)+FF(I-1,J,K))/(FF(I-1,J,K)+FF(I,J,K)+EPSILON)
      			A1=(abs(FF(I-1,J,K)-FF(I,J,K)))/(FF(I-1,J,K)+FF(I,J,K)+EPSILON)
      			SW_U(I,J,K)=.5_DP*(1._DP+sign(1._DP,-1._DP*((A1-ES)*A2)))
      		END DO
      	END DO
      END DO
!      
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IMM1
      			A1=(abs(-FF(I,J,K)+FF(I,J-1,K)))/(FF(I,J-1,K)+FF(I,J,K)+EPSILON)
      			A2=(FF(I,J,K)+FF(I,J-1,K))/(FF(I,J-1,K)+FF(I,J,K)+EPSILON)
      			SW_V(I,J,K)=.5_DP*(1._DP+sign(1._DP,-1._DP*((A1-ES)*A2)))
      		END DO
      	END DO
      END DO
!
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
      			A1=(abs(FF(I,J,K-1)-FF(I,J,K)))/(FF(I,J,K)+FF(I,J,K-1)+EPSILON)
      			A2=(FF(I,J,K-1)+FF(I,J,K))/(FF(I,J,K)+FF(I,J,K-1)+EPSILON) 
      			SW_Z(I,J,K)=.5_DP*(1._DP+sign(1._DP,-1._DP*((A1-ES)*A2)))
      		END DO
      	END DO
      END DO
!
!-----------------------------------------------------------------------
!    RECALCULATE MASS FLUXES WITH ANTIDIFFUSION VELOCITY 
!-----------------------------------------------------------------------
      DO K=1,KBM1
      	DO J=2,JMM1
      		DO I=2,IM
      			IF(FF(I,J,K).LT.VALUE_MIN.OR.FF(I-1,J,K).LT.VALUE_MIN) THEN
         			XFlux(I,J,K)=0.0_DP 
      			ELSE
         			SW=min(SW_U(I-1,J,K),SW_U(I,J,K))
         			UDx=ABS(XFlux(I,J,K))
         			U2Dt=( 2.*DTI2*XFlux(I,J,K)*XFlux(I,J,K)/ &
     				(ARU(I,J)*(DT(I-1,J)+DT(I,J))) )
     	   			Mol=(FF(I,J,K)-FF(I-1,J,K))/(FF(I-1,J,K)+FF(I,J,K)+EPSILON)
         			XFlux(I,J,K)= (UDx - U2Dt)*Mol*SW
         			Abs_1=ABS(UDx) 
         			Abs_2=ABS(U2Dt)
          			IF(Abs_1 .LT. Abs_2) XFlux(I,J,K)=0.0_DP
      			END IF
      		END DO
      	END DO
      END DO
!
      DO K=1,KBM1
      	DO J=2,JM
      		DO I=2,IMM1
      			IF(FF(I,J,K).LT.VALUE_MIN.OR.FF(I,J-1,K).LT.VALUE_MIN) THEN
         			YFlux(I,J,K)=0.0_DP
      			ELSE
	   			SW=min(SW_V(I,J-1,K),SW_V(I,J,K))
         			VDy=ABS(YFlux(I,J,K))
         			V2Dt=( 2._DP*DTI2*YFlux(I,J,K)*YFlux(I,J,K)/ &
     				(ARV(I,J)*(DT(I,J-1)+DT(I,J))) )
         			Mol=(FF(I,J,K)-FF(I,J-1,K))/(FF(I,J-1,K)+FF(I,J,K)+EPSILON)
         			YFlux(I,J,K)= (VDy - V2Dt)*Mol*SW
         			Abs_1=ABS(VDy)
         			Abs_2=ABS(V2Dt)
          			IF(Abs_1 .LT. Abs_2)YFlux(I,J,K)=0.0_DP
      			END IF
      		END DO
      	END DO
      END DO
!
      DO K=2,KBM1
      	DO J=2,JMM1
      		DO I=2,IMM1
      			IF(FF(I,J,K).LT.VALUE_MIN.OR.FF(I,J,K-1).LT.VALUE_MIN) THEN
         			ZFlux(I,J,K)=0.0_DP
      			ELSE
         			SW=min(SW_Z(I,J,K-1),SW_Z(I,J,K))
         			WDz=ABS(ZFlux(I,J,K))
         			W2Dt=( DTI2*ZFlux(I,J,K)*ZFlux(I,J,K)/ &
     				(DZZ(K-1)*DT(I,J)) )
         			Mol=(FF(I,J,K-1)-FF(I,J,K))/(FF(I,J,K)+FF(I,J,K-1)+EPSILON)
         			ZFlux(I,J,K)= (WDz - W2Dt)*Mol*SW
         			Abs_1=ABS(WDz)
         			Abs_2=ABS(W2Dt)
          			IF(Abs_1 .LT. Abs_2)ZFlux(I,J,K)=0.0_DP
      			END IF
      		END DO
      	END DO
      END DO
!
      RETURN
      END SUBROUTINE SMOL_ADIF
