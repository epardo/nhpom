      SUBROUTINE DEPTH(Z,ZZ,DZ,DZZ,KB)
      USE double
      IMPLICIT NONE
      REAL(KIND=DP), DIMENSION(KB) :: Z, ZZ, DZ, DZZ
      INTEGER, DIMENSION(12) :: KDZ
      DATA KDZ/1,1,2,4,8,16,32,64,128,256,512,1024/
      REAL(KIND=DP) hof
      INTEGER :: KB, K, KL1, KL2
      REAL(KIND=DP) :: DELZ
! ***
!    THIS SUBROUTINE ESTABLISHES THE VERTICAL SIGMA GRID WITH LOG
!    DISTRIBUTIONS  AT THE TOP AND BOTTOM AND A LINEAR DISTRIBUTION
!    BETWEEN. CHOOSE KL1 AND KL2. THE VALUES, KL1 = 2 AND KL2 = KB-2,
!    YIELD AN EQUALLY SPACED DISTRIBUTION WITH NO LOG PORTIONS.   
! ***
      KL1=2
      KL2=KB-2
!
      Zz(1)=0.
      DO K=2,KL1
	Zz(K)=Zz(K-1)+KDZ(K-1)
      ENDDO
      DELZ=Zz(KL1)-Zz(KL1-1)
      DO K=KL1+1,KL2
	Zz(K)=Zz(K-1)+DELZ
      ENDDO
      DO K=KL2+1,KB
	DZz(K)=FLOAT(KDZ(KB-K+1))*DELZ/FLOAT(KDZ(KB-KL2))
	hof=0.4
!	do k=2,kb
!	zz(k)=zz(k-1)+1./(kb-1.)
!	enddo
	Zz(K)=Zz(K-1)+DZz(K)                                         
      ENDDO
      DO K=1,KB
	Zz(K)=-Zz(K)/zz(kb)
      ENDDO

      DO K=2,KB
	Z(K)=0.5*(Zz(K)+Zz(K-1))
      ENDDO
      Z(1)=zz(1)
      DO K=1,KB-1
	DZ(K)=Z(K)-Z(K+1)
	DZZ(K)=ZZ(K)-ZZ(K+1)
      END DO
      
      WRITE(6,70)
   70 FORMAT(/2X,'K',7X,'Z',9X,'ZZ',9X,'DZ',9X,'DZZ',/)
	dzz(kb)=0.
	dz(kb)=0.
      DO K=1,KB
	WRITE(6,80) K,Z(K),ZZ(K),DZ(K),DZZ(K)
   80   FORMAT(' ',I5,4F10.3)
      END DO
      RETURN
      END SUBROUTINE DEPTH
