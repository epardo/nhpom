      SUBROUTINE ADVCTw2(ADVw)               
!======================================================================
!  This routine calculates the horizontal portions of momentum advection
!  well in advance of their use in ADVU and ADVV so that their vertical 
!  integrals (created in MAIN) may be used in the external mode calculation.
!======================================================================
      USE BLK2D
      USE BLK33D
      USE BLK3D
      USE PROM
      IMPLICIT NONE
 !    INCLUDE 'comblk98.h' 
	
      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: ADVw
      REAL(KIND=DP), DIMENSION(:, :, :), POINTER :: XFLUXw, YFLUXw
      INTEGER :: I, J, K
 !     equivalence(xfluxw,a),(yfluxw,c)
      xfluxw => a
      yfluxw => c
      
      DO K=1,KB
      	DO J=1,JM
      		DO I=1,IM
      			ADVw(I,J,K)=0.
      			XFLUXw(I,J,K)=0.
  			YFLUXw(I,J,K)=0.
  		END DO
  	END DO
      END DO


!******** HORIZONTAL ADVECTION FLUXES *****************************
      DO K=2,KBm1
      	DO J=2,JM
      		DO I=2,IM
			XFLUXw(I,J,K)=.25E0* &
     			(U(I,J,K-1)+U(I,J,K))*(Ww(i-1,j,k)*DT(I-1,J)+ &
     			wW(i,j,k)*DT(I,J))
     			
			YFLUXw(I,J,K)=.25E0* &
     			(V(I,J,K-1)+V(I,J,K))*(Ww(i,j-1,k)*DT(I,J-1)+ &
     			Ww(i,j,k)*DT(I,J))
     		END DO
     	END DO
     END DO    


!******** DO HORIZ. ADVECTION *******

	
      DO K=1,KB
      	DO J=1,JMm1
      		DO I=1,IMm1
 			ADVw(I,J,K)= &
     			+dy(i,j)*(XFLUXw(I+1,J,K)-XFLUXw(I,J,K)) &
     			+dx(i,j)*(YFLUXw(I,J+1,K)-YFLUXw(I,J,K))
     		END DO
     	END DO
     END DO

     RETURN
    END SUBROUTINE ADVCTw2
    
      SUBROUTINE ADVeW2(ADVw,DTI2)
      USE BLK1D
      USE BLK2D
      USE BLK33D
      USE BLK3D
      IMPLICIT NONE
!     INCLUDE 'comblk98.h'

      REAL(KIND=DP), DIMENSION(IM,JM,KB) :: ADVw, wpr
      REAL(KIND=DP) :: DTI2
      INTEGER :: I, J, K
!
!  Do vertical advection
      DO K=1,KB  
      	DO J=1,JM
      		DO I=1,IM
  			WwF(I,J,K)=0.
  		END DO
  	END DO
      END DO
  
  
      DO I=1,IM
      	DO J=1,JM
		DO K=2,KB
			Wpr(I,J,K)=.25e0*(Ww(I,J,K)+ww(i,j,k-1))*(w(I,J,K)+w(i,j,k-1))
		END DO
	END DO
      END DO

!****COMBINE HOR. and VERT. ADVECTION with
!           -FVD + GDEG/DX + BAROCLINIC TERM **********************
	
      DO I=1,IM
      	DO J=1,JM
		DO K=1,KB-1
	 		wWF(I,J,K)=ADVw(I,J,K)+(Wpr(I,J,K)-Wpr(I,J,K+1))*dx(I,J)* &
			dy(i,j)*fsm(i,j)/(DZz(K))
		END DO
	END DO
      END DO


!****** STEP FORWARD IN TIME ***********************************
           
      DO I=1,IM	
      	DO J=1,JM
		DO K=1,KB-1   
 			wWF(I,J,K)= &
     			((H(I,J)+ETB(I,J))*dx(I,J)*dy(i,j)*wwb(I,J,K)*fsm(i,j) &
     			-DTI2*wwF(I,J,K)*fsm(i,j)) &
     			/((H(I,J)+ETF(I,J))*dx(I,J)*dy(i,j))
     		END DO
     	END DO
      END DO
!
      RETURN
      END SUBROUTINE ADVeW2
 
