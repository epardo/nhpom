NAME:=nhpom

#
# Compiler flags
#
FC = gfortran-mp-7

ADV_DIR = $(ADVISOR_2017_DIR)

#ANNOTATE_FLAGS = -I $(ADV_DIR)/include/$(Arch) -L$(ADV_DIR)/$(libdir) -ladvisor -ldl
ANNOTATE_FLAGS = -fopt-info

#OBJS = $(patsubst %.f90,%.o,$(wildcard *.f90))

.PHONY: all clean debug release 

# Default build
all: debug release

release:
	cd release; make

debug:
	cd debug; make

clean::
	cd debug; make clean; cd ../release; make clean;
